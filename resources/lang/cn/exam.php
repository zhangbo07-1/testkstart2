<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '考试管理',
    'create' => '添加新考试',
    'title' => '考试标题',
    'description' => '考试描述',
    'id' =>'考试编号',
    'pass_score' => '通过分数',
    'email' => '是否发送邮件提醒',
    'link' => '是否允许关联到课程',
    'link_log' => '如果勾选此选项，该考试不能被分配',
    'assign' =>'关联考试',
    'assigned' => '已关联考试',
    'for_assign' => '可选关联',
    'assign_success' => '关联考试成功',
    'combinate' =>'组卷',
    'weight' => '权重',

];
