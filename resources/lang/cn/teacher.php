<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'assign' =>'关联教师',
    'assigned' => '已关联教师',
    'for_assign' => '可关联教师',
    'assign_success' => '关联教师成功',
    'unassign_success' => '取消关联成功',

];
