<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '讨论区管理',
    'name' => '讨论区',
    'create' => '添加新讨论',
    'select_image' => '选择标题图片',
    'summary' =>'摘要',
    'comment' => '评论',


];
