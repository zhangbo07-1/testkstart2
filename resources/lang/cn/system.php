<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logo' => '图标管理',
    'info' => '系统信息',
    'logo_limit' => '请上传300×100像素的png文件作为系统LOGO，谢谢!',
    'logo_now' => '当前图标',
    'company' => '组织名称',
    'phone'  => '组织电话',
    'email' => '组织邮箱',
    'company_limit' =>'组织名只能由字母组成',


];
