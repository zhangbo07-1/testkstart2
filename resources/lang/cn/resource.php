<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '资料库管理',
    'name' => '资料库',
    'create' => '添加新资料',
    'title' => '资料名',
    'file'=>'文件名',
    'size' => '文件大小',


];
