<?php

return [

    /*
       |--------------------------------------------------------------------------
       | survey Language Lines
       |--------------------------------------------------------------------------
       |
       | The following language lines are used during survey for various
       | messages that we need to display to the user. You are free to modify
       | these language lines according to your application's requirements.
       |
     */

    'manage' => '评估管理',
    'create' => '添加新评估',
    'name' => '评估',
    'assign' => '关联评估',
    'assigned'=>'已关联评估',
    'for_assign' => '可选评估',
    'assign_success' => '关联评估成功',



];
