<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'distribute' =>'学员分配',
    'distributed' => '已分配学员',
    'for_distribute' => '未分配学员',
    'distribute_success' => '分配成功',
    'undistribute_success' => '取消分配成功',
    'overdue'=>'已过期学员',
    'redistribute_success' => '重分配成功',

];
