<?php

return [

    /*
    |--------------------------------------------------------------------------
    | button Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during button for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'edit' => '编辑',
    'delete' => '删除',
    'submit' => '确定',
    'cancel' => '取消',
    'return' => '返回',
    'search' => '搜索',
    'range' => '选择对象',
    'preview'=> '预览',
    'push'   => '发布',
    'result' => '结果',
    'add_select' => '添加选项',
    'upload' => '上传',
    'download' => '下载',
    'department' => '选择部门',
    'view' => '查看',
    'file' => '选择文件',
    'catalog' => '选择目录',
    'update' => '更新',
    'example' => '导出模板',
    'redistribute'=>'重新分配',
    'mark' => '批阅',

];
