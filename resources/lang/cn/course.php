<?php

return [

    /*
    |--------------------------------------------------------------------------
    | survey Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during survey for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '课程管理',
    'create' => '添加新课程',
    'title' => '课程标题',
    'description' => '课程描述',
    'id' =>'课程编号',
    'hours' => '课程时长',
    'email' => '是否发送邮件提醒',
    'link' => '是否分配给新用户',
    'link_log' => '如果勾选此选项，该考试不能被分配',
    'target' => '目标用户',
    'order' => '是否按照以下顺序',
    'order_log' => '（课程->考试->评估）',
    'overdue' => '是否允许打开过期课程',
    'upload' => '课件上传',
    'upload_log' => '仅支持scorm2004课件包,格式为.zip',
    'ware_name' => '课件名',



];
