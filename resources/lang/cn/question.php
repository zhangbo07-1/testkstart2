<?php

return [

    /*
    |--------------------------------------------------------------------------
    | question Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during question for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '题目管理',
    'create' => '添加新题目',
    'edit'   => '编辑题目',
    'select' => '选择题',
    'radio' => '单选题',
    'mulit' => '多选题',
    'false' => '是非题',
    'grade'  => '评分题',
    'essay'  => '简答题',
    'title'  => '题目',
    'grade_hight' => '高平分',
    'grade_low'   => '低评分',
    'select_title' =>'选项',
    'priority' => '强制',
    'true' =>'正确',
    'unit' => '题',
    'answer' => '回答',


];
