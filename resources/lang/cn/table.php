<?php

return [

    /*
    |--------------------------------------------------------------------------
    | table Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during table for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => '标题',
    'pushed_time' => '发布时间',
    'create_time' => '创建时间',
    'update_time'=> '更新时间',
    'pushedOrNot' => '是否已发布',
    'operation'   => '操作',
    'text'        => '正文',
    'Qnum'        => '题目数',
    'choose'      => '选择',
    'yes'         => '是',
    'no'          => '不',
    'none'        => '无',
    'range'      => '用户对象',
    'catalog'    => '目录',
    'type'       => '类型',
    'author'     => '作者',
    'user'       => '用户',
    'comment_num' => '回复数',
    'input_select_user' => '用户名/姓名/角色',
    'input_select_email' => '请输入要搜索邮件类型',
    'input_select_survey' => '请输入要搜索的调研名',
    'input_select_evaluation' => '请输入要搜索的评估名',
    'input_select_department' => '请输入要搜索的组织名',
    'input_select_catalog'    => '请输入要搜索的目录名',
    'input_select_exam'   => '请输入要搜索的考试名',
    'input_select_course' => '请输入要搜索的课程名',
    'input_select' => '请输入要搜索的关键字',
    'user_name' =>'用户名',
    'email' => '邮箱',
    'data' => '日期',
    'status' => '状态',
    'success' => '成功',
    'fail' => '失败',
    'sending' => '发送中',
    'language' => '语言',
    'more' => '更多',
    'rank' => '排行',
    'unknow' => '未知',
    'select_file' => '文件路径或网络链接',
    'must' => '（必填项）',
    'limit_data' =>'限定时长',
    'limit_times' => '限定次数',
    'minute' => '分钟',
    'hour' => '小时',
    'day' => '天',
    'limit_data_log' => '（0表示无限制）',
    'select_Qnum' => '已选题目',


];
