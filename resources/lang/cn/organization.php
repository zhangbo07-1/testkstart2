<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '组织结构管理',
    'create' => '新建组织',
    'rename' => '重命名',
    'delete' => '删除',


];
