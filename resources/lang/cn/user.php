<?php

return [

    /*
    |--------------------------------------------------------------------------
    | news Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during news for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'manage' => '用户管理',
    'create' =>'新建用户',
    'title' => '用户名',
    'email'=>'邮箱地址',
    'pwd' => '密码',
    'real_name' => '姓名',
    'role' =>'角色',
    'ranger' => '管理对象',
    'department' => '部门',
    'position' =>'职位',
    'rank' => '职级',


];
