@extends('layout.master')

@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
@append

@section('content')
    @include('system_info.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection