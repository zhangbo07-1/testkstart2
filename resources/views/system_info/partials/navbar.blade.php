<div class="leftpanel">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">
                <h5 class="sidebar-title"></h5>
                <ul class="nav nav-pills nav-stacked nav-quirk">
                    <li id="logoNav">
                        <a href="/system-manage/logo">
                            <i class="fa fa-home"></i><span>{{ trans('system.logo') }}</span>
                        </a>
                    </li>
                    <li id="messageNav">
                        <a href="/system-manage/info">
                            <i class="fa fa-user"></i><strong>{{ trans('system.info') }}</strong>
                        </a>
                    </li>
{{--
                    <li id="censorNav">
                        <a href="/system-manage/censor">
                            <i class="fa fa-refresh"></i><strong>审核管理</strong>
                        </a>
                    </li>
                    <li id="emailNav">
                        <a href="/system-manage/email">
                            <i class="fa fa-refresh"></i><strong>邮箱管理</strong>
                        </a>
                    </li>
  --}}
                </ul>
            </div>
        </div>
    </div>
</div>
