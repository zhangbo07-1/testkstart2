@extends('system-manage.partials.template')

@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
@endsection
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append

    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">系统信息</h2>
                </div>
            </div>
            
            

            <div class="panel panel-announcement">
                <br>
               <div>@include('flash::message')</div>
                
                请添加登录所需邮箱后缀，例如163.com
                
                <br>
                <br>
                <div class="login-form clearfix">
                    <form  action="{{ URL('system-manage/email') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">


                        <div id="listNUM">
                            <input type="hidden" id="emailNUM" name="emailNUM" value="{{count($info->emailtype)}}">
                        </div>


                        <div id="emailPlace">
                            @for($i=1;$i<=count($info->emailtype);$i++)
                                <div id="emaildiv_{{$i}}">
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="text" name="email{{$i}}" class="form-control" required="required" value="{{$info->emailtype[$i-1]->email}}" >
                                            </td>
                                            <td><input type="button" value="删除" class="dm3-btn dm3-btn-medium button-large"  onclick="delEmail({{$i}})"/></td>
                                        </tr>
                                    </table>
                                </div>
                            @endfor
                        </div>

                      
                       
                        <div class="form-group">
                            <label for="confirm_button" class="col-xs-4 control-label"></label>

                            <div class="col-xs-8">
                                <button class="dm3-btn dm3-btn-medium button-large" type="submit">
                                    <i class="fa fa-check"></i>&nbsp;确认
                                    
                                </button>
                                 <input type="button" class="dm3-btn dm3-btn-medium button-large" id="addEmailBTN" onClick="addEmail()" value="添加邮件"/>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endsection
