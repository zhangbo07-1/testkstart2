@extends('system-manage.partials.template')

@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
@endsection


@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">系统信息</h2>
            </div>
        </div>
        
        

        <div class="panel panel-announcement">
            <br>
            <br>
            @include('partials.layout.flash')
            <div class="login-form clearfix">
                <form  class="form-horizontal length-750px" action="{{ URL('system-manage/censor') }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                       
                        <label  class="col-xs-4 control-label">是否需要审核</label>
                        @if($info->censor == 0)
                            <input type="radio" name="censor" checked="checked" value="0" >不需要审核
                            <br>
                            <input type="radio" name="censor" value="1" >需要审核
                        @else
                            <input type="radio" name="censor" value="0" >不需要审核
                            <br>
                            <input type="radio" name="censor" checked="checked" value="1" >需要审核
                        @endif
                        
                    </div>
                    

                    <div class="form-group">
                        <label for="confirm_button" class="col-xs-4 control-label"></label>
                        <div class="col-xs-8">
                            <button class="dm3-btn dm3-btn-medium button-large" type="submit">
                                <i class="fa fa-check"></i>&nbsp;确认
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    
@endsection
