@extends('system_info.partials.template')

@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
@endsection


@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{ trans('system.info') }}</h2>
            </div>
        </div>
        
        

        <div class="panel panel-announcement">
            <br>
            <br>
            <div class="login-form clearfix">
                <form  class="form-horizontal length-750px" action="{{ URL('system-manage/info') }}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label  class="col-xs-4 control-label">{{ trans('system.company') }}</label>

                        <div class="col-xs-8">
                            <input type="text" id="company" name="company" class="form-control width-168px" required="required"
                                   value="{{$info->company}}">
                            {!! errors_for('company', $errors) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-xs-4 control-label">{{ trans('system.phone') }}</label>

                        <div class="col-xs-8">
                            <input type="text" name="phone" class="form-control width-168px" required="required"
                                   value="{{$info->phone}}">
                            {!! errors_for('phone', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label  class="col-xs-4 control-label">{{ trans('system.email') }}</label>

                        <div class="col-xs-8">
                            <input type="text" name="email" class="form-control" required="required" value="{{$info->email}}">
                            {!! errors_for('email', $errors) !!}
                        </div>
                    </div>

                    

                    <div class="form-group">
                        <label for="confirm_button" class="col-xs-4 control-label"></label>

                        <div class="col-xs-8">
                            <button class="dm3-btn dm3-btn-medium button-large" type="submit">
                                <i class="fa fa-check"></i>&nbsp;{{ trans('button.submit') }}
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <script>
     function checkinput(e)
     {
         
         var nameVal =$("#company").val()
             if (!nameVal.match( /^[|a-zA-Z]*$/)) {
                 alert("{{ trans('system.company_limit') }}");
                 return false;
                 
             }
         return true;
         
     }

     
    </script>
@endsection
