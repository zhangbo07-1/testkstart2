@extends('layout.base')
@section('js')

    {!! Html::script('assets/bootstrap-treeview/dist/bootstrap-treeview.js') !!}
    {!! Html::script('assets/js/manage.js') !!}
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="row" style="margin-right:0">
            <div class="col-md-6 col-md-offset-3">

                <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:100%;">
                    <div id="tree"></div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <div align="center">
            <a class="dm3-btn dm3-btn-medium dm3-btn button-large" onClick="getSelect()">{{ trans('button.submit') }}</a>
        </div>

    @endsection
