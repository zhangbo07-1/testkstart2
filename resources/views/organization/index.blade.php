@extends('layout.master')
@section('css')
    {!! Html::style('assets/jstree/dist/themes/default/style.min.css') !!}
    @append
    @section('js')

        {!! Html::script('assets/jstree/dist/jstree.js') !!}
        {!! Html::script('assets/js/organization.js') !!}
        @append
        @section('content')
            <div class="container">
                <br>
                <br>
                <h3>{{ trans('organization.manage') }}</h3>
                <div class="container clearfix">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-inline">
                                <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="create_node();"><i class="glyphicon glyphicon-asterisk"></i>{{ trans('organization.create') }}</button>
                                <button type="button"class="dm3-btn dm3-btn-medium button-large" onclick="rename_node();"><i class="glyphicon glyphicon-pencil"></i>{{ trans('organization.rename') }}</button>
                                <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="delete_node();"><i class="glyphicon glyphicon-remove"></i>{{ trans('organization.delete') }}</button>
                                <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="search_node();"><i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}</button>
                                <input id="searchWord" type="text" class="form-control"  placeholder="{{ trans('table.input_select') }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <div id="jstree_div"></div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>


            </div>
        @endsection
