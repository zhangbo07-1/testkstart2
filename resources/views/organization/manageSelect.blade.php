@extends('layout.base')
@section('css')
    {!! Html::style('assets/jstree/dist/themes/default/style.min.css') !!}
    @append
    @section('js')

        {!! Html::script('assets/jstree/dist/jstree.js') !!}
        {!! Html::script('assets/js/manage.js') !!}
        {!! Html::script('assets/js/views/view.operation.js') !!}
        @append
        @section('content')

            <div class="col-md-6 col-md-offset-3">
                <div class="form-inline">
                    <button type="button" class="dm3-btn dm3-btn-medium button-large" onclick="search_node();"><i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}</button>
                    <input id="searchWord" type="text" class="form-control"  placeholder="{{ trans('table.input_select') }}">
                </div>
            </div>
            <div class="row" style="margin-left:0">
                <div class="col-md-6 col-md-offset-3">
                    <div id="jstree_div"></div>
                </div>
            </div>
            <br>
            <br>
            <div align="center">
                <a class="dm3-btn dm3-btn-medium dm3-btn button-large" onClick="getSelect()">{{ trans('button.submit') }}</a>
                <a class="dm3-btn dm3-btn-medium dm3-btn-red button-large" href="javascript:window.close();">{{ trans('button.cancel') }}</a>
            </div>

        @endsection
