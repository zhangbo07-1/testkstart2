@extends('layout.master')

@section('content')
    <br>
    <br>

    <div class="container">

        <h1 class="text-center">培训记录</h1>

 <div align="right">
        <a class="dm3-btn dm3-btn-medium button-large" href="javascript:window.location.href=document.referrer; " ><i class="fa fa-reply"></i>&nbsp;返回</a>
    </div>

        <div class="bar"></div>
        <h4>详细信息</h4>

        <div class="table-responsive">
            <table class="table">
                <tr>
                    <td>注册时间:</td>
                    <td>{{$record->created_at}}</td>
                </tr>
                <tr>
                    <td>开始时间:</td>
                    <td>{{$record->started_at}}</td>
                </tr>
                <tr>
                    <td>结束时间:</td>
                    @if($record->process >=2)
                        <td>{{$record->end_at}}</td>
                    @else
                        <td></td>
                    @endif
                </tr>
                <tr>
                    <td>总培训时间:</td>
                    <td>
                        @if($record->study_span)
                            {{round($record->study_span/60,1)}}分钟
                            @else
                            0分钟
                            @endif
                    </td>
                </tr>
                <tr>
                    <td>在线启动总数:</td>
                    <td>{{$record->study_times}}</td>
                </tr>
                <tr>
                    <td>学习状态:</td>
                    @if($record->process ==0)
                        <td>未开始</td>
                    @elseif($record->process ==1)
                        <td>进行中</td>
                    @else
                        <td>已完成</td>
                    @endif
                </tr>
                <tr>
                    <td>考试状态:</td>
                    @if(!$record->course->exam)
                        <td>无</td>
                    @elseif(count($examResults)<1)
                        <td>未开始</td>
                    @elseif($record->exam_max_score > $record->course->exam->pass_score)
                        <td>通过</td>
                    @elseif(count($examResults) >=$record->course->exam->limit_times && $record->course->exam->limit_times > 0)
                        <td>已完成</td>
                    @else
                        <td>未通过</td>
                    @endif
                </tr>
                <tr>
                    <td>评估状态:</td>
                    @if(!$record->course->evaluationRecord)
                        <td>无</td>
                    @elseif(count($evaResults)<1)
                        <td>未开始</td>
                    @else
                        <td>已完成</td>
                    @endif
                </tr>
                <tr>
                    <td>分数:</td>

                    <td>{{$record->lesson_max_score}}</td>
                </tr>
            </table>
        </div>
    </div>

    @include('courses.partials.log_partial')

@endsection
