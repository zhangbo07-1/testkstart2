@extends('app')

@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
@endsection


@section('content')
    <div class="container">
        <br>
        <br>

        <h3>{{$course->name}}</h3>
        <div align="right">
            <a class="dm3-btn dm3-btn-medium button-large" href="javascript:window.location.href=document.referrer; " ><i class="fa fa-reply"></i>&nbsp;返回</a>
        </div>
        <br>
        <br>

        课程描述：

        {{$course->description}}

        <br>
        <br>

        学习类型：

        在线课程

        <br>
        <br>

        课程目录：

        {{$course->courseCatalog->title}}

        <br>
        <br>

        课程时长：

        {{$course->hours}}

        <br>
        <br>

        截止日期：

        {{$course->limitdays}}

        <br>
        <br>

        目标用户：

        {{$course->target}}
        <br>
        <br>

        课程语言：

        {{$course->courseLanguage->title}}
        <br>
    </div>
@endsection
