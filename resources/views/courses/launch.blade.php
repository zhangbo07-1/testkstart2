<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN"
          "http://www.w3.org/TR/1999/REC-html401-19991224/frameset.dtd">
<html>
    <head><title>KnowSurface</title>
        <meta content='IE=edge' http-equiv='X-UA-Compatible'>

        {{--<meta name="viewport" content="width=600, target-densitydpi=device-dpi, user-scalable=yes, minimum-scale=0.1, maximum-scale=10">--}}
        <meta name="_token" content="{{ csrf_token() }}"/>
        <script>
         var user_id = {{ $user_id }};
         var course_id = "{{ $course_id }}";
         var start = Date();
         var scormtype = 1;
         var suspend_data = '{!!$suspend_data!!}';
         var completion_status = '{{$completion_status}}';
         var cmi_location = '{{$cmi_location}}';
         var cmi_mode = '{{$cmi_mode}}';
         function loadScoFrame() {
             window.sco.location="/{!! $filename !!}";
         }


        </script>
        {!! Html::script('assets/vendor/jquery.js') !!}
        {!! Html::script('assets/js/chamilo_api.js') !!}
    </head>
    <frameset rows="0,*" onload="setTimeout('loadScoFrame()',1000)">
        <frame id="apiadapter" frameborder="0" name="apiadapter" src="">
            <frame id="sco" frameborder="0" name="sco" src="">
                <noframes>
                    <body>您的浏览器无法处理框架！</body>
                </noframes>
    </frameset>
</html>
