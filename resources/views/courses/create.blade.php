@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>

            <h3>{{ trans('course.create') }}</h3>

            <div class="entry-content">

                <div class=" clearfix">
                    {!! Form::open(array('url'=>'courses','method'=>'POST', 'class' => 'form-horizontal')) !!}


                    <div class="form-group {!! errors_for_tag('course_id', $errors) !!}">
                        <label for="course_id" class="col-xs-4 control-label">{{ trans('course.id') }}{{ trans('table.must') }}</label>

                        <div class="col-xs-8">
                            {!! Form::text("course_id",null,array('class' => 'form-control','required' => 'required')) !!}
                            {!! errors_for('course_id', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('name', $errors) !!}">
                        <label for="title" class="col-xs-4 control-label">{{ trans('course.title') }}{{ trans('table.must') }}</label>

                        <div class="col-xs-8">
                            {!! Form::text('title', null, ['size' => '30x1','class'=>"form-control","required"=>"required"]) !!}
                            {!! errors_for('title', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('description', $errors) !!}">
                        <label for="description" class="col-xs-4 control-label">{{ trans('course.description') }}</label>

                        <div class="col-xs-8">
                            {!! Form::textarea('description', null, ['size' => '30x4','class'=>"form-control"]) !!}
                            {!! errors_for('description', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('hours', $errors) !!}">
                        <label for="hours" class="col-xs-4 control-label">{{ trans('course.hours') }}</label>

                        <div class="col-xs-8">
                            <div class="form-inline margin-left-10px">
                                <div class="form-group width-130px">
                                    <div class="input-group">
                                        {!! Form::text("hours",0,array('class' => 'form-control text-right')) !!}
                                        <div class="input-group-addon">{{ trans('table.hour') }}</div>
                                    </div>
                                </div>
                            </div>
                            {!! errors_for('hours', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('minutes', $errors) !!}">
                        <label for="minutes" class="col-xs-4 control-label"></label>

                        <div class="col-xs-8">
                            <div class="form-inline margin-left-10px">
                                <div class="form-group width-130px">
                                    <div class="input-group">
                                        {!! Form::text("minutes",0,array('class' => 'form-control text-right')) !!}
                                        <div class="input-group-addon">{{ trans('table.minute') }}</div>
                                    </div>
                                </div>
                            </div>
                            {!! errors_for('minutes', $errors) !!}
                        </div>
                    </div>
                    <div class="form-group {!! errors_for_tag('limitdays', $errors) !!}">
                        <label for="limitdays" class="col-xs-4 control-label">{{ trans('table.limit_data') }}</label>

                        <div class="col-xs-8">
                            <div class="form-inline margin-left-10px">
                                <div class="form-group width-230px">
                                    <div class="input-group">
                                        {!! Form::text("limitdays",0,array('class' => 'form-control text-right')) !!}
                                        <div class="input-group-addon">{{ trans('table.day') }}{{ trans('table.limit_data_log') }}</div>
                                    </div>
                                </div>
                            </div>
                            {!! errors_for('limitdays', $errors) !!}
                        </div>
                    </div>


                    <div class="form-group {!! errors_for_tag('manage_id', $errors) !!}">
                        <label for="manage_id" class="col-xs-4 control-label">{{ trans('table.range') }}</label>

                        <div class="col-xs-8">
                            <div style="display:inline-block;">

                                <a href="javascript:manageSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.range') }}</a>
                            </div>
                            <div style="display:inline-block;" id="manage_name">{{$manages}}</div>
                            <input type="hidden" id="manage_id"  name="manage_id" value=""/>
                            {!! errors_for('manage_id', $errors) !!}
                        </div>
                    </div>
                    <div class="form-group"{!! errors_for_tag('catalog_id', $errors) !!}">
                        <label for="catalog_id" class="col-xs-4 control-label">{{ trans('table.catalog') }}{{ trans('table.must') }}</label>

                        <div class="col-xs-8">
                            <div style="display:inline-block;">

                                <a href="javascript:catalogSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.catalog') }}</a>
                            </div>
                            <div style="display:inline-block;" id="catalog_name"></div>
                            <input type="hidden" id="catalog_id"  name="catalog_id" value=""/>
                            {!! errors_for('catalog_id', $errors) !!}
                        </div>
                    </div>


                    <div class="form-group {!! errors_for_tag('target', $errors) !!}">
                        <label for="trget" class="col-xs-4 control-label">{{ trans('course.target') }}</label>

                        <div class="col-xs-8">
                            {!! Form::textarea('target', null, ['size' => '30x4','class'=>"form-control"]) !!}
                            {!! errors_for('target', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('emailed', $errors) !!}">
                        <label for="emailed" class="col-xs-4 control-label">{{ trans('course.email') }}</label>

                        <div class="col-xs-8">
                            {!! Form::checkbox('emailed',1,true,['class'=>'div-form-control']) !!}
                            {!! errors_for('emailed', $errors) !!}
                        </div>
                    </div>



                    <div class="form-group {!! errors_for_tag('if_send_new_user', $errors) !!}">
                        <label for="if_send_new_user" class="col-xs-4 control-label">{{ trans('course.link') }}</label>

                        <div class="col-xs-8">
                            {!! Form::checkbox('if_send_new_user',1,false,['class'=>'div-form-control']) !!}
                            {!! errors_for('if_send_new_user', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('if_order', $errors) !!}">
                        <label for="if_order" class="col-xs-4 control-label">{{ trans('course.order') }}</label>

                        <div class="col-xs-8">
                            {!! Form::checkbox('if_order',1,false,['class'=>'div-form-control']) !!}
                            {{ trans('course.order_log') }}
                            {!! errors_for('if_order', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('if_open_overdue', $errors) !!}">
                        <label for="if_open_overdue" class="col-xs-4 control-label">{{ trans('course.overdue') }}</label>

                        <div class="col-xs-8">
                            {!! Form::checkbox('if_open_overdue',1,true,['class'=>'div-form-control']) !!}
                            {!! errors_for('if_open_overdue', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('language_id', $errors) !!}">
                        <label for="lanugage_id" class="col-xs-4 control-label">{{ trans('table.language') }}</label>

                        <div class="col-xs-8">
                            {!! Form::select('language_id', $languageSelect,null,['class'=>"form-control"]) !!}
                            {!! errors_for('language_id', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="confirm_button" class="col-xs-4 control-label"></label>

                        <div class="col-xs-8">
                            <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                            <a href="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    @endsection
