@extends('courses.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')

        @section('container')
            <div class="contentpanel">
                <div class="panel panel-announcement">
                    <div class="panel-heading">
                        <h2 class="panel-title">{{$course->title }}</h2>
                    </div>
                </div>

                <div class="panel panel-announcement">
                    <br>
                    <br>
                    {!! Form::open(array('url'=>'courses/'.$course->id,'method'=>'PUT', 'files'=>true, 'class' => 'form-horizontal length-750px')) !!}
                    <div class="form-group {!! errors_for_tag('course_id', $errors) !!}">
                        <label for="course_id" class="col-xs-4 control-label">{{ trans('course.id') }}{{ trans('table.must') }}</label>
                        <div class="col-xs-8">
                            {!! Form::text("course_id",$course->course_id,array('class' => 'form-control','required' => 'required')) !!}
                            {!! errors_for('course_id', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('name', $errors) !!}">
                        <label for="title" class="col-xs-4 control-label">{{ trans('course.title') }}{{ trans('table.must') }}</label>

                        <div class="col-xs-8">
                            {!! Form::text('title', $course->title, ['class'=>"form-control","required"=>"required"]) !!}
                            {!! errors_for('title', $errors) !!}
                        </div>
                    </div>


                    <div class="form-group {!! errors_for_tag('description', $errors) !!}">
                        <label for="description" class="col-xs-4 control-label">{{ trans('course.description') }}</label>

                        <div class="col-xs-8">
                            {!! Form::textarea('description', $course->description, ['size' => '30x4','class'=>"form-control"]) !!}
                            {!! errors_for('description', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('hours', $errors) !!}">
                        <label for="hours" class="col-xs-4 control-label">{{ trans('course.hours') }}</label>

                        <div class="col-xs-8">
                            <div class="form-inline margin-left-10px">
                                <div class="form-group width-130px">
                                    <div class="input-group">
                                        {!! Form::text("hours",$course->hours,array('class' => 'form-control text-right')) !!}
                                        <div class="input-group-addon">{{ trans('table.hour') }}</div>
                                    </div>
                                </div>
                            </div>
                            {!! errors_for('hours', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('minutes', $errors) !!}">
                        <label for="minutes" class="col-xs-4 control-label"></label>

                        <div class="col-xs-8">
                            <div class="form-inline margin-left-10px">
                                <div class="form-group width-130px">
                                    <div class="input-group">
                                        {!! Form::text("minutes",$course->minutes,array('class' => 'form-control text-right')) !!}
                                        <div class="input-group-addon">{{ trans('table.minute') }}</div>
                                    </div>
                                </div>
                            </div>
                            {!! errors_for('minutes', $errors) !!}
                        </div>
                    </div>
                    <div class="form-group {!! errors_for_tag('limitdays', $errors) !!}">
                        <label for="hours" class="col-xs-4 control-label text-right">{{ trans('table.limit_data') }}</label>

                        <div class="col-xs-8">
                            <div class="form-inline margin-left-10px">
                                <div class="form-group width-230px">
                                    <div class="input-group">
                                        {!! Form::text("limitdays",$course->limitdays,array('class' => 'form-control text-right')) !!}
                                        <div class="input-group-addon">{{ trans('table.day') }}{{ trans('table.limit_data_log') }}</div>
                                    </div>
                                </div>
                            </div>
                            {!! errors_for('limitdays', $errors) !!}
                        </div>
                    </div>


                    <div class="form-group {!! errors_for_tag('manage_id', $errors) !!}">
                        <label for="manage_id" class="col-xs-4 control-label">{{ trans('table.range') }}</label>

                        <div class="col-xs-8">
                            <div style="display:inline-block;">

                                <a href="javascript:manageSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.range') }}</a>
                            </div>
                            <div style="display:inline-block;" id="manage_name">{{$manages}}</div>
                            <input type="hidden" id="manage_id"  name="manage_id" value=""/>
                            {!! errors_for('manage_id', $errors) !!}
                        </div>
                    </div>
                    <div class="form-group"{!! errors_for_tag('catalog_id', $errors) !!}">
                        <label for="catalog_id" class="col-xs-4 control-label">{{ trans('table.catalog') }}{{ trans('table.must') }}</label>

                        <div class="col-xs-8">
                            <div style="display:inline-block;">

                                <a href="javascript:catalogSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.catalog') }}</a>
                            </div>
                            <div style="display:inline-block;" id="catalog_name">{{$course->catalog->name}}</div>
                            <input type="hidden" id="catalog_id"  name="catalog_id" value="{{$course->catalog_id}}"/>
                            {!! errors_for('catalog_id', $errors) !!}
                        </div>
                    </div>


                    <div class="form-group {!! errors_for_tag('target', $errors) !!}">
                        <label for="trget" class="col-xs-4 control-label">{{ trans('course.target') }}</label>

                        <div class="col-xs-8">
                            {!! Form::textarea('target', $course->target, ['size' => '30x4','class'=>"form-control"]) !!}
                            {!! errors_for('target', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('emailed', $errors) !!}">
                        <label for="emailed" class="col-xs-4 control-label">{{ trans('course.email') }}</label>

                        <div class="col-xs-8">
                            {!! Form::checkbox('emailed',1,$course->emailed,['class'=>'div-form-control']) !!}
                            {!! errors_for('emailed', $errors) !!}
                        </div>
                    </div>



                    <div class="form-group {!! errors_for_tag('if_send_new_user', $errors) !!}">
                        <label for="if_send_new_user" class="col-xs-4 control-label">{{ trans('course.link') }}</label>

                        <div class="col-xs-8">
                            {!! Form::checkbox('if_send_new_user',1,$course->if_send_new_user,['class'=>'div-form-control']) !!}
                            {!! errors_for('if_send_new_user', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('if_order', $errors) !!}">
                        <label for="if_order" class="col-xs-4 control-label">{{ trans('course.order') }}</label>

                        <div class="col-xs-8">
                            {!! Form::checkbox('if_order',1,$course->if_order,['class'=>'div-form-control']) !!}
                            {{ trans('course.order_log') }}
                            {!! errors_for('if_order', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('if_open_overdue', $errors) !!}">
                        <label for="if_open_overdue" class="col-xs-4 control-label">{{ trans('course.overdue') }}</label>

                        <div class="col-xs-8">
                            {!! Form::checkbox('if_open_overdue',1,$course->if_open_overdue,['class'=>'div-form-control']) !!}
                            {!! errors_for('if_open_overdue', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('language_id', $errors) !!}">
                        <label for="lanugage_id" class="col-xs-4 control-label">{{ trans('table.language') }}</label>

                        <div class="col-xs-8">
                            {!! Form::select('language_id', $languageSelect,$course->language_id,['class'=>"form-control"]) !!}
                            {!! errors_for('language_id', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="create_time" class="col-xs-4 control-label">{{ trans('table.create_time') }}</label>

                        <div class="col-xs-8">
                            <div class="div-form-control">{{$course->created_at}}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="update_time" class="col-xs-4 control-label">{{ trans('table.update_time') }}</label>

                        <div class="col-xs-8">
                            <div class="div-form-control">{{$course->created_at}}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="confirm_button" class="col-xs-4 control-label"></label>

                        <div class="col-xs-8">
                            <button class="dm3-btn dm3-btn-medium button-large" type="submit">
                                <i class="fa fa-check"></i>&nbsp;确认
                            </button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        @endsection
