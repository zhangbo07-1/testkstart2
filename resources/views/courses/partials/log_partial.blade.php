<div class="container">
    <h1 class="text-center">考试记录</h1>

    <div class="table-responsive courseTable">
        <table class="table table-striped">
            <thead>
                <th>
                    考试名
                </th>
                <th>
                    考试日期
                </th>
                <th>
                    总分
                </th>
                <th>
                    状态
                </th>
            </thead>
            <tbody>
          @if($examResults)
                @foreach($examResults as $examResult)
                    <tr>
                        <td><a href="log/{{$examResult->id}}/detailed">{{$examResult->exam->title}}</a></td>
                        <td>{{$examResult->created_at}}</td>
                        <td>{{$examResult->score}}</td>
                        @if($examResult->passed)
                            <td>通过</td>
                        @else
                            <td>失败</td>
                        @endif
                    </tr>
                @endforeach
@endif
            </tbody>
        </table>
    </div>
</div>
