@extends('layout.master')

@section('content')
    @include('courses.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection