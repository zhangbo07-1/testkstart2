<div class="leftpanel">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">
                <h5 class="sidebar-title"></h5>
                <ul class="nav nav-pills nav-stacked nav-quirk">
                    <li id="editNav">
                        <a href="/courses/{{$course->course_id}}/edit"><i class="fa fa-home"></i><span>课程属性</span></a>
                    </li>
                    @can('modify',$course)
                    <li id="combinateNav">
                        <a href="/courses/{{$course->course_id}}/teachers">
                            <i class="fa fa-user"></i><strong>{{ trans('teacher.assign') }}</strong>
                        </a>
                    </li>
                    <li id="distributeNav">
                        <a href="/courses/{{$course->course_id}}/distribute">
                            <i class="fa fa-user"></i><strong>课程分配</strong>
                        </a>
                    </li>
                    @endcan
                    <li id="uploadNav">
                        <a href="/courses/{{$course->course_id}}/upload">
                            <i class="fa fa-refresh"></i><strong>课程更新</strong>
                        </a>
                    </li>
                    <li id="previewNav">
                        <a href="/courses/{{$course->course_id}}/launch" target="_blank">
                            <i class="fa fa-eye"></i><strong>课程预览</strong>
                        </a>
                    </li>
                    <li id="examNav">
                        <a href="/courses/{{$course->course_id}}/exam">
                            <i class="fa fa-link"></i><strong>关联考试</strong>
                        </a>
                    </li>
                    <li id="surveyNav">
                        <a href="/courses/{{$course->course_id}}/evaluation">
                            <i class="fa fa-check"></i><strong>关联评估</strong>
                        </a>
                    </li>
@can('delete',$course)
                    <li>
                        <a class="deleteCourseButton red" href="#" url="/courses/{{$course->course_id}}/delete">
                            <i class="fa fa-trash"></i><strong>课程删除</strong>
                        </a>
                    </li>
          @endcan
                </ul>
            </div>
        </div>
    </div>
</div>
