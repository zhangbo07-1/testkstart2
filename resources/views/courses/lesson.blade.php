@extends('layout.master')
@section('css')

    {!! Html::style('assets/bower_components/raty/lib/jquery.raty.css') !!}
@endsection

@section('js')
    {!! Html::script('assets/bower_components/raty/lib/jquery.raty.js') !!}
    {!! Html::script('assets/js/jquery_raty.min.js') !!}
@endsection

@section('content')
    <br>
    <br>
    <div class="container">
        <aside class="lessons-container">
            <h3 class="lessons-title">进行中的课程</h3>
            <ul class="lessons-ul">
                {{--考試 icon-18--}}
    {!! errors_for('error', $errors) !!}
                @foreach($records as $record)
                    @if($record->course)
                    <li class="row">
                        <div class="col-md-6">
                            <div class="col-fixed"><span class="icon icon-0"></span></div>
                            <div class="row row-margin-5px">
                                <div class="lessons-padding ">

                                    <div>
                    <a class="lessons-course-name"
                                            href="/lessons/{{$record->course->course_id}}/info">{{ $record->course->title}}</a>
                                    </div>

                                <span class="post-date"><i class="fa fa-square orange">&nbsp;</i>进行中&nbsp;&nbsp;
                                    <i class="fa fa-clock-o">
                                        &nbsp;</i>开始时间:{{ $record->process != 0 ?  date('Y-m-d',strtotime($record->started_at)) : '尚未开始' }}</span>

                                    <div class="star-pane" course_id="{{$record->course->id}}"
                                         score="{{$record->score}}">

                                    </div>
                                    @if ($record->course->limitdays != 0)
                                        <div>
                                            截止时间: {!! getEndDate($record->created_at,$record->course->limitdays) !!}</div>

                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="lessons-padding-button">
                                {{--<a class="dm3-btn dm3-btn-medium" href="s" target="_blank">开始</a>--}}
                                <a class="dm3-btn dm3-btn-medium"
                                   href="{{ '/courses/'.$record->course->course_id.'/launch' }}" target="_blank">
                                    <i class="fa fa-caret-right"></i>&nbsp;开始</a>
                                <a class="dm3-btn dm3-btn-medium" href="/lessons/{{$record->course->course_id}}/log">
                                    <i class="fa fa-folder-o"></i>&nbsp;记录</a>

                                @if($record->course->examDistribution)
                                                     <a class="dm3-btn dm3-btn-medium" href="/lessons/{{$record->course->course_id}}/exam/{{$record->course->examDistribution->exam->exam_id}}/answer">
                                        <i class="fa fa-check-square-o"></i>&nbsp;考试</a>
                                @else
                                    <a class="dm3-btn dm3-btn-gray" disabled >
                                        <i class="fa fa-check-square-o"></i>&nbsp;考试</a>
                                @endif
                                @if($record->course->evaluationRecord)
                                    <a class="dm3-btn dm3-btn-medium grey"
                                       href="/lessons/{{$record->course->course_id}}/evaluation/{{$record->course->evaluationRecord->id}}/answer">
                                        <i class="fa fa-dot-circle-o"></i>&nbsp;评估</a>

                                                           @else
                                                            <a  class="dm3-btn dm3-btn-gray" disabled >

                                        <i class="fa fa-dot-circle-o"></i>&nbsp;评估</a>
                                @endif
                            </div>
                        </div>
                    </li>
@endif
                @endforeach

                @foreach($examRecords as $record)
                    <li class="row">
                        <div class="col-md-6">
                            <div class="col-fixed"><span class="icon icon-18"></span></div>
                            <div class="row row-margin-5px">
                                <div class="lessons-padding ">
                                    <div><a class="lessons-course-name"
                                            href="{{ '/lessons/'.$record->exam->id.'/exam' }}">{{ $record->exam->title }}</a></div>
                                <span class="post-date"><i class="fa fa-square orange">&nbsp;</i>进行中&nbsp;&nbsp;
                                    <i class="fa fa-clock-o">
                                        &nbsp;</i>开始时间:{{ $record->process != 0 ? $record->started_at : '尚未开始' }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="lessons-padding-button">
                                {{--<a class="dm3-btn dm3-btn-medium" href="s" target="_blank">开始</a>--}}
                                <a class="dm3-btn dm3-btn-medium" href="{{ '/lessons/'.$record->exam->id.'/exam' }}">
                                    <i class="fa fa-caret-right"></i>&nbsp;开始</a>
                                <a class="dm3-btn dm3-btn-medium" href="/exam-lessons/{{$record->exam->id}}/log">
                                    <i class="fa fa-folder-o"></i>&nbsp;记录</a>
                                <a class="dm3-btn  dm3-btn-gray" disabled>
                                    <i class="fa fa-check-square-o"></i>&nbsp;考试</a>
                                <a class="dm3-btn dm3-btn-gray" disabled>
                                    <i class="fa fa-dot-circle-o"></i>&nbsp;评估</a>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </aside>
    </div>

@endsection
