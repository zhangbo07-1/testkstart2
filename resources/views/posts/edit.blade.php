@extends('layout.master')
@section('content')
    @include('UMEditor.head')
    <div class="container">
        <div class="row">
            <div class="panel">


                <div class="panel-body">
                    <br>
                    <h5 style="color: #337ab7;">{{ trans('table.title') }}</h5>
                    {!! Form::open(array('url'=>'posts/'.$post->id,'method'=>'PUT', 'files'=>true)) !!}

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <input type="text" name="title" class="form-control" required="required" value="{{ $post->title }}">

                    <br>
                    <h5 style="color: #337ab7;">{{ trans('post.select_image') }}</h5>

                     <a onclick="upfile.click();" class="dm3-btn dm3-btn-medium button-large">
                                {{ trans('button.file') }}
                            </a>
                            <div style="display:inline-block;">
                                <input type="text" name="url" class="form-control"  id="fileURL">
                            </div>
                            <input type="file" name="image" id="upfile" onchange="fileURL.value=this.value" style="display:none">
                    {!! errors_for('image', $errors) !!}
                    
                    <h5 style="color: #337ab7;">{{ trans('post.summary') }}</h5>
                    <textarea name="summary" rows="1" class="form-control" required="required">{{ $post->summary }}</textarea>
                    <br>
                    <h5 style="color: #337ab7;">{{ trans('table.text') }}</h5>

                    <textarea id="myEditor"" name="body" class="form-control" style="height:240px;" required="required">{{ $post->body }}</textarea>

                    <br>
                    <br>
                    <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                    <a href="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
        
    </div>
@endsection
