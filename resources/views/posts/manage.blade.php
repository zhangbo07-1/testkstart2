@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>
            <h3>{{ trans('post.manage') }}</h3>
            <div class="row">
                <div class="col-sm-12 main">
                    <div class="courseTable text-center">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>{{ trans('table.title') }}</th>
                                    <th>{{ trans('table.author') }}</th>
                                    <th>{{ trans('table.create_time') }}</th>
                                    <th>{{ trans('table.comment_num') }}</th>
                                    <th>{{ trans('table.operation') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $post)
                                    <tr ng-repeat="trainee in trainees" class="ng-scope">
                                        <td class="vertical-align-middle"><a href="{{URL('posts/'.$post->id)}}">{{ $post->title }}</a></td>
                                        <td class="vertical-align-middle">{{ $post->user->name }}</td>
                                        <td class="vertical-align-middle">{{ $post->updated_at }}</td>
                                        <td class="vertical-align-middle">{{ count($post->comments) }}</td>

                                        <td class="vertical-align-middle">

                                            <form action="{{ URL('posts/'.$post->id) }}" method="POST" style="display: inline;">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <button type="submit" onClick="return ifDelete()" class="dm3-btn dm3-btn-medium dm3-btn-red button-large"><i class="fa fa-times"></i>&nbsp;{{ trans('button.delete') }}</button>

                                            </form>
                                        </td>
                                    </tr><!-- end ngRepeat: trainee in trainees -->
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            {!! $posts->render() !!}
        </div>
    @endsection
