@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <section class="section-content">
                <div class="container clearfix">
                    <div>
                        <div  align="right">
                            <a class="dm3-btn dm3-btn-medium button-large" onClick="returnurl()" ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>

                        </div>

                        <h1 class="entry-title" style=" text-align:center;">{{ $post->title }}</h1>

                        <div class="entry-content">
                            <h5 style="color: #337ab7;">{{ trans('post.summary') }}</h5>
                            {{$post->summary}}
                            <br>
                            <br>

                            <h5  style="color: #337ab7;">{{ trans('table.text') }}</h5>
                            <div>{!!$post->body!!} </div>
                        </div>
                        <br>
                        <footer class="post-meta">
                            <span class="author vcard">
                                <a class="fn n" rel="author">
                                    {{ $post->user->name }}</a>
                            </span>
                            <span class="post-date">
                                <time class="entry-date" datetime="2014-10-21T07:07:50+00:00">{{ $post->updated_at }}</time>
                            </span>

                            <span class="comments-link"><a>{{count($post->Comments)}}</a></span>
                        </footer>


                        <div id="comments" class="comments-area">
                            <hr>
                            @can('modify',$post)
                            <a href="{{ URL('posts/'.$post->id.'/edit') }}" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.edit') }}</a>
                            <form action="{{ URL('posts/'.$post->id) }}" method="POST" style="display: inline;">
                                <input name="_method" type="hidden" value="DELETE">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <button type="submit" onClick="return ifDelete()" class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.delete') }}</button>
                            </form>
                            @endcan
                        </div>

                        @include('posts.comments')
                    </div>
                </div>
            </section>

        </div>
 <script>
     function returnurl()
     {

         var url = document.referrer;
         if((url.indexOf("manage")>-1))
         {
             window.location.href="/posts-manage";
         }
         else
         {
             window.location.href="/posts";
             }

     }
    </script>
    @endsection
