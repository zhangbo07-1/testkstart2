@extends('layout.master')
@section('content')

    @include('UMEditor.head')

    <div class="container">
        <br>
        <br>
        <h3>{{ trans('post.create') }}</h3>
        <div class="row">
            <div class="panel">

                <div class="panel-body">


                    <h5 style="color: #337ab7;">{{ trans('table.title') }}</h5>
                    {!! Form::open(array('url'=>'posts','method'=>'POST','files'=>true))!!}
                    <input type="text" name="title" class="form-control" required="required">
                    {!! errors_for('title', $errors) !!}
                    <br>
                    <h5 style="color: #337ab7;">{{ trans('post.select_image') }}</h5>

                    <a onclick="upfile.click();" class="dm3-btn dm3-btn-medium button-large">
                                {{ trans('button.file') }}
                            </a>
                            <div style="display:inline-block;">
                                <input type="text" name="url" class="form-control" id="fileURL">
                            </div>
                            <input type="file" name="image" id="upfile" onchange="fileURL.value=this.value" style="display:none">
                    {!! errors_for('image', $errors) !!}
                    
                    <h5 style="color: #337ab7;">{{ trans('post.summary') }}</h5>
                    <textarea name="summary" rows="1" class="form-control" required="required"></textarea>
                    {!! errors_for('summary', $errors) !!}

                    <br>
                    <h5 style="color: #337ab7;">{{ trans('table.text') }}</h5>
                    {!! errors_for('body', $errors) !!}
                    <textarea name="body" class="form-control" style="height:240px;" required="required" id='myEditor'></textarea>
                    <br>
                    <button class="dm3-btn dm3-btn-medium button-large" onClick="return checkinput()">{{ trans('button.submit') }}</button>
                    <a href="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
    <script>
     function checkinput(e)
     {

         if(!UM.getEditor('myEditor').hasContents())
         {
             alert("{{ trans('error.text_can_not_empty') }}");
             return false;

         }
         return true;
     }
    </script>
@endsection
