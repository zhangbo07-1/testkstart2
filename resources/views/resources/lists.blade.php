@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <br>
        <br>
        <div class="container">
            <aside class="lessons-container">
                <h3>{{ trans('resource.name') }}</h3>
                <div class="row">
                    <div class="col-sm-12 main">
                        <form class="form-inline" action="/resources">
                            <div style="display:inline-block;">

                                <a href="javascript:catalogSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.catalog') }}</a>
                            </div>
                            <div style="display:inline-block;" id="catalog_name"></div>
                            <input type="hidden" id="catalog_id"  name="catalog_id" value=""/>
                            <button type="submit" class="dm3-btn dm3-btn-medium button-large">
                                <i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}
                            </button>
                        </form>

                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 main">
                        <ul class="lessons-ul">
                            @foreach($resources as $resource)
                                <li class="row">
                                    <div class="col-md-6">
                                        <table class="no-border">
                                            <tr>
                                                <td width="60px">
                                                    <img src="{!!get_url()!!}/img/resource_logo.png">
                                                </td>
                                                <td class="row-margin-5px">
                                                    <div class="lessons-padding ">
                                                        <div><a class="lessons-course-name">{{$resource->name}}</a>

                                                            <div class="post-date">{{ trans('resource.file') }}:&nbsp;{{$resource->filename}}
                                                            </div>
                                                            <div>{{ trans('resource.size') }}:&nbsp;{{$resource->size/1000}}KB</div>
                                                            <div>
                                                                <i class="fa fa-clock-o">
                                                                    &nbsp;{{ trans('table.create_time') }}:&nbsp;</i>{{ date('Y-m-d H:i', strtotime($resource->updated_at)) }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                        
                                    </div>
                                    <div class="col-md-6">
                                        <div class="lessons-padding-button text-center">
                                            <a href="{{ url('resources/'.$resource->id.'/download') }}" class="dm3-btn dm3-btn-medium button-large">
                                                <i class="fa fa-download"></i>&nbsp;{{ trans('button.download') }}</a>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                        {!! $resources->render() !!}
                    </div>
                </div>
            </aside>
        </div>
    @endsection
