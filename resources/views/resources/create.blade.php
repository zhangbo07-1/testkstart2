@extends('layout.master')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>
            
            <h3>{{ trans('resource.create') }}</h3>
            <div class="entry-content">
                <div class="clearfix">
                    
                    {!! Form::open(array('url'=>'resources-manage','method'=>'POST', 'files'=>true,'class' => 'form-horizontal')) !!}
                    <br>
                    
                    <div class="form-group {!! errors_for_tag('name', $errors) !!}">
                        <label  class="col-xs-5 control-label">{{ trans('resource.title') }}:</label>
                        <div class="col-xs-7">
                            <div style="display:inline-block;">
                        {!! Form::text("name",null,array('required' => 'required','class'=>"form-control")) !!}
                                </div>
                            {!! errors_for('name', $errors) !!}
                            
                        </div>
                    </div>
                    <div class="form-group {!! errors_for_tag('file', $errors) !!}">
                        <label class="col-xs-5 control-label">{{ trans('table.select_file') }}:</label>
                        <div  class="col-xs-7">
                            <a onclick="upfile.click();" class="dm3-btn dm3-btn-medium button-large">
                                {{ trans('button.file') }}
                            </a>
                            <div style="display:inline-block;">
                                <input type="text" name="url" class="form-control" id="fileURL">
                            </div>
                            <input type="file" name="file" id="upfile" onchange="fileURL.value=this.value" style="display:none">
                            {!! errors_for('file', $errors) !!}
                        </div>
                    </div>
                    
                    <div class="form-group {!! errors_for_tag('manage_id', $errors) !!}">
                        <label class="col-xs-5 control-label">{{ trans('table.range') }}:</label>
                        <div class="col-xs-7">
                            <div style="display:inline-block;">
                                <a href="javascript:manageSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.range') }}</a>
                            </div>
                            <div style="display:inline-block;" id="manage_name">{{$manages}}</div>
                            <input type="hidden" id="manage_id"  name="manage_id" value=""/>
                            {!! errors_for('manage_id', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('catalog_id', $errors) !!}">
                        <label class="col-xs-5 control-label">{{ trans('table.catalog') }}:</label>
                        <div class="col-xs-7">
                            <div style="display:inline-block;">
                                
                                <a href="javascript:catalogSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.catalog') }}</a>
                            </div>
                            <div style="display:inline-block;" id="catalog_name"></div>
                            <input type="hidden" id="catalog_id"  name="catalog_id" value=""/>
                            {!! errors_for('ctalog_id', $errors) !!}
                        </div>
                    </div>
                    <div id="loading" align="center">
                    </div>
                    {!! errors_for('success', $errors) !!}
                    <br>
                    <div class="form-group">
                        <label for="confirm_button" class="col-xs-5 control-label"></label>
                        <div class="col-xs-7">
                            
                            <button class="dm3-btn dm3-btn-medium button-large" onClick="showLoad()">{{ trans('button.submit') }}</button>
                            <a href="{{ URL('resources-manage') }}" class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.return') }}</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    @endsection
