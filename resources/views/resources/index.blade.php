@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>
            <h3>{{ trans('resource.manage') }}</h3>
            <div class="row">
                <div class="col-sm-12 main">

                    <form class="form-inline" action="/resources-manage">
                        <div style="display:inline-block;">

                            <a href="javascript:catalogSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.catalog') }}</a>
                        </div>
                        <div style="display:inline-block;" id="catalog_name"></div>
                        <input type="hidden" id="catalog_id"  name="catalog_id" value=""/>
                        <br>
                        <div style="float: left;">
                            
                            <button type="submit" class="dm3-btn dm3-btn-medium button-large">
                                <i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}
                            </button>
                        </div>

                        <div style="float: right;">
                            <a href="{{URL('resources-manage/create')}}" class="dm3-btn dm3-btn-medium button-large"><i
                                    class="fa fa-plus"></i>&nbsp;{{ trans('resource.create') }}</a>
                        </div>

                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 main">
                    <div class="table-responsive">
                        <div class="courseTable text-center">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{ trans('resource.title') }}</th>
                                        <th>{{ trans('resource.file') }}</th>
                                        <th>{{ trans('resource.size') }}</th>
                                        <th>{{ trans('table.create_time') }}</th>
                                        <th>{{ trans('table.operation') }}</th>
                                    </tr>
                                </thead>
                                <tbody>


                                    @foreach($resources as $resource)


                                        <tr>
                                            <td class="vertical-align-middle">{{ $resource->name }}</td>
                                            <td class="vertical-align-middle">{{ $resource->filename }}</td>
                                            <td class="vertical-align-middle">{{ $resource->size/1000}}KB</td>
                                            <td class="vertical-align-middle">{{ $resource->updated_at }}</td>
                                            <td class="vertical-align-middle">

                                                
                                                <a href="{{ url('resources/'.$resource->id.'/download') }}" class="dm3-btn dm3-btn-medium button-large">
                                                    <i class="fa fa-download"></i>&nbsp;{{ trans('button.download') }}</a>
                                                <form action="{{ URL('resources-manage/'.$resource->id) }}" method="POST"
                                                      style="display: inline;">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button type="submit" onClick="return ifDelete()"
                                                            class="dm3-btn dm3-btn-medium dm3-btn-red button-large"><i
                                                        class="fa fa-times"></i>&nbsp;{{ trans('button.delete') }}
                                                    </button>

                                                </form>

                                            </td>
                                        </tr><!-- end ngRepeat: trainee in trainees -->
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        {!! $resources->render() !!}
                    </div>
                </div>
            </div>
            
        </div>
    @endsection


