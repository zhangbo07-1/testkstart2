<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />
    {!! Html::style('assets/style.css') !!}
    {!! Html::style('assets/css/font-awesome.css') !!}
    {!! Html::style('assets/css/owl.carousel.css') !!}
    {!! Html::style('assets/css/owl.theme.css') !!}
    {!! Html::style('assets/css/owl.transitions.css') !!}
    {!! Html::style('assets/css/editor-style.css') !!}
    {!! Html::style('assets/css/woocommerce.css') !!}
    {!! Html::style('assets/css/magnific-popup.css') !!}
    {!! Html::style('assets/css/flexslider.css') !!}
    {!! Html::style('assets/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css') !!}
    {!! Html::style('css/animate.css') !!}
    {!! Html::style('assets/bower_components/fontawesome/css/font-awesome.css') !!}
    {!! Html::style('css/custom.css') !!}
    {!! Html::style('css/bootstrap-pagination.css') !!}
    {!! Html::style('css/bootstrap_form.css') !!}
    @yield('css')
    {!! Html::style('assets/css/app.css') !!}
</head>
<body class="archive post-type-archive post-type-archive-ib_educator_course has-toolbar">
<div id="page-container">
@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
@endsection

@section('subTemplateJs')
    {!! Html::script('assets/js/exam.min.js') !!}
@endsection

@section('content')


    <div class="leftpanel" style="top:0px">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">            
                <table>
                    <thead>
                        <th>
                            问题
                        </th>

                        <th>
                            标记
                        </th>

                    </thead>

                    <tbody>

                        @for($i = 0;$i < count($examResult->questionResult); $i++)
                            <tr>
                                <td><a href="{{$examResult->questionResult[$i]->question->id}}">题目{{$i+1}}</td>
                                    
                                    @if($examResult->questionResult[$i]->ture == 1)
                                        <td><a style="color: #00FF00"><strong>
                                            √
                                        </strong></a></td>
                                    @else
                                        <td><a style="color: #FF0000"><strong>
                                            X
                                        </strong></a></td>
                                    @endif

                                    

                            </tr>
                        @endfor
                            <tr>
                                <td></td>
 
                                        <td></td>

                            </tr>
                    </tbody>
                </table>
</div>
</div>
</div>
        </div>
    <div class="mainpanel">
        @yield('questionlay')
    </div>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}" />
    {!! Html::style('assets/style.css') !!}
    {!! Html::style('assets/css/font-awesome.css') !!}
    {!! Html::style('assets/css/owl.carousel.css') !!}
    {!! Html::style('assets/css/owl.theme.css') !!}
    {!! Html::style('assets/css/owl.transitions.css') !!}
    {!! Html::style('assets/css/editor-style.css') !!}
    {!! Html::style('assets/css/woocommerce.css') !!}
    {!! Html::style('assets/css/magnific-popup.css') !!}
    {!! Html::style('assets/css/flexslider.css') !!}
    {!! Html::style('assets/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css') !!}
    {!! Html::style('css/animate.css') !!}
    {!! Html::style('assets/bower_components/fontawesome/css/font-awesome.css') !!}
    {!! Html::style('css/custom.css') !!}
    {!! Html::style('css/bootstrap-pagination.css') !!}
    {!! Html::style('css/bootstrap_form.css') !!}
    @yield('css')
    {!! Html::style('assets/css/app.css') !!}
</head>
<body class="archive post-type-archive post-type-archive-ib_educator_course has-toolbar">
<div id="page-container">