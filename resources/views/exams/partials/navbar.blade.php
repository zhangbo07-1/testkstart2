<div class="leftpanel">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">
                <h5 class="sidebar-title"></h5>
                <ul class="nav nav-pills nav-stacked nav-quirk">
                    <li id="editNav">
                        <a href="/exams/{{$exam->exam_id}}/edit"><i class="fa fa-home"></i><span>考试属性</span></a>
                    </li>

                    <li id="combinateNav">
                        <a href="/exams/{{$exam->exam_id}}/combinate">
                            <i class="fa fa-book"></i><strong>组卷</strong>
                        </a>
                    </li>
                    <li id="combinateNav">
                        <a href="/exams/{{$exam->exam_id}}/teachers">
                            <i class="fa fa-user"></i><strong>{{ trans('teacher.assign') }}</strong>
                        </a>
                    </li>
                    <li id="previewNav">
                        <a href="/exams/{{$exam->exam_id}}/preview">
                            <i class="fa fa-book"></i><strong>预览</strong>
                        </a>

                    </li>
                    @can('modify',$exam)
                    @if ($exam->if_link)
                        <li id="distributeNav">
                            <a disabled>
                                <i class="fa fa-user gray"></i><strong class="gray">考试分配</strong>
                            </a>
                        </li>
                        
                    @else
                        <li id="distributeNav">
                            <a href="/exams/{{$exam->exam_id}}/distribute">
                                <i class="fa fa-user"></i><strong>考试分配</strong>
                            </a>
                        </li>
                    @endif
                    @endcan
                    <li id="scoreNav">
                        <a href="/exams/{{$exam->exam_id}}/mark">
                            <i class="fa fa-user"></i><strong>阅卷</strong>
                        </a>
                    </li>
          @can('delete',$exam)
                    <li>
                        <a class="deleteExamButton red" href="#" url="/exams/{{$exam->exam_id}}/delete">
                            <i class="fa fa-trash"></i><strong>考试删除</strong>
                        </a>
                    </li>
          @endcan
                </ul>
            </div>
        </div>
    </div>
</div>
