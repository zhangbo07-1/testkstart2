@extends("exams.partials.template")

@section('container')
    <div class="contentpanel">


     <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{$exam->title}}</h2>
            </div>

            
        </div>
        <div class="panel panel-announcement">
            <div class="panel-body">
                <div class="row">
                    
                    <div class="col-sm-12 main">
                        <div class="table-responsive">
                            <div class="courseTable text-center">
    <br>
                                <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:100%;">
                                    <table class="table table-striped table-bordered text-center">
                                        <thead>
                                            <tr>
                                                <th>{{ trans('question.title') }}</th>
                                                
                                                <th>{{ trans('table.operation') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($results as $result)
                                                <tr ng-repeat="trainee in trainees" class="ng-scope">
                                                    <td class="vertical-align-middle">{!! $result->question->title !!}</td>

                                                    <td class="vertical-align-middle">
                                                        <a href="{{ url('exams/'.$exam->exam_id.'/result/'.$result->id.'/mark') }}"  class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-pencil"></i>&nbsp;{{ trans('button.mark') }}</a>
                                                       
                                                    </td>
                                                </tr><!-- end ngRepeat: trainee in trainees -->
                                            @endforeach
                                        </tbody>
                                    </table>
<br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    
@endsection
