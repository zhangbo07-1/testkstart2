@extends('app')
@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
    {!! Html::style('assets/bower_components/raty/lib/jquery.raty.css') !!}
@endsection

@section('js')
    {!! Html::script('assets/bower_components/raty/lib/jquery.raty.js') !!}
@endsection

@section('content')
    <div class="container">
        <br>
        <br>

        <div align="right">
            <a class="dm3-btn dm3-btn-medium" href="javascript:history.back(-1)"><i class="fa fa-reply"></i>&nbsp;返回</a>
        </div>
        @include('courses.partials.log_partial')
    </div>
@endsection