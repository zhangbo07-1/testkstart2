@extends('exams.partials.answer_template')

@section('questionlay')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{$examResult->exam->title}}</h2>
            </div>
        </div>
        <div class="panel panel-announcement">
            <div class="panel-body">

                <div>
                    {!!$question->title!!}
                    <br>
                    <table class="no-border">

                        
                        @for($i = 1;$i <= count($question->select);$i++)

                            <tr>
                                <td>
                                    @if($question->select[$i-1]->ture_id == 1)
                                        <a style="color: #00FF00"><strong>
                                            √
                                        </strong></a>

                                    @else
                                        <a style="color: #FF0000"><strong>
                                            X
                                        </strong></a>                                                            
                                    @endif
 @if($question->type == 2)
                                                      <input type="checkbox"/>
                                                @else
                                                          <input type="radio" />
                                                @endif
                                </td>
                                <td>
                                    {{$question->select[$i-1]->title}}" 
                                </td>
                            </tr>

                        @endfor
                    </table>
                    <br>
                </div>


            </div>
        </div>
    </div>
@endsection
