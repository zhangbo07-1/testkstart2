@extends('exams.partials.template')

@section('container')
    <div class="contentpanel">

        <div class="panel panel-announcement">
            <div class="panel-body">
                <div class="col-md-10 col-md-offset-1">
                    <a>{{ trans('question.title') }}</a>
                    <form action="{{ URL('/exams/'.$exam->exam_id.'/questionResult/'.$result->id.'/marking/') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        {!! $result->question->title !!}

                        <a>{{ trans('question.answer') }}</a>
                        <br>
                        {{$result->answer}}
                        <br>
                        <br>
                        <br>


                        0：差 ～～5：优
                        <br>
                        @for( $i = 0;$i<6;$i++)

                            <input type="radio" id="select" name="mark" value="{{$i}}"/>
                            {{$i}}
                            &nbsp;&nbsp;&nbsp;
                        @endfor
                        <br>
                        <br>
                        
                        <button  class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                        <a href="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>

                    </form>

                </div>
            </div>
        </div>
    </div>
    </div>

@endsection
