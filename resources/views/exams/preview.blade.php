
@extends('exams.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
@section('container')


     <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{$exam->title}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">

                    <div class="panel-body">
                        <br>
                        @foreach($questions as $index=>$question)
                            <div class="one" style="border-top: solid 20px #efefef;">
                                <h5 style="color: #337ab7;">{{$index+1}}.{!!$question->title!!}</h5>
                               @include('questions.select_show')
                                
                            </div>

                        @endforeach
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

