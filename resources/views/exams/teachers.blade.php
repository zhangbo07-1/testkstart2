@extends('exams.partials.template')

@section('js')

    {!! Html::script('assets/bower_components/handlebars/handlebars.min.js') !!}
    {!! Html::script('assets/bower_components/jquery-bootpag/lib/jquery.bootpag.min.js') !!}
    {!! Html::script('assets/bower_components/spin.js/spin.js') !!}
    {!! Html::script('assets/js/exam_teacher.js') !!}
@endsection

@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{$exam->title}}</h2>
            </div>

            <div class="panel-body">
                <div class="form-inline">
                    <input id="searchWord" class="form-control" type="text" name="findByUsername" placeholder="{{ trans('table.input_select') }}">
                    <button id="searchButton" type="submit" class="dm3-btn dm3-btn-medium button-large">
                        <i class="fa fa-search"></i>&nbsp;{{ trans('button.search') }}
                    </button>
                </div>
            </div>
        </div>

        <div class="panel panel-announcement">
            <div class="courseTable">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">{{ trans('teacher.for_assign') }}</h4>
                    </div>
                    <div class="panel-body text-center">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th><input id="unCheckUsersButton" type="checkbox"></th>
                                    <th>{{ trans('user.title') }}</th>
                                    <th>{{ trans('user.real_name') }}</th>
                                    <th>{{ trans('user.department') }}</th>
                                    <th>{{ trans('user.role') }}</th>
                                </tr>
                            </thead>
                            <tbody id="usersNotCheckedForm">
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-body">
                        <div id="usersNotCheckedForm-Pagination"></div>
                        <button id="notCheckUsersSubmit" class="dm3-btn dm3-btn-medium button-large">
                            <i class="fa fa-thumbs-o-up"></i>&nbsp;{{ trans('button.submit') }}
                        </button>
                    </div>

                </div>
            </div>
        </div>

        <div class="panel panel-announcement">
            <div class="courseTable">
                <div class="panel">
                    <div class="panel-heading">
                        <h4 class="panel-title">{{ trans('teacher.assigned') }}</h4>
                    </div>
                    <div class="panel-body text-center">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th><input id="checkUsersButton" type="checkbox"></th>
                                    <th>{{ trans('user.title') }}</th>
                                    <th>{{ trans('user.real_name') }}</th>
                                    <th>{{ trans('user.department') }}</th>
                                    <th>{{ trans('user.role') }}</th>
                                </tr>
                            </thead>
                            <tbody id="usersCheckedForm"></tbody>
                        </table>
                    </div>
                    <div class="panel-body">
                        <div id="usersCheckedForm-Pagination"></div>
                        <button id="checkUsersSubmit" class="dm3-btn dm3-btn-medium dm3-btn-red button-large"><i class="fa fa-times"></i>&nbsp;{{ trans('button.delete') }}</button>
                    </div>

                </div>
            </div>
        </div>
        <script id="userCheckedTemplate" type="text/x-handlebars-template">
            @{{#each data}}
            <tr>
                <td><input class="checkUsers" type="checkbox" name="user[]"
                           id="@{{id}}" value="@{{id}}"></td>
                  <td>@{{name}}</td>
                <td>@{{profile.realname}}</td>
                <td>@{{department.name}}</td>
                <td>@{{role.name}}</td>
            </tr>
            @{{/each}}
        </script>

        <script id="userNotCheckedTemplate" type="text/x-handlebars-template">
            @{{#each data}}
            <tr>
                <td><input class="unCheckUsers" type="checkbox" name="user[]"
                           id="@{{id}}" value="@{{id}}"></td>
                 <td>@{{name}}</td>
                <td>@{{profile.realname}}</td>
                <td>@{{department.name}}</td>
                <td>@{{role.name}}</td>
            </tr>
            @{{/each}}
        </script>
        <script>
         var exam_id = {{$exam->id}};
        </script>
@endsection
