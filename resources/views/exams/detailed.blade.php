@extends('app')
@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
    {!! Html::style('assets/bower_components/raty/lib/jquery.raty.css') !!}
@endsection

@section('js')
    {!! Html::script('assets/bower_components/raty/lib/jquery.raty.js') !!}
@endsection

@section('content')
    <div class="container">
        <h1 class="text-center">详细信息</h1>

        <h4>考试名称：{{$questionResults[0]->examResult->exam->title}}</h4>

        <div align="right">
            @if($questionResults[0]->examResult->exam->limit_times == 1)
                <a class="dm3-btn dm3-btn-medium" href="answer/{{$questionResults[0]->question->id}}" target="_blank">
                    查看答案
                </a>
            @else
                <a class="dm3-btn dm3-btn-gray" disabled target="_blank">
                    查看答案
                </a>
            @endif
            <a class="dm3-btn dm3-btn-medium" href="javascript:history.back(-1)"><i class="fa fa-reply"></i>&nbsp;返回</a>
        </div>
        <div class="table-responsive courseTable">

            <table class="table table-striped">
                <thead>
                <th>
                    问题编号
                </th>
                <th>
                    问题文本
                </th>
                <th>
                    标记
                </th>
                <th>
                    得分
                </th>
                </thead>

                <tbody>

                @foreach($questionResults as $questionResult)
                    <tr>
                        <td>{{$questionResult->question->bank_id}}_{{$questionResult->question->id}}</a></td>
                        <td>{!! $questionResult->question->title !!}</td>
                        @if($questionResult->ture == 1)
                            <td><a style="color: #00FF00"><strong>
                                        √
                                    </strong></a></td>
                            <td>{{round(100/count($questionResults))}}</td>
                        @else
                            <td><a style="color: #FF0000"><strong>
                                        X
                                    </strong></a></td>
                            <td>0</td>
                        @endif


                    </tr>
                @endforeach
                </tbody>
            </table>
            {!! $questionResults->render() !!}
        </div>

@endsection
