@extends('exams.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{$exam->title}}</h2>
            </div>
        </div>
        <div class="panel panel-announcement">
            <div class="panel-body" style="width: 700px;">

                {!! Form::open(array('url'=>'exams/'.$exam->id,'method'=>'PUT', 'class' => 'form-horizontal length-750px')) !!}

                 <div class="form-group {!! errors_for_tag('exam_id', $errors) !!}">
                        <label for="exam_id" class="col-xs-4 control-label">{{ trans('exam.id') }}{{ trans('table.must') }}</label>

                        <div class="col-xs-8">
                            {!! Form::text("exam_id",$exam->exam_id,array('class' => 'form-control ','required' => 'required')) !!}
                            {!! errors_for('exam_id', $errors) !!}
                        </div>
                    </div>


                    <div class="form-group {!! errors_for_tag('title', $errors) !!}">
                        <label for="title" class="col-xs-4 control-label">{{ trans('exam.title') }}{{ trans('table.must') }}</label>

                        <div class="col-xs-8">
                            {!! Form::text('title', $exam->title, ['class'=>"form-control","required"=>"required"]) !!}
                            {!! errors_for('title', $errors) !!}
                        </div>
                    </div>


                    <div class="form-group {!! errors_for_tag('description', $errors) !!}">
                        <label for="description" class="col-xs-4 control-label">{{ trans('exam.description') }}</label>

                        <div class="col-xs-8">
                            {!! Form::textarea('description', $exam->description, ['size' => '30x4','class'=>"form-control"]) !!}
                            {!! errors_for('description', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('exam_time', $errors) !!}">
                        <label for="exam_time" class="col-xs-4 control-label">{{ trans('table.limit_data') }}</label>

                        <div class="col-xs-8">
                            <div class="form-inline margin-left-10px">
                                <div class="form-group">
                                    <div class="input-group"  style="width:220px;">
                                        {!! Form::text("exam_time",$exam->exam_time,array('class' => 'form-control text-right')) !!}
                                        <div class="input-group-addon">{{ trans('table.minute') }}</div>
                                    </div>
                                </div>
                            </div>
                            {!! errors_for('exam_time', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('limit_times', $errors) !!}">
                        <label for="limit_times" class="col-xs-4 control-label text-right">{{ trans('table.limit_times') }}</label>

                        <div class="col-xs-8">
                            {!! Form::text("limit_times",$exam->limit_times,array('class' => 'form-control width-168px text-right')) !!}

                            {!! errors_for('limit_times', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('pass_score', $errors) !!}">
                        <label for="pass_score" class="col-xs-4 control-label">{{ trans('exam.pass_score') }}</label>

                        <div class="col-xs-8">
                            {!! Form::text("pass_score",$exam->pass_score,array('class' => 'form-control width-168px text-right')) !!}
                            {!! errors_for('pass_score', $errors) !!}
                        </div>
                    </div>
                    <div class="form-group {!! errors_for_tag('manage_id', $errors) !!}">
                        <label for="manage_id" class="col-xs-4 control-label">{{ trans('table.range') }}</label>

                        <div class="col-xs-8">
                            <div style="display:inline-block;">

                                <a href="javascript:manageSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.range') }}</a>
                            </div>
                            <div style="display:inline-block;" id="manage_name">{{$manages}}</div>
                            <input type="hidden" id="manage_id"  name="manage_id" value=""/>
                            {!! errors_for('manage_id', $errors) !!}
                        </div>
                    </div>
                    <div class="form-group"{!! errors_for_tag('catalog_id', $errors) !!}">
                        <label for="catalog_id" class="col-xs-4 control-label">{{ trans('table.catalog') }}</label>

                        <div class="col-xs-8">
                            <div style="display:inline-block;">

                                <a href="javascript:catalogSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.catalog') }}</a>
                            </div>
                            <div style="display:inline-block;" id="catalog_name">{{$exam->catalog->name}}</div>
                            <input type="hidden" id="catalog_id"  name="catalog_id" value="{{$exam->catalog_id}}"/>
                            {!! errors_for('catalog_id', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('emailed', $errors) !!}">
                        <label for="emailed" class="col-xs-4 control-label">{{ trans('exam.email') }}</label>

                        <div class="col-xs-8">
                            {!! Form::checkbox('emailed',1,$exam->emailed,['class'=>'div-form-control']) !!}
                            {!! errors_for('emailed', $errors) !!}
                        </div>
                    </div>

                    <div class="form-group {!! errors_for_tag('if_link', $errors) !!}">
                        <label for="if_link" class="col-xs-4 control-label">{{ trans('exam.link') }}</label>

                        <div class="col-xs-8">
                            {!! Form::checkbox('if_link',1,$exam->if_link,['class'=>'div-form-control']) !!}
                            <span>{{ trans('exam.link_log') }}</span>
                            {!! errors_for('if_link', $errors) !!}
                        </div>
                    </div>

                <div class="form-group">
                        <label for="create_time" class="col-xs-4 control-label">{{ trans('table.create_time') }}</label>

                        <div class="col-xs-8">
                            <div class="div-form-control">{{$exam->created_at}}</div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="update_time" class="col-xs-4 control-label">{{ trans('table.update_time') }}</label>

                        <div class="col-xs-8">
                            <div class="div-form-control">{{$exam->created_at}}</div>
                        </div>
                    </div>


               <div class="form-group">
                        <label for="confirm_button" class="col-xs-4 control-label"></label>

                        <div class="col-xs-8">
                            <button class="dm3-btn dm3-btn-medium button-large" type="submit">
                                <i class="fa fa-check"></i>&nbsp;确认
                            </button>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection
