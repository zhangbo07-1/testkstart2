@extends('app')
@section('content')


    <div class="container">
        <br>
        <br>
        <h3>预览考题</h3>
        <br>
        <br>
        @include('partials.layout.errors')
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">

                    <div class="panel-body">
                        
                        @include('partials.layout.errors')
                        <ol>
                            @foreach($questions as $question)
                                <div class="one" style="border-top: solid 20px #efefef; padding: 5px 20px;">
                                    <li>题目</li>
                                    {!!$question->title!!}
                                    <table border="0px">

                                        @for( $i = 0;$i<count($question->select);$i++)
                                            <tr>
                                                @if($question->type == 2)
                                                    <td>     <input type="checkbox" name="select{{$question->select[$i]->id}}" value="yes" />
                                                @else
                                                        <td>     <input type="radio" name="question{{$question->id}}" value="{$question->select[$i]->id{}}" />
                                                @endif
                                                <a> {{$question->select[$i]->title}}</a>
                                                        </td>
                                            </tr>
                                        @endfor

                                    </table>

                                </div>

                            @endforeach
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

