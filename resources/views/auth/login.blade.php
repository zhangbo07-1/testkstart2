@extends('layout.master')
@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
@endsection

@section('content')
    <div class="container">
        <div class="entry-content">
            <div id="page-title">
                <br>

                <h1 class="entry-title">登录</h1>
            </div>
            <div id="auth-forms">
                {!! Form::open(['id' => 'loginForm' ,'route' => 'login', 'class' => 'input']) !!}
                <div class="form-group {!! errors_for_tag('email', $errors) !!}">
                    <label class="" for="exampleInputAmount">邮箱</label>
                    @if(0)

                        <div class="input-group">
                            <input class="form-control" id="loginEmailPrefix" type="text" value="" placeholder="邮箱前缀">
                            <span class="input-group-addon">@</span>
                            {!! Form::select('emailType', $emailTypeSelect,null,array('id' => 'loginEmailDomain','class'=>'form-control no-radius-left-top')) !!}
                        </div>
                        {!! errors_for('email', $errors) !!}
                        <input id="loginEmail" class="form-control" name="email" type="hidden" value="">
                    @else
                        {!! errors_for('email', $errors) !!}
                        <input id="loginEmail" class="form-control" name="email"  placeholder="邮件地址">
                    @endif
                </div>


                <div class="form-group {!! errors_for_tag('password', $errors) !!}">
                    <label>密码</label>
                    {!! Form::password('password', ['class'=> 'form-control','placeholder' => '密码6~20位']) !!}
                    {!! errors_for('password', $errors) !!}
                </div>

                <div class="checkbox">
                    <label><input name="remember" type="checkbox"> 记住我</label>
                </div>
                <div>
                    {!! Form::submit('登录',['class' => 'dm3-btn dm3-btn-medium button-large']) !!}
 <a class="dm3-btn dm3-btn-medium button-large" href="#">忘记密码</a>
                   {{-- <a class="dm3-btn dm3-btn-medium button-large" href="/register">注册</a>--}}
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! Html::script('assets/js/auth.min.js') !!}
@endsection
