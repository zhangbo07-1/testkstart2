@extends('layout.master')
@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
@endsection

@section('content')
    <div class="container">
        <div class="entry-content">
            <div id="page-title">
                <br>

                <h1 class="entry-title">新用户注册</h1>
            </div>
            <div id="auth-forms">

                {!! Form::open(['id' => 'registerForm', 'route' => 'register', 'class' => 'form']) !!}

                <div class="form-group  {!! errors_for_tag('name', $errors) !!}">
                    <label>用户名</label>
                    <input type="text" class="form-control" name="name" value="" placeholder="用户名长度6~20位">
                    {!! errors_for('name', $errors) !!}
                </div>

                <div class="form-group  {!! errors_for_tag('email', $errors) !!}">
                    <label class="" for="exampleInputAmount">邮箱</label>
@if(count($emailTypeSelect))
                    <div class="input-group">
                        <input class="form-control" id="registerEmailPrefix" type="text" value="" placeholder="邮箱前缀">
                        <span class="input-group-addon">@</span>
                        {!! Form::select('emailType', $emailTypeSelect,null,array('id' => 'registerEmailDomain','class'=>'form-control no-radius-left-top')) !!}
                    </div>
                    {!! errors_for('email', $errors) !!}
                </div>

                <input id="registerEmail" class="form-control" name="email" type="hidden" value="">
@else
                 {!! errors_for('email', $errors) !!}
                 <input id="registerEmail" class="form-control" name="email" type="text" placeholder="邮件地址">
                </div>   
@endif
                <div class="form-group {!! errors_for_tag('password', $errors) !!}">
                    <label>密码</label>
                    {!! Form::password('password', ['class'=> 'form-control','placeholder'=>'密码长度6~20位']) !!}
                    {!! errors_for('password', $errors) !!}
                </div>

                <div class="form-group {!! errors_for_tag('password', $errors) !!}">
                    <label>密码确认</label>
                    {!! Form::password('password_confirmation', ['class'=> 'form-control','placeholder'=>'密码长度6~20位']) !!}
                </div>

                <div>
                    {!! Form::submit('注册',['class' => 'dm3-btn dm3-btn-medium button-large']) !!}

                    <a href="/manual/registration" target="_blank">遇到问题?</a>
                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    {!! Html::script('assets/js/auth.min.js') !!}
@endsection


