<div class="leftpanel">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">
                <h5 class="sidebar-title"></h5>
                <nav>
                    <ul class="nav nav-pills nav-stacked nav-quirk">
                        <li>
                            <a href="/log-manage/login">
                                <i class="fa fa-user"></i><span>{{ trans('log.login') }}</span>
                            </a>
                        </li>
                        <li>
                            <a href="/log-manage/email">
                                <i class="fa fa-envelope"></i><span>{{ trans('log.email') }}</span>
                            </a>
                        </li>
                        
                        
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
