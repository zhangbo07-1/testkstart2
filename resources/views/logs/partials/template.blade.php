@extends('layout.master')


@section('content')
    @include('logs.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection