@extends('logs.partials.template')


@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{ trans('log.email') }}</h2>
            </div>
        </div>

        <div class="panel panel-announcement">
            <div class="panel-body">
                <div class="clearfix">
                    <div  style="display:inline-block;">
                        <form class="form-inline" action="/log-manage/email">
                            <div class="form-group"> 
                                <input type="text" name="findByUsername" placeholder="{{ trans('table.input_select_user') }}" class="form-control">
                            </div>
                            <div class="form-group"> 
                                <input type="text" name="findByEmailtitle" placeholder="{{ trans('table.input_select_email') }}" class="form-control">
                            </div>
                            <button type="submit" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.search') }}</button>
                            
                        </form>
                    </div>
                    
                </div>
                <div class="row">
                    <div class="col-sm-11 main">
                        <div class="table-responsive">
                            <div class="courseTable text-center">
                                <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:100%;">
                                    <table id="table" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>{{ trans('table.user_name') }}</th>
                                                <th>{{ trans('table.email') }}</th>
                                                <th>{{ trans('table.type') }}</th>
                                                <th>{{ trans('table.data') }}</th>
                                                <th>{{ trans('table.status') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($logs as $emailLog)
                                                <tr>
                                                    <td class="vertical-align-middle">{{$emailLog->username}}</td>
                                                    <td class="vertical-align-middle">{{$emailLog->useremail}}</td>
                                                    <td class="vertical-align-middle">{{$emailLog->emailTitle}}</td>
                                                    <td class="vertical-align-middle">{{$emailLog->updated_at}}</td>
                                                    @if($emailLog->status == 2)
                                                        <td class="vertical-align-middle">{{ trans('table.success') }}</td>
                                                    @elseif($emailLog->status == 1)
                                                        <td class="vertical-align-middle">{{ trans('table.fail') }}</td>
                                                    @elseif($emailLog->status == 0)
                                                        <td class="vertical-align-middle">{{ trans('table.sending') }}</td>
                                                    @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {!! $logs->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
