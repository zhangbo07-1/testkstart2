@extends('logs.partials.template')
@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{ trans('log.login') }}</h2>
            </div>
        </div>

        <div class="panel panel-announcement">
            <div class="panel-body">
                <div class="clearfix">
                    <div  style="display:inline-block;">
                        <form class="form-inline" action="/log-manage/login">
                            <div class="form-group"> 
                                <input type="text" name="findByUsername" placeholder="{{ trans('table.input_select_user') }}" class="form-control">
                            </div>
                            {!! Form::select('loginType', array('0' => '请选择类型...','登入' => '登入', '登出' => '登出'),null,array('class' => 'form-control')) !!}
                            <button type="submit" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.search') }}</button>
                        </form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-11 main">
                        <div class="table-responsive">
                            <div class="courseTable text-center">
                                <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:100%;">
                                    <table id="table" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>{{ trans('table.user_name') }}</th>
                                                <th>{{ trans('table.email') }}</th>
                                                <th>{{ trans('table.type') }}</th>
                                                <th>{{ trans('table.data') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($logs as $log)
                                                <tr>
                                                    <td class="vertical-align-middle">{{$log->user->name}}</td>
                                                    <td class="vertical-align-middle">{{$log->user->email}}</td>
                                                    <td class="vertical-align-middle">{{$log->status}}</td>
                                                    <td class="vertical-align-middle">{{$log->created_at}}</td>
                                                    
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                {!! $logs->render() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
@endsection
