@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <section class="section-content">
            <div class="container clearfix">
                <div class="posts-grid posts-grid-3 clearfix" style="width:110%">
                    <article id="" class="post-grid post-693 ib_educator_course type-ib_educator_course status-publish has-post-thumbnail hentry ib_educator_category-educator">
                        <div class="post-thumb">
                            <a><img width="360" height="224" src="{!!get_url()!!}/img/slides/0.jpg" class="attachment-ib-educator-grid wp-post-image" alt="Business Process" srcset="{!!get_url()!!}/img/slides/0.jpg 360w, {!!get_url()!!}/img/slides/0.jpg 720w" sizes="(min-width: 768px) 360px, 96vw" /></a>
                        </div>

                        <div class="post-body">
                            <h2 class="entry-title">
                                <a rel="bookmark">{{ trans('news.name') }}</a>
                            </h2>
                            @foreach ($messages as $message)
                                <div class="price">

                                    <a href="{{URL('news/'.$message->id)}}">{{ $message->title }}</a>


                                </div>

                            @endforeach

                        </div>

                        <footer class="post-meta">
                            <span class="difficulty">
                                <a href="{{URL('news/lists')}}">{{ trans('table.more') }}...</a></span>
                        </footer>
                    </article>
                    <article id="" class="post-grid post-693 ib_educator_course type-ib_educator_course status-publish has-post-thumbnail hentry ib_educator_category-educator">
                        <div class="post-thumb">
                            <a ><img width="360" height="224" src="{!!get_url()!!}/img/slides/1.jpg" class="attachment-ib-educator-grid wp-post-image" alt="Business Process" srcset="{!!get_url()!!}/img/slides/1.jpg 360w, {!!get_url()!!}/img/slides/1.jpg 720w" sizes="(min-width: 768px) 360px, 96vw" /></a>
                        </div>

                        <div class="post-body">
                            <h2 class="entry-title">
    <a  rel="bookmark">{{ trans('table.rank') }}</a>
                            </h2>
                            @foreach ($users as $user)
                                <div class="price">

                                    <a>{{ $user->realname}}</a>


                                </div>

                            @endforeach
                        </div>
                        <footer class="post-meta">
    <span class="difficulty">{{ trans('table.more') }}...</span>
                        </footer>
                    </article>


                    <article id="" class="post-grid post-693 ib_educator_course type-ib_educator_course status-publish has-post-thumbnail hentry ib_educator_category-educator">
                        <div class="post-thumb">
                            <a><img width="360" height="224" src="{!!get_url()!!}/img/slides/2.jpg" class="attachment-ib-educator-grid wp-post-image" alt="Business Process" srcset="{!!get_url()!!}/img/slides/2.jpg 360w, {!!get_url()!!}/img/slides/2.jpg 720w" sizes="(min-width: 768px) 360px, 96vw" /></a>
                        </div>

                        <div class="post-body">
                            <h2 class="entry-title">
                                <a rel="bookmark">{{ trans('survey.name') }}</a>
                            </h2>
                            @foreach ($surveys as $survey)
                                
                                <div class="price">

                                    <a href="{{URL('survey/'.$survey->id.'/answer/')}}">{{ $survey->title }}</a>


                                </div>

                            @endforeach

                        </div>

                        <footer class="post-meta">
                            <span class="difficulty">
    <a href="{{URL('survey/lists')}}">{{ trans('table.more') }}...</a></span>
                        </footer>
                    </article>

                </div>
            </div>
        </section>
    @endsection
