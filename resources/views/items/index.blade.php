    @extends($survey->type == 1?"survey.partials.template":"evaluation.partials.template")

@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-body">
                <div align="right">
                    <a href="{{URL(($survey->type == 1?'survey/':'evaluation/').$survey->id.'/items/create')}}" class="dm3-btn dm3-btn-medium button-large" value="GET"><i class="fa fa-plus"></i>&nbsp;{{ trans('question.create') }}</a>
                </div>
                <div class="row">
                    
                    <div class="col-sm-12 main">
                        <div class="table-responsive">
                            <div class="courseTable text-center">
                                <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:100%;">
                                    <table class="table table-striped table-bordered text-center">
                                        <thead>
                                            <tr>
                                                <th>{{ trans('question.title') }}</th>
                                                <th>{{ trans('table.type') }}</th>
                                                <th>{{ trans('table.operation') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($survey->items as $item)
                                                <tr ng-repeat="trainee in trainees" class="ng-scope">
                                                    <td class="vertical-align-middle">{{ $item->title }}</td>

                                                    <td class="vertical-align-middle">{{ $item->type == 1 ? trans('question.select'):trans('question.grade')}}</td>
                                                    <td class="vertical-align-middle">
                                                        <a href="{{ url(($survey->type == 1?'survey/':'evaluation/').$survey->id.'/items/'.$item->id.'/edit') }}"  class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-pencil"></i>&nbsp;{{ trans('button.edit') }}</a>
                                                        <form action="{{ URL(($survey->type == 1?'survey/':'evaluation/').$survey->id.'/items/'.$item->id) }}" method="POST" style="display: inline;">
                                                            <input name="_method" type="hidden" value="DELETE">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <button type="submit" onClick="return ifDelete()" class="dm3-btn dm3-btn-medium dm3-btn-red button-large"><i class="fa fa-times"></i>&nbsp;{{ trans('button.delete') }}</button>

                                                        </form>
                                                    </td>
                                                </tr><!-- end ngRepeat: trainee in trainees -->
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

    
@endsection
