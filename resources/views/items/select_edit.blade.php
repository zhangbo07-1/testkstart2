<div class="selects" style="margin-top: 10px;">

    @for( $i = 1;$i<=2;$i++)
        <a> {{ trans('question.select_title') }}{{$i}}</a>

        <input type="text" name="select{{$i}}" class="form-control" required="required" value="{{$item->selects[$i-1]->title}}" >
    @endfor

    <div id="selectPlace">
        @for($i;$i<=count($item->selects);$i++)
            <div id="selectdiv_{{$i}}">
                <a> {{ trans('question.select_title') }}{{$i}}</a>
                <br>
                <div class="col-md-11" style="padding-left: 0px;">
                     <input type="text" name="select{{$i}}" class="form-control" required="required" value="{{$item->selects[$i-1]->title}}" >
                </div>
                <div class="col-md-1">
                    <input type="button" value="{{ trans('button.delete') }}" class="dm3-btn dm3-btn-medium"  onclick="delSelect({{$i}})"/>
                </div>
            </div>

        @endfor
    </div>

    <div align="right">
        <input type="button" class="dm3-btn dm3-btn-medium" id="addSelectBTN" onClick="addSelect()" value="{{ trans('button.add_select') }}"/>
    </div>
</div>
