@extends('layout.master')
@section('content')
    <div class="container">
        <br>
        <br>
        <div class="container">
            <aside class="lessons-container">
                <h3 class="lessons-title">调研</h3>
                <ul class="lessons-ul">
                    @foreach($surveys as $survey)
                        @if($survey->pushed>0)
                            <li class="row">
                                <div class="col-md-6">
                                    <div class="col-fixed"><span class="icon icon-15"></span></div>
                                    <div class="row row-margin-5px">
                                        <div class="lessons-padding ">
                                            <div><a class="lessons-course-name">{{$survey->title}}</a></div>


                                            <span class="post-date">
                                                题目数:&nbsp;{{count($survey->items)}}&nbsp;&nbsp;
                                                <span>
                                                    @if(Auth::user()->surveyResult->where('survey_id',$survey->id)->first())
                                                        是否完成:&nbsp;是</span>

                                                    @else
                                                        是否完成:&nbsp;否</span>
                                                    @endif


                                    </span>
                                    <i class="fa fa-clock-o">&nbsp;创建时间:&nbsp;</i>{{ date('Y-m-d H:i', strtotime($survey->updated_at)) }}





                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="lessons-padding-button">
                                        @if(Auth::user()->surveyResult->where('survey_id',$survey->id)->first())

                                            <a class="dm3-btn dm3-btn-gray button-large"><i class="fa fa-edit"></i>&nbsp;答卷</a>

                                        @else

                                            <a href="{{ url('survey/'.$survey->id.'/answer/') }}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-edit"></i>&nbsp;答卷</a>
                                        @endif
                                        
                                    </div>
                                </div>
                            </li>
                        @endif
                    @endforeach
                    
                </ul>
                {!! $surveys->render() !!}
            </aside>
        </div>
@endsection
