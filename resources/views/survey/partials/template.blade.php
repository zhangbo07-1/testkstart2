@extends('layout.master')

@section('content')
    @include('survey.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection