@extends('survey.partials.template')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-5 col-md-offset-3">
                            <form action="{{ URL('survey/'.$survey->id) }}" method="POST">
                                <input name="_method" type="hidden" value="PUT">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <br>
                                <a>{{ trans('table.title') }}</a>
                                <input type="text" name="title" class="form-control" required="required" value="{{ $survey->title }}">
                                {!! errors_for('title', $errors) !!}
                                <br>
                                <a>{{ trans('table.range') }}</a>
                                <br>
                                <div style="display:inline-block;">
                                    <a href="javascript:manageSelect();" class="dm3-btn dm3-btn-medium">{{ trans('button.range') }}</a>
                                </div>
                                <div style="display:inline-block;" id="manage_name">{{$manages}}</div>
                                <input type="hidden" id="manage_id"  name="manage_id" value=""/>
                                {!! errors_for('manage_id', $errors) !!}
                                <br>
                                <br>
                                <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                                <a href="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    @endsection
