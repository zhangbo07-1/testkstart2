@extends('survey.partials.template')
@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">调研结果</h2>
            </div>
        </div>
        <div class="panel panel-announcement">
            <div class="panel-body">
                <div class="col-md-12">
                <div class="table-responsive">
                    @if($result['resultNumber'])
                        <div align="right" >
                            <a href="#" class="dm3-btn dm3-btn-medium button-large" value="GET">导出EXCEL</a>
                            <br>
                            <h5 style="color: #337ab7;">{{$survey->title}}:答题人数:{{$result['resultNumber']}}</h5>
                        </div>
<div class="courseTable text-center">
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>内容</th>
                                    <th>选取人数</th>
                                    <th>百分比</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($survey->items as $index=>$item)
                                    <tr ng-repeat="trainee in trainees" class="ng-scope">
                                        <td class="ng-binding"><h5 style="color: #337ab7;">题目{{$index+1}}</h5></td>
                                        <td class="ng-binding">{{ $item->title }}</td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    @if($item->type == 1)

                                        @foreach($item->selects as $select)
                                            <tr ng-repeat="trainee in trainees" class="ng-scope">
                                                <td class="ng-binding"></td>
                                                <td class="ng-binding">{{ $select->title }}</td>
                                                <td class="ng-binding">{{$result['number'][$item->id][$select->id] }}</td>

                                                <td class="ng-binding">{{ round($result['number'][$item->id][$select->id]/$result['resultNumber']*100,2)}}%</td>
                                            </tr>
                                        @endforeach
                                    @else
                                        @if($grade = $item->grade)
                                            <tr ng-repeat="trainee in trainees" class="ng-scope">
                                                <td class="ng-binding">声明</td>

                                                <td class="ng-binding">
                                                    1为{{$grade->lowtitle}} ～～5为{{$grade->hightitle}}
                                                </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            @for($j=1; $j<=5; $j++)
                                                
                                                
                                                <tr ng-repeat="trainee in trainees" class="ng-scope">
                                                    <td class="ng-binding"></td>
                                                    <td class="ng-binding">{{$j}}分</td>
                                                    <td class="ng-binding">
                                                        {{$result['number'][$item->id][$j]}}
                                                    </td>

                                                    <td class="ng-binding">
                                                        {{ round($result['number'][$item->id][$j]/$result['resultNumber']*100,2)}}%
                                                    </td>
                                                </tr>
                                                
                                            @endfor
                                            <tr ng-repeat="trainee in trainees" class="ng-scope">
                                                <td class="ng-binding">平均分</td>
                                                <td class="ng-binding">{{$result['avg'][$item->id]}}</td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
</div>
                    @else
                        尚无成绩
                    @endif

                </div>
            </div>
        </div>

    </div>

@endsection
