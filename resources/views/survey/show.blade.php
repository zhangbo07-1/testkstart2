@extends('survey.partials.template')
@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{ trans('button.preview') }}</h2>
            </div>
        </div>

        <div class="panel panel-announcement">
            <div class="panel-body">
                <div class="col-md-10 col-md-offset-1">
                    <h5 style="color: #337ab7;">{{$survey->title}}</h5>
                    @foreach($survey->items as $index=>$item)

                        <div style="border-top: solid 20px #efefef;">
                            
                            <h5 style="color: #337ab7;">{{$index+1}}.{{$item->title}}</h5>
                            @if($item->type == 1)
                                @include('items.select_show')
                            @else
                                @include('items.grade_show')
                            @endif
                        </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

