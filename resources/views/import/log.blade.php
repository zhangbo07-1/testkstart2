@extends('layout.base')

        @section('content')

           <div class="row" style="margin-right:0">
                <div class="col-md-8 col-md-offset-2">
        {!!$import->error!!}

                </div>
           </div>
           <br>
           <br>
            <div align="center">
                <a class="dm3-btn dm3-btn-medium dm3-btn button-large" href="javascript:window.close();">{{ trans('button.submit') }}</a>
            </div>

        @endsection
