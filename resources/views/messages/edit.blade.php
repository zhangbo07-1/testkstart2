@extends('layout.master')
@section('content')
    @include('UMEditor.head');

    <div class="container">
        <div class="row">
                <div class="panel">


                    <div class="panel-body">

                        <hr>
                        <h5 style="color: #337ab7;">{{ trans('table.title') }}</h5>
                        <form action="{{ URL('news-manage/'.$message->id) }}" method="POST">
                            <input name="_method" type="hidden" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="text" name="title" class="form-control" required="required" value="{{ $message->title }}">
                            {!! errors_for('title', $errors) !!}
                            <br>
                            <h5 style="color: #337ab7;">{{ trans('table.text') }}</h5>
                            {!! errors_for('body', $errors) !!}
                            <textarea id='myEditor' name="body" class="form-control" style="height:240px;" required="required">{{ $message->body }}</textarea>
                            <br>
                            <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                            <a href="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                        </form>

                    </div>
                </div>
        </div>
    </div>
@endsection
