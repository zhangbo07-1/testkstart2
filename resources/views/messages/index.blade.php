@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append

    @section('content')

        <div class="container">
            <br>
            <br>
            <h3>{{ trans('news.manage') }}</h3>
            <div class="row">
                <div class="col-sm-12 main">
                    @can('create_message',Auth::user())
                    <div align="right">
                        <a href="{{URL('news-manage/create')}}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-plus"></i>&nbsp;{{ trans('news.create') }}</a>
                    </div>
                    @endcan
                    <div class="table-responsive">
                        <div class="courseTable text-center">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{ trans('table.title') }}</th>
                                        <th>{{ trans('table.pushed_time') }}</th>
                                        <th>{{ trans('table.operation') }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($messages as $message)
                                        <tr ng-repeat="trainee in trainees" class="ng-scope">
                                            <td class="vertical-align-middle"><a href="{{URL('news-manage/'.$message->id)}}">{{ $message->title }}</a></td>
                                            <td class="vertical-align-middle">{{ $message->updated_at }}</td>

                                            <td class="vertical-align-middle">
                                                @can('modify',$message)
                                                <a href="{{ url('/news-manage/'.$message->id.'/edit') }}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-pencil"></i>&nbsp;{{ trans('button.edit') }}</a>
                                                <form action="{{ URL('news-manage/'.$message->id) }}" method="POST" style="display: inline;">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button type="submit" onClick="return ifDelete()" class="dm3-btn dm3-btn-medium dm3-btn-red button-large"><i class="fa fa-times"></i>&nbsp;{{ trans('button.delete') }}</button>

                                                </form>
                                                @endcan
                                            </td>
                                        </tr><!-- end ngRepeat: trainee in trainees -->
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {!! $messages->render() !!}
        </div>
    @endsection
