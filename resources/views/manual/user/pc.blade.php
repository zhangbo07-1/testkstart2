@extends('manual.container')

@section('navbar')
    @include('manual.user.navbar')
@endsection

@section('content')

    <h2>电脑端学习</h2>

    <br>

    <div id="nav1" class="entry-content">
        <h4>使用帮助</h4>

        <p>你可以随时点击右上方用户名菜单下的“使用帮助”，查看系统使用教程。</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image001.jpg" alt="">

    </div>

    <div class="dm3-divider-dotted"></div>

    <section id="nav2" class="entry-content">

        <h4>用户登入</h4>

        <p>通过输入注册时填写的邮箱以及密码登入系统</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image003.jpg" alt="">

    </section>

    <div class="dm3-divider-dotted"></div>

    <section id="nav3" class="entry-content">

        <h4>系统主页</h4>

        <p>在系统主页上，你可以：
        <ul>
            <li><strong>浏览新闻：</strong>点击新闻标题即可浏览新闻内容。点击新闻下方的“更多”按钮，进入所有新闻列表；</li>

            <li><strong>查看排行：</strong>查看到目前课程完成率最高的排行名单；</li>

            <li><strong>参与系统调研问卷：</strong>点击调研标题即可进入调研。点击调研下方的“更多”按钮，进入所有调研列表；</li>
        </ul>
        </p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image005.jpg" alt="">

    </section>

    <div class="dm3-divider-dotted"></div>

    <section id="nav4" class="entry-content">

        <h4>学习</h4>

        <p>点击“我的培训-进行中”，每个课程包含四个按钮：
        <ul>
            <li><strong>开始：</strong>点击“开始”打开课程或考试；</li>
            <li><strong>记录：</strong>点击“记录”查看学习记录；</li>
            <li><strong>考试：</strong>如果“考试”按钮高亮，说明此课程被关联了考试。点击“考试”开始答题。如果课程和考试关联，则必须全部都完成，整个课程才会被记录已完成。</li>
            <li><strong>评估：</strong>如果“评估”按钮高亮，说明此课程被关联了评估。点击“评估”开始答题。如果课程和评估关联，则必须全部都完成，整个课程才会被记录已完成。</li>
        </ul>
        </p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image007.jpg" alt="">

    </section>

    <div class="dm3-divider-dotted"></div>


    <section id="nav5" class="entry-content">

        <h4>课程学习</h4>

        <p>点击“开始”按钮，打开课程学习。注意，请浏览课程的所有页面，如果课程中有考题，请答完所有题目。
        </p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image009.jpg" alt="">

    </section>

    <div class="dm3-divider-dotted"></div>

    <section id="nav6" class="entry-content">

        <h4>课后评估</h4>

        <p>点击“评估”按钮，开始答题。每个评估只能参与一次。</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image011.jpg" alt="">

    </section>

    <div class="dm3-divider-dotted"></div>


    <section id="nav7" class="entry-content">

        <h4>考试</h4>

        <p>点击“考试”按钮，或者点击考试的“开始”按钮，开始答题。</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image013.jpg" alt="">

    </section>

    <div class="dm3-divider-dotted"></div>

    <section id="nav8" class="entry-content">

        <h4>查看学习记录</h4>

        <p>点击“记录”按钮，查看学习记录。如果该课程有考试关联，则记录中将显示课程记录以及考试记录。如果无考试关联，将只显示课程记录。</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image015.jpg" alt="">
        <img class="photo" src="{!!get_url()!!}/img/manual/user/image017.jpg" alt="">
        <img class="photo" src="{!!get_url()!!}/img/manual/user/image019.jpg" alt="">
        <img class="photo" src="{!!get_url()!!}/img/manual/user/image021.jpg" alt="">


    </section>

    <div class="dm3-divider-dotted"></div>

    <section id="nav9" class="entry-content">

        <h4>已完成记录</h4>
        <p>已完成的课程或考试，将自动归入已完成列表，点击“我的培训-已完成”查看学习记录。</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image023.jpg" alt="">

    </section>

    <div class="dm3-divider-dotted"></div>

    <section id="nav10" class="entry-content">

        <h4>资料库下载</h4>

        <p>点击“资料库”，可下载资料库中的内容。</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image025.jpg" alt="">

    </section>

    <div class="dm3-divider-dotted"></div>

    <section id="nav11" class="entry-content">

        <h4>讨论区</h4>

        <p>点击“讨论区”，点击“新建讨论”输入标题和内容，并点击“发表文档”。</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image027.jpg" alt="">

    </section>

    <div class="dm3-divider-dotted"></div>

    <section id="nav12" class="entry-content">

        <h4>个人信息</h4>

        <p>点击右上方的用户名，点击“个人信息”。可在此处编辑个人信息，以及修改个人密码。</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/user/image029.jpg" alt="">
        <img class="photo" src="{!!get_url()!!}/img/manual/user/image031.jpg" alt="">

    </section>

    <div class="dm3-divider-dotted"></div>


@endsection