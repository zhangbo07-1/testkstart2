<ul class="list-unstyled" style="width:150px;margin:0 auto;">
    <li><a href="/manual/user/pc"><h3>电脑端学习</h3></a></li>
    <li>
        <ul class="list-unstyled">
            <li><a href="/manual/user/pc#nav1">使用帮助</a></li>
            <li><a href="/manual/user/pc#nav2">用户登入</a></li>
            <li><a href="/manual/user/pc#nav3">系统主页</a></li>
            <li><a href="/manual/user/pc#nav4">学习</a></li>
            <li><a href="/manual/user/pc#nav5">课程学习</a></li>
            <li><a href="/manual/user/pc#nav6">课后评估</a></li>
            <li><a href="/manual/user/pc#nav7">考试</a></li>
            <li><a href="/manual/user/pc#nav8">查看学习记录</a></li>
            <li><a href="/manual/user/pc#nav9">已完成记录</a></li>
            <li><a href="/manual/user/pc#nav10">资料库下载</a></li>
            <li><a href="/manual/user/pc#nav11">讨论区</a></li>
            <li><a href="/manual/user/pc#nav12">个人信息</a></li>
        </ul>
    </li>
    <li><a href="/manual/user/wechat"><h3>微信端学习</h3></a></li>
    <li>
        <ul class="list-unstyled">
            <li><a href="/manual/user/wechat#nav1">使用帮助</a></li>
            <li><a href="/manual/user/wechat#nav2">系统主页</a></li>
            <li><a href="/manual/user/wechat#nav3">学习</a></li>
            <li><a href="/manual/user/wechat#nav4">课程学习</a></li>
            <li><a href="/manual/user/wechat#nav5">课后评估</a></li>
            <li><a href="/manual/user/wechat#nav6">考试</a></li>
            <li><a href="/manual/user/wechat#nav7">查看学习记录</a></li>
            <li><a href="/manual/user/wechat#nav8">考试记录</a></li>
            <li><a href="/manual/user/wechat#nav9">已完成记录</a></li>
            <li><a href="/manual/user/wechat#nav10">资料库下载</a></li>
            <li><a href="/manual/user/wechat#nav11">讨论区</a></li>
            <li><a href="/manual/user/wechat#nav12">个人信息</a></li>
        </ul>
    </li>
</ul>

