<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link media="all" type="text/css" rel="stylesheet" href="{{ elixir("assets/css/style.css") }}">
    {!! Html::style('assets/css/font-awesome.css') !!}
    {!! Html::style('assets/css/owl.carousel.css') !!}
    {!! Html::style('assets/css/owl.theme.css') !!}
    {!! Html::style('assets/css/owl.transitions.css') !!}
    {!! Html::style('assets/css/editor-style.css') !!}
    {!! Html::style('assets/css/woocommerce.css') !!}
    {!! Html::style('assets/css/magnific-popup.css') !!}
    {!! Html::style('assets/css/flexslider.css') !!}
    {!! Html::style('assets/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css') !!}
    {!! Html::style('css/animate.css') !!}
    {!! Html::style('assets/bower_components/fontawesome/css/font-awesome.css') !!}
    {!! Html::style('css/custom.css') !!}
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
    @yield('css')
    <link media="all" type="text/css" rel="stylesheet" href="{{ elixir("assets/css/app.css") }}">
</head>
<body>


<div class="container">
    <h1>Knowsurface注册流程</h1>
    <br>
    <h2>第一步：注册账号</h2>
    <ol>
        <li>从PC的网页浏览器或者手机的网页浏览器中打开站点地址（eg: abc.knowsurface.com）；</li>
        <li>点击右上角“注册”按钮；</li>
        <li>填写用户名、邮箱、密码；</li>
        <img class="photo" src="{!!get_url()!!}/img/manual/registration/image001.jpg">
        <li>点击“注册”按钮；</li>
        <li>从注册的邮箱中收到一封验证邮件；</li>
        <li>根据邮件中的提示，点击验证链接；</li>
        <img class="photo" src="{!!get_url()!!}/img/manual/registration/image003.jpg">
        <li><p>提示验证成功，即可登入系统；</p>
        <p>用户手册在主页→右上角点击用户名→使用帮助</p>
        </li>
    </ol>
    <div class="dm3-divider-dotted"></div>
    <h2>第二步：绑定微信账号</h2>
    <p>在微信中关注“知识平面”公众账号，点击绑定，输入注册时填写的邮箱和密码，成功绑定。</p>
    <img class="photo" src="{!!get_url()!!}/img/manual/registration/image005.jpg">
    <p>用户手册请点击右上角<img src="{!!get_url()!!}/img/manual/user/image033.jpg">按钮→点击用户名→使用帮助</p>
    <p>祝您学习愉快！</p>
    <a class="dm3-btn dm3-btn-medium button-large" href="/register">返回注册</a>
    <br>
</div>

{!! Html::script('assets/bower_components/jquery/dist/jquery.min.js') !!}
{{--{!! Html::script('assets/js/bootstrap.min.js') !!}--}}

{!! Html::script('assets/vendor/jquery.js') !!}
{{--{!! Html::script('assets/js/plugins.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.easing.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.appear.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.cookie.js') !!}--}}
{{--{!! Html::script('assets/vendor/bootstrap.js') !!}--}}
{{--{!! Html::script('assets/vendor/twitterjs/twitter.js') !!}--}}
{{--{!! Html::script('assets/vendor/rs-plugin/js/jquery.themepunch.plugins.min.js') !!}--}}
{{--{!! Html::script('assets/vendor/rs-plugin/js/jquery.themepunch.revolution.js') !!}--}}
{{--{!! Html::script('assets/vendor/owl-carousel/owl.carousel.js') !!}--}}
{{--{!! Html::script('assets/vendor/circle-flip-slideshow/js/jquery.flipshow.js') !!}--}}
{{--{!! Html::script('assets/vendor/magnific-popup/magnific-popup.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.validate.js') !!}--}}
{{--{!! Html::script('assets/js/views/view.home.js') !!}--}}
{{--{!! Html::script('assets/js/theme.js') !!}--}}
{{--{!! Html::script('assets/js/custom.js') !!}--}}
{{--{!! Html::script('/js/main.min.js') !!}--}}
{!! Html::script('assets/js/modernizr.js') !!}
{!! Html::script('assets/js/base.js') !!}
{!! Html::script('assets/js/bootstrap.min.js') !!}
{!! Html::script('assets/js/jquery.flexslider.js') !!}
{!! Html::script('assets/js/jquery.magnific-popup.js') !!}
{!! Html::script('assets/js/jquery.validate.js') !!}
{!! Html::script('assets/js/owl.carousel.js') !!}
{!! Html::script('assets/js/main.js') !!}
{{--{!! Html::script('assets/js/theme.js') !!}--}}
{!! Html::script('assets/js/views/view.contact.advanced.js') !!}
{!! Html::script('assets/js/views/view.contact.js') !!}
{!! Html::script('assets/js/views/view.home.js') !!}
{!! Html::script('assets/bower_components/jquery-ui/jquery-ui.min.js') !!}
{!! Html::script('assets/bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js') !!}
@yield('subTemplateJs')
@yield('js')
{!! Html::script('assets/js/main.min.js') !!}

</body>
</html>
