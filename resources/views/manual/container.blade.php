<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link media="all" type="text/css" rel="stylesheet" href="{{ elixir("assets/css/style.css") }}">
    {!! Html::style('assets/css/font-awesome.css') !!}
    {!! Html::style('assets/css/owl.carousel.css') !!}
    {!! Html::style('assets/css/owl.theme.css') !!}
    {!! Html::style('assets/css/owl.transitions.css') !!}
    {!! Html::style('assets/css/editor-style.css') !!}
    {!! Html::style('assets/css/woocommerce.css') !!}
    {!! Html::style('assets/css/magnific-popup.css') !!}
    {!! Html::style('assets/css/flexslider.css') !!}
    {!! Html::style('assets/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css') !!}
    {!! Html::style('css/animate.css') !!}
    {!! Html::style('assets/bower_components/fontawesome/css/font-awesome.css') !!}
    {!! Html::style('css/custom.css') !!}
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
    @yield('css')
    <link media="all" type="text/css" rel="stylesheet" href="{{ elixir("assets/css/app.css") }}">


</head>
<body>


<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">知识平面-KnowSurface</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        @can('student',Auth::user())
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="/manual/user/pc" class="dropdown-toggle" data-toggle="dropdown" role="button"
                       aria-haspopup="true"
                       aria-expanded="false">学员手册 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/manual/user/pc">PC端学习</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/manual/user/wechat">微信端学习</a></li>
                    </ul>
                </li>
            </ul>
            @else
                <ul class="nav navbar-nav">
                    <li class="dropdown">
                        <a href="/manual/manager" class="dropdown-toggle" data-toggle="dropdown" role="button"
                           aria-haspopup="true"
                           aria-expanded="false">管理员手册 <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/manual/manager#nav1">报表</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/manual/manager#nav2">学习管理</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/manual/manager#nav3">系统管理</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/manual/manager#nav4">用户管理</a></li>
                        </ul>
                    </li>
                </ul>
            @endcan
            
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/logout">登出</a></li>
            </ul>
           <ul class="nav navbar-nav navbar-right">
                <li><a href="/">主页</a></li>
            </ul>
        </div>
        <!--/.nav-collapse -->
    </div>
</nav>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3">
            @yield('navbar')
        </div>
        <div class="col-sm-9 ">
            @yield('content')
        </div>
    </div>
</div>

{!! Html::script('assets/bower_components/jquery/dist/jquery.min.js') !!}
{{--{!! Html::script('assets/js/bootstrap.min.js') !!}--}}

{!! Html::script('assets/vendor/jquery.js') !!}
{{--{!! Html::script('assets/js/plugins.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.easing.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.appear.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.cookie.js') !!}--}}
{{--{!! Html::script('assets/vendor/bootstrap.js') !!}--}}
{{--{!! Html::script('assets/vendor/twitterjs/twitter.js') !!}--}}
{{--{!! Html::script('assets/vendor/rs-plugin/js/jquery.themepunch.plugins.min.js') !!}--}}
{{--{!! Html::script('assets/vendor/rs-plugin/js/jquery.themepunch.revolution.js') !!}--}}
{{--{!! Html::script('assets/vendor/owl-carousel/owl.carousel.js') !!}--}}
{{--{!! Html::script('assets/vendor/circle-flip-slideshow/js/jquery.flipshow.js') !!}--}}
{{--{!! Html::script('assets/vendor/magnific-popup/magnific-popup.js') !!}--}}
{{--{!! Html::script('assets/vendor/jquery.validate.js') !!}--}}
{{--{!! Html::script('assets/js/views/view.home.js') !!}--}}
{{--{!! Html::script('assets/js/theme.js') !!}--}}
{{--{!! Html::script('assets/js/custom.js') !!}--}}
{{--{!! Html::script('/js/main.min.js') !!}--}}
{!! Html::script('assets/js/modernizr.js') !!}
{!! Html::script('assets/js/base.js') !!}
{!! Html::script('assets/js/bootstrap.min.js') !!}
{!! Html::script('assets/js/jquery.flexslider.js') !!}
{!! Html::script('assets/js/jquery.magnific-popup.js') !!}
{!! Html::script('assets/js/jquery.validate.js') !!}
{!! Html::script('assets/js/owl.carousel.js') !!}
{!! Html::script('assets/js/main.js') !!}
{{--{!! Html::script('assets/js/theme.js') !!}--}}
{!! Html::script('assets/js/views/view.contact.advanced.js') !!}
{!! Html::script('assets/js/views/view.contact.js') !!}
{!! Html::script('assets/js/views/view.home.js') !!}
{!! Html::script('assets/bower_components/jquery-ui/jquery-ui.min.js') !!}
{!! Html::script('assets/bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js') !!}
@yield('subTemplateJs')
@yield('js')
{!! Html::script('assets/js/main.min.js') !!}

</body>
</html>

