@extends('manual.container')

@section('navbar')
    @include('manual.manager.navbar')
@endsection

@section('content')

    <h2>管理员手册</h2>

    <br>

    <h3>报表</h3>

    <br>

    <div id="nav1" class="entry-content">

        <p><strong>系统支持多种报表数据统计。包括：</strong>
        <ul>
            <li><strong>用户信息：</strong>列出目前系统中的所有用户帐号的信息，可根据部门、职位、状态等过滤用户，并可将最终结果导出为excel报表；</li>
            <li><strong>培训记录：</strong>列出目前系统中所有用户的所有学习记录，可根据不同课程、不同时间进行过滤选择，并可将结果导出为excel报表；</li>
            <li><strong>题目分析：</strong>查看所有题目的正确率；</li>
            <li><strong>评估报表：</strong>查看课程评估的统计情况；</li>
        </ul>
        </p>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image001.jpg" alt="">

    </div>

    <div class="dm3-divider-dotted"></div>

    <h3>学习管理</h3>


    <div id="nav2" class="entry-content">

        <h4>目录管理</h4>

        <p>课程目录相当于将课程分类，管理员可自行创建课程目录，并将课程归纳与某个目录中。</p>

        <p><strong>创建目录</strong></p>

        <ul>
            <li>点击“学习管理—目录管理”</li>

            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image003.jpg" alt="">

            <li>点击“添加目录”</li>

            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image005.jpg" alt="">

            <li>输入目录名称，点击“新建”按钮</li>
        </ul>
        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image007.jpg" alt="">


        <p><strong>管理目录</strong></p>

        <ol>
            <li>点击“学习管理—目录管理”</li>

            <li>点击目录右侧的“管理”按钮，可更新目录名称</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image009.jpg" alt="">
        </ol>
        <p><strong>删除目录</strong></>
        <ol>
            <li>点击“学习管理—目录管理”，点击“删除”按钮。注意，默认目录不允许删除。</li>
        </ol>
        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image011.jpg" alt="">
    </div>

    <div class="dm3-divider-dotted"></div>

    <div id="nav2subnav2" class="entry-content">

        <h4>课程管理</h4>

        <p>课程管理包含了课件的上传、更新、删除和分配，同时可对课件信息进行管理，并可关联相应的考试或评估。</p>

        <p><strong>课程搜索</strong></p>
        <ol>
            <li>点击“学习管理—课程管理”</li>
            <li>在目录下拉菜单中，选择课程目录，点击“搜索”，可搜索出这个目录中的所有课程</li>
            <li>在输入框中，输入课程名，点击“搜索”，可搜索出标题中包含输入字段的所有课程</li>
        </ol>
        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image013.jpg" alt="">

        <div class="dm3-divider-dotted"></div>
        <p><strong>添加课程</strong></p>
        <ol>
            <li>点击“学习管理—课程管理”</li>
            <li>点击“添加课程”，输入以下相关信息</li>
            <li>注意，课程编号为必填项，且必须为字母或数字，接受中横杠、下划线，不能包含空格和中文。</li>


            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image015.jpg" alt="">

            <li>点击“确定”，进行课程上传。点击“浏览”选择您的课程包，注意，仅支持scorm2004课件包，格式为.zip。选择课程包后，点击“上传”</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image017.jpg" alt="">
        </ol>

        <div class="dm3-divider-dotted"></div>
        <p><strong>课程分配</strong></p>

        <ol>
            <li>点击“学习管理—课程管理”</li>
            <li>点击课件后的“管理”按钮</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image019.jpg" alt="">
            <li>点击“课程分配”，勾选需要分配的目标用户。你可以通过输入关键词搜索用户，关键词包含用户名、姓名、email、微信号、部门、职位等所有用户信息中的关键词。</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image021.jpg" alt="">
            <li>选定用户后，点击“确认”，课程分配成功</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image023.jpg" alt="">
            <li>如果需要批量的将课程退出用户学习列表，则同样在此处，在“已参与课程的学员”列表中，勾选用户并点击“删除”按钮，即可批量退课。</li>
        </ol>
        <div class="dm3-divider-dotted"></div>
        <p><strong>课程更新</strong></p>

        <ol>
            <li>点击“学习管理—课程管理”</li>
            <li>点击课件后的“管理”按钮</li>
            <li>点击“课程更新”，并上传课程包进行内容更新。注意，仅支持scorm2004课件包，格式为.zip。</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image025.jpg" alt="">
            <li>或者点击“学习管理—课程管理”，点击课件后的“更新”按钮</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image027.jpg" alt="">
        </ol>

        <p><strong>课程预览</strong></p>
        <ol>
            <li>点击“学习管理—课程管理”</li>
            <li>点击课件后的“管理”按钮</li>
            <li>点击“课程预览”，新窗口中将出现课件内容</li>
        </ol>
        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image029.jpg" alt="">
        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image031.jpg" alt="">

        <p><strong>关联考试</strong></p>

        <p>如果课程被关联了考试，则用户必须完成课程，并通过考试，整个课程才算完成。</p>
        <ol>
            <li>要关联考试，请点击“学习管理—课程管理”</li>
            <li>点击课件后的“管理”按钮</li>
            <li>点击“关联考试”</li>
            <li>搜索需要关联的考试，并点击“确定”</li>
        </ol>
        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image033.jpg" alt="">

        <p><strong>关联评估</strong></p>

        <p>如果课程被关联了评估，则用户必须完成课程，并参与评估，整个课程才算完成。</p>
        <ol>
            <li>要关联评估，请点击“学习管理—课程管理”</li>
            <li>点击课件后的“管理”按钮</li>
            <li>点击“关联评估”</li>
            <li>搜索需要关联的评估，并点击“确定”</li>
        </ol>
        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image035.jpg" alt="">

        <p><strong>删除课程</strong></p>
        <ol>
            <li>点击“学习管理—课程管理”</li>
            <li>点击课件后的“管理”按钮</li>
            <li>点击“删除课程”，并点击“确认”</li>
        </ol>
        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image037.jpg" alt="">


    </div>

    <div class="dm3-divider-dotted"></div>


    <div id="nav2subnav3" class="entry-content">

        <h4>考试管理</h4>

        <p><strong>添加考试</strong></p>
        <ol>
            <li>点击“学习管理—考试管理”</li>
            <li>点击“添加考试”</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image039.jpg" alt="">
            <li>输入相关内容，并点击“确定”</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image041.jpg" alt="">
        </ol>

        <div class="dm3-divider-dotted"></div>

        <p><strong>组卷</strong></p>

        <ol>
            <li>点击“学习管理—考试管理”</li>
            <li>点击考试后的“管理”按钮</li>
            <li>点击“组卷”，系统将列出所有的题库以及题库中的题目数量，请在对应题库中输入题数来确定从该题库抽取的题目数。</li>
        </ol>
        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image043.jpg" alt="">

        <p><strong>考试分配</strong></p>
        <ol>
            <li>点击“学习管理—考试管理”</li>
            <li>点击考试后的“管理”按钮</li>
            <li>点击“考试分配”，你可以通过输入关键词搜索用户，关键词包含用户名、姓名、email、微信号、部门、职位等所有用户信息中的关键词</li>
            <li>点击“确认”，考试分配成功</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image045.jpg" alt="">
            <li>如需将考试退出用户学习列表，则在已参与考试的学员列表中勾选用户，并点击“删除”；</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image047.jpg" alt="">
        </ol>
    </div>


    <div id="nav2subnav4" class="entry-content">

        <h4>题库管理</h4>

        <br>

        <p>你可以在系统中创建题库，在题库中添加题目，并在生成考卷时，从不同题库中抽取题目进行组卷。</p>

        <p><strong>添加题库</strong></p>
        <ol>
            <li>点击“学习管理—题库”</li>
            <li>点击“添加新题库”</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image049.jpg" alt="">
            <li>输入题库名，点击“确定”</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image051.jpg" alt="">
        </ol>

        <div class="dm3-divider-dotted"></div>

        <p><strong>添加题目</strong></p>

        <ol>
            <li>点击“学习管理—题库”</li>
            <li>点击题库中的“编辑”按钮</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image053.jpg" alt="">
            <li>进入题库，点击“添加题目”按钮：</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image055.jpg" alt="">
            <li>输入题干内容，以及选项内容：</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image057.jpg" alt="">
            <li>系统目前支持单项选择题，多项选择题，以及是非题，请勾选题型后再输入题目内容；
                <p>如果该题是强制题，在组卷时希望强制被抽到，请在是否强制处勾选“是”；</p>

                <p>如果系统给予的答案选项不够，请点击“添加选项”；</p>
            </li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image059.jpg" alt="">
        </ol>

        <div class="dm3-divider-dotted"></div>

        <p><strong>编辑题目、删除题目</strong></p>

        <ol>
            <li>点击“学习管理—题库”</li>
            <li>点击题库中的“编辑”按钮可编辑题目内容</li>
            <li>点击题库中的“删除”按钮，删除题目</li>
        </ol>
        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image061.jpg" alt="">

    </div>


    <div id="nav2subnav4" class="entry-content">

        <h4>课程评估</h4>


        <p><strong>创建评估</strong></p>
        <ol>
            <li>点击“学习管理—课程评估”</li>
            <li>点击“添加新评估”</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image063.jpg" alt="">
            <li>输入标题，并点击“发布”</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image065.jpg" alt="">
        </ol>

        <div class="dm3-divider-dotted"></div>

        <p><strong>添加评估题目</strong></p>
        <ol>
            <li>点击“学习管理—课程评估”</li>
            <li>点击评估后的“编辑”按钮</li>
            <li>点击“添加新题目”按钮</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image067.jpg" alt="">
            <li>系统支持评分题和选择题，输入题干以及选项内容，点击“发布”</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image069.jpg" alt="">
        </ol>

    </div>

    <div class="dm3-divider-dotted"></div>


    <h2>系统管理</h2>

    <br>

    <div id="nav3" class="entry-content">

        <h4>新闻管理</h4>

        <p>管理员可在新闻管理中发布近期培训通告、新课程上线通知等内容，新闻将显示在用户主页上，发布对象为所有用户。</p>

        <p><strong>添加新闻</strong></p>

        <ol>
            <li>点击“系统管理-新闻管理”</li>

            <li>点击“添加新闻”按钮</li>

            <li>输入新闻标题以及正文内容，并点击“发布新闻”按钮</li>
        </ol>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image071.jpg" alt="">

        <p><strong>编辑和删除新闻</strong></p>
        <ol>
            <li>点击“系统管理-新闻管理”</li>
            <li>点击新闻右侧的“编辑”或“删除”按钮</li>
        </ol>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image073.jpg" alt="">


    </div>

    <div class="dm3-divider-dotted"></div>


    <div id="nav3subnav2" class="entry-content">

        <h4>调研管理</h4>

        <p><strong>创建调研</strong></p>

        <ol>
            <li>点击“系统管理—调研管理”</li>
            <li>点击“添加新调研”</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image075.jpg" alt="">
            <li>输入标题，并点击“发布”</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image077.jpg" alt="">
        </ol>


        <div class="dm3-divider-dotted"></div>

        <p><strong>添加调研题目</strong></p>
        <ol>
            <li>点击“系统管理—调研管理”</li>
            <li>点击调研后的“编辑”按钮</li>
            <li>点击“添加新题目”按钮</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image079.jpg" alt="">
            <li>系统支持评分题和选择题，输入题干以及选项内容，点击“发布”</li>
            <img class="photo" src="{!!get_url()!!}/img/manual/manager/image082.jpg" alt="">
        </ol>

        <p><strong>发布调研</strong></p>
        <ol>
            <li>点击“系统管理—调研管理”</li>
            <li>点击调研后的“发布”按钮，该调研将发布给所有系统用户，并显示在系统主页</li>
        </ol>

        <div class="dm3-divider-dotted"></div>

    </div>


    <div id="nav3subnav3" class="entry-content">

        <h4>资料库管理</h4>

        <p>管理员可上传各种文档资料，供用户下载和阅读。</p>

        <p><strong>添加资料</strong></p>
        <ol>
            <li>点击“系统管理-资料库管理”</li>
            <li>输入资料名，并选择资料进行上传</li>
        </ol>
        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image084.jpg" alt="">

        <div class="dm3-divider-dotted"></div>

        <p><strong>下载和删除资料</strong></p>
        <ol>
            <li>点击“系统管理-资料库管理”</li>
            <li>点击资料右侧的“下载”或“删除”按钮</li>
        </ol>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image086.jpg" alt="">

        <div class="dm3-divider-dotted"></div>
    </div>

    <div id="nav3subnav4" class="entry-content">

        <h4>讨论区</h4>

        <ol>
            <li>点击“系统管理-讨论区”</li>
            <li>点击标题名可预览现有讨论区</li>
            <li>点击“删除”可删除现有讨论区</li>
        </ol>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image088.jpg" alt="">

        <div class="dm3-divider-dotted"></div>
    </div>

    <div id="nav3subnav5" class="entry-content">

        <h4>邮件管理</h4>

        <p>Knowsurface系统支持邮件发送，目前支持的模板有：</p>

        <ul>
            <li><strong>课程分配提醒：</strong>当课程分配给用户时，用户邮箱以及微信账号都将收到消息提示；</li>
            <li><strong>课程催促邮件：</strong>对于有设置完成期限的课程，在到期日期前未完成的用户，每周会收到邮件或微信消息提醒；</li>
            <li><strong>课程完成提醒：</strong>当课程完成后，用户会收到邮件或微信消息提醒；</li>
            <li><strong>课程到期提醒：</strong>对于有设置完成期限的课程，在到期前3天，将会收到邮件或微信消息提醒；</li>
            <li><strong>调研问卷邀请：</strong>当发布新调研问卷时，所有用户将会收到邮件或微信消息提醒；</li>
            <li><strong>注册认证邮件：</strong>新用户注册系统时，将会收到验证邮件。</li>
        </ul>

        <p>点击“系统管理-邮件管理”，可对各个模板进行内容修改，修改后点击“确认修改”</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image090.jpg" alt="">

        <div class="dm3-divider-dotted"></div>
    </div>


    <div id="nav3subnav6" class="entry-content">

        <h4>配置管理</h4>

        <p><strong>Logo管理</strong></p>

        <ol>
            <li>上传您的企业logo，点击“系统管理-配置管理-logo管理”</li>
            <li>点击“选择图片”从您的电脑中选择合适的logo</li>
            <li>点击“开始上传”</li>
        </ol>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image092.jpg" alt="">

        <p><strong>系统信息</strong></p>

        <ol>
            <li>点击“系统管理-配置管理-系统信息”</li>
            <li>修改企业信息并点击“确认”</li>
        </ol>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image094.jpg" alt="">

        <p><strong>审核管理</strong></p>

        <p>点击“系统管理-配置管理-审核管理”，以确认新注册的用户是否要经过审核才能被激活。</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image096.jpg" alt="">

        <div class="dm3-divider-dotted"></div>


        <p><strong>邮箱管理</strong></p>

        <p>点击“系统管理-配置管理-邮箱管理”，添加企业指定的邮箱后缀。</p>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image098.jpg" alt="">

        <div class="dm3-divider-dotted"></div>

    </div>

    <h3 id="nav4">用户管理</h3>

    <br>

    <div class="entry-content">

        <p>KnowSurface支持用户自行注册，省去了管理员创建用户的繁琐步骤。</p>

        <p><strong>编辑用户</strong></p>
        <ol>
            <li>点击“用户管理”</li>
            <li>通过关键词搜索出需要编辑的用户。注意，任何用户信息中的文字均支持关键词搜索。</li>
            <li>点击用户行右侧的“编辑”按钮</li>
        </ol>


        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image100.jpg" alt="">

        <p>管理员可修改的用户信息包括：</p>
        <ul>
            <li><strong>用户密码：</strong>管理员可重置用户密码；</li>
            <li><strong>用户角色：</strong>角色包括系统管理员、培训管理员（可执行报表）、用户（普通用户）；</li>
            <li><strong>用户状态：</strong>活动、未激活（已注册还未通过邮件验证）、待审核（通过邮件验证等待管理员审核）、关闭；</li>
            <li><strong>个人信息：</strong> 姓名、部门、职位、生日、联系方式等；</li>
        </ul>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image102.jpg" alt="">

        <p><strong>删除用户</strong></p>
        <ol>
            <li>点击“用户管理”</li>
            <li>通过关键词搜索出需要编辑的用户。注意，任何用户信息中的文字均支持关键词搜索。</li>
            <li>点击用户行右侧的“删除”按钮</li>
        </ol>

        <img class="photo" src="{!!get_url()!!}/img/manual/manager/image104.jpg" alt="">
    </div>

    <div class="dm3-divider-dotted"></div>








    {{--;;sd;ds--}}
    <div id="nav2subnav5" class="entry-content">

        <h4></h4>


        <p><strong></strong></p>


        <div class="dm3-divider-dotted"></div>
    </div>
@endsection