<ul class="list-unstyled" style="width:150px;margin:0 auto;">
    <li><a href="/manual/manager"><h3>管理员手册</h3></a></li>
    <li>
        <ul class="list-unstyled">
            <li><a href="/manual/manager#nav1">报表</a></li>
            <li><a href="/manual/manager#nav2">学习管理</a>
                <ul class="list-unstyled">
                    <li><a href="/manual/manager#nav2">目录管理</a>
                    <li><a href="/manual/manager#nav2subnav2">课程管理</a>
                    <li><a href="/manual/manager#nav2subnav3">考试管理</a>
                    <li><a href="/manual/manager#nav2subnav4">题库管理</a>
                    <li><a href="/manual/manager#nav2subnav5">课程评估</a>
                </ul>
            </li>
            <li><a href="/manual/manager#nav3">系统管理</a>
                <ul class="list-unstyled">
                    <li><a href="/manual/manager#nav3">新闻管理</a>
                    <li><a href="/manual/manager#nav3subnav2">调研管理</a>
                    <li><a href="/manual/manager#nav3subnav3">资料库管理</a>
                    <li><a href="/manual/manager#nav3subnav4">讨论区管理</a>
                    <li><a href="/manual/manager#nav3subnav5">邮件管理</a>
                    <li><a href="/manual/manager#nav3subnav6">配置管理</a>
                </ul>
            </li>
            <li><a href="/manual/manager#nav4">用户管理</a></li>
        </ul>
    </li>
</ul>

