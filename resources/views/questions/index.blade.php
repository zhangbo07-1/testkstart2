@extends('questionBank.partials.template')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-body">
                    <div align="right">
                        <a href="{{URL('question-bank/'.$bank->id.'/questions/create')}}" class="dm3-btn dm3-btn-medium button-large" value="GET"><i class="fa fa-plus"></i>&nbsp;{{ trans('question.create') }}</a>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 main">
                            <div class="table-responsive">
                                <div class="courseTable text-center">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>{{ trans('question.title') }}</th>
                                                <th>{{ trans('table.type') }}</th>
                                                <th>{{ trans('table.operation') }}</th>
                                            </tr>
                                        </thead>
                                        <tbody>



                                            @foreach($bank->questions as $question)


                                                <tr ng-repeat="trainee in trainees" class="ng-scope">
                                                    <td class="vertical-align-middle">{!! $question->title !!}</td>

                                                    <td class="vertical-align-middle">
                                                        @if($question->type == 1)
                                                            {{ trans('question.radio') }}
                                                        @elseif($question->type ==2)
                                                            {{ trans('question.mulit') }}
                                                        @elseif($question->type == 3)
                                                            {{ trans('question.false') }}
                                                        @elseif($question->type ==4)
                                                            {{ trans('question.essay') }}
                                                        @else
                                                            {{ trans('table.unknow') }}
                                                        @endif
                                                    </td>
                                                    <td class="vertical-align-middle">


                                                        <a href="{{ url('question-bank/'.$bank->id.'/questions/'.$question->id.'/edit') }}"  class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-pencil"></i>&nbsp;{{ trans('button.edit') }}</a>



                                                        <form action="{{ URL('question-bank/'.$bank->id.'/questions/'.$question->id) }}" method="POST" style="display: inline;">
                                                            <input name="_method" type="hidden" value="DELETE">
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <button type="submit" onClick="return ifDelete()" class="dm3-btn dm3-btn-medium dm3-btn-red button-large"><i class="fa fa-times"></i>&nbsp;{{ trans('button.delete') }}</button>

                                                        </form>
                                                    </td>

                                                </tr><!-- end ngRepeat: trainee in trainees -->
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    @endsection


