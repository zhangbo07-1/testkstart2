@extends('questionBank.partials.template')
@include('UMEditor.head')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append

    @section('container')
        <div class="contentpanel">
            <div class="panel panel-announcement">
                <div class="panel-heading">
                    <h2 class="panel-title">{{ trans('question.edit') }}</h2>
                </div>
            </div>

            <div class="panel panel-announcement">
                <div class="panel-body">
                    <div class="col-md-10 col-md-offset-1">
                        <a>{{ trans('question.title') }}</a>
                        <form action="{{ URL('/question-bank/'.$bank->id.'/questions/'.$question->id) }}" method="POST">
                            <input name="_method" type="hidden" value="PUT">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <textarea name="title" rows="5" class="form-control" required="required"  id='myEditor' style="height:240px;">{!!$question->title!!}</textarea>
                            <div id="listNUM">
                                <input type="hidden" id="radioNUM" name="radioNUM"  value="{{count($question->selects)}}">
                                <input type="hidden" id="mulitNUM" name="mulitNUM" value="{{count($question->selects)}}">
                            </div>
                            {{ trans('question.priority') }}：&nbsp;
                            @if($question->priority == 1)
                                <input type="radio" checked="checked" name="priority" value="1" />
                                {{ trans('table.yes') }}&nbsp;&nbsp;

                                <input type="radio"  name="priority" value="0" />
                                {{ trans('table.no') }}
                            @else
                                <input type="radio"  name="priority" value="1" />
                                {{ trans('table.yes') }}&nbsp;&nbsp;

                                <input type="radio" checked="checked" name="priority" value="0" />
                                {{ trans('table.no') }}
                            @endif
                            @if($question->type == 1)
                                @include('questions.radio_edit')
                                
                            @elseif($question->type == 2)
                                @include('questions.mulit_edit')
                                
                            @elseif($question->type == 3)
                                @include('questions.false_edit')
                            @endif
                            <br>
                            <button  class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                            <a href="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>

                        </form>

                    </div>
                </div>
            </div>
        </div>
        </div>

    @endsection
