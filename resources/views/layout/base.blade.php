<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ csrf_token() }}" />
        {!! Html::style('assets/css/style.css') !!}
        {!! Html::style('assets/css/font-awesome.css') !!}
        {!! Html::style('assets/css/owl.carousel.css') !!}
        {!! Html::style('assets/css/owl.theme.css') !!}
        {!! Html::style('assets/css/owl.transitions.css') !!}
        {!! Html::style('assets/css/editor-style.css') !!}
        {!! Html::style('assets/css/woocommerce.css') !!}
        {!! Html::style('assets/css/magnific-popup.css') !!}
        {!! Html::style('assets/css/flexslider.css') !!}
        {!! Html::style('assets/bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css') !!}
        {!! Html::style('css/animate.css') !!}
        {!! Html::style('assets/bower_components/fontawesome/css/font-awesome.css') !!}
        {!! Html::style('css/custom.css') !!}
        {!! Html::style('css/bootstrap-pagination.css') !!}
        {!! Html::style('css/bootstrap_form.css') !!}
        {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.css') !!}
        {!! Html::style('assets/css/app.css') !!}
        @yield('css')


    </head>
    <body class="archive post-type-archive post-type-archive-ib_educator_course has-toolbar">
        <div id = "navbar">
            @yield('navbar')
        </div>
        <div id="page-container">
            @yield('content')
        </div>
        <div id = "footer">
            @yield('footer')
        </div>
        {!! Html::script('assets/bower_components/jquery/dist/jquery.min.js') !!}
        {!! Html::script('assets/vendor/jquery.js') !!}
        {!! Html::script('assets/js/modernizr.js') !!}
        {!! Html::script('assets/js/base.js') !!}
        {!! Html::script('assets/js/bootstrap.min.js') !!}
        {!! Html::script('assets/js/jquery.flexslider.js') !!}
        {!! Html::script('assets/js/jquery.magnific-popup.js') !!}
        {!! Html::script('assets/js/jquery.validate.js') !!}
        {!! Html::script('assets/js/owl.carousel.js') !!}
        {!! Html::script('assets/js/main.js') !!}
        {!! Html::script('assets/js/views/view.contact.advanced.js') !!}
        {!! Html::script('assets/js/views/view.contact.js') !!}
        {!! Html::script('assets/js/views/view.home.js') !!}
        {!! Html::script('assets/bower_components/jquery-ui/jquery-ui.min.js') !!}
        {!! Html::script('assets/bower_components/noty/js/noty/packaged/jquery.noty.packaged.min.js') !!}
        {!! Html::script('assets/js/main.min.js') !!}
        @yield('js')


    </body>
</html>
