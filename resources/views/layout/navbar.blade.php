<div id="page-toolbar">
    <div class="container clearfix nopadding">
        <div class="toolbar-items" style="display:inline-block;">
            <div class="item">
                <div class="inner">
                    {!!get_info()!!}
                </div>
                
            </div>
        </div>
    </div>
     
</div>



<header id="page-header" class="fixed-header">
    <div id="page-header-inner">
        <div id="header-container">
            <div class="container clearfix nopadding">
                <div id="main-logo">
                    <a href="">
                        <img src="{!!get_url()!!}/img/logo.png" srcset="{!!get_url()!!}/img/logo.png 1x, {!!get_url()!!}/img/logo.png 2x" alt=""> </a>
                </div>

                <nav id="main-nav">
                    <ul id="menu-main-menu" class="menu">
                        @if (Auth::guest())

                            <li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent">
                                <a href="{{route('login')}}">登录</a>
                            </li>
                            {{--
                                              <li class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent">
                                              <a href="{{route('register')}}">注册</a>
                                              </li>
                                              --}}
                        @else
                            <li id="indexLink" class="menu-item menu-item-type-post_type menu-item-object-page">
                                <a href="/">主页</a>
                            </li>
                            <li id="lessonsLink"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-768">
                                <a>我的培训</a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-812">
                                        <a href="/lessons">进行中</a></li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-785">
                                        <a href="/lessons-done">已完成</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-785">
                                        <a href="/lessons-overdue">已过期</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="resourcesLink" class="menu-item menu-item-type-custom menu-item-object-custom">
                                <a href="/resources">资料库</a>
                            </li>
                            <li id="postsLink"
                                class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent">
                                <a href="/posts">讨论区</a>
                            </li>

                            @can('manage_report',Auth::user())
                            <li id="reportLink"
                                class="menu-item menu-item-type-post_type menu-item-object-page current_page_parent">
                                <a href="/reports">报表</a>
                            </li>
                            @endcan
                            @can('manage_lesson',Auth::user())
                            <li id="courseLink"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-769">
                                <a>学习管理</a>
                                <ul class="sub-menu">
                                    
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/courses">课程管理</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/exams">考试管理</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/question-bank">题库</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/evaluation">课程评估</a>
                                    </li>
                                </ul>
                            </li>
                            @endcan
                            @can('manage_user',Auth::user())
                            <li id="userManageLink"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-769">
                                <a>{{ trans('user.manage') }}</a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/users">{{ trans('user.manage') }}</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/organization">组织结构管理</a>
                                    </li>
                                    @can('import_users',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/users/import">用户导入</a>
                                    </li>
                                    @endcan
                                    @can('import_organization',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/organization/import">组织结构导入</a>
                                    </li>
                                    @endcan
                                </ul>
                            </li>
                            @endcan
                            @can('manage_system',Auth::user())
                            <li id="systemLink"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-769">
                                <a>系统管理</a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/news-manage">{{ trans('news.manage') }}</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/survey">{{ trans('survey.manage') }}</a>
                                    </li>
                                   
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/resources-manage">资料库管理</a>
                                    </li>

                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/posts-manage">讨论区管理</a>
                                    </li>
                                    @can('manage_emailTemplet',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/email-manage">邮件管理</a>
                                    </li>
                                    @endcan

                                    @can('manage_logs',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/log-manage">日志管理</a>
                                    </li>
                                    @endcan
                                    @can('manage_info',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/system-manage">配置管理</a>
                                    </li>
                                    @endcan
                                    @can('manage_catalog',Auth::user())
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/catalogs">目录管理</a>
                                    </li>
                                    @endcan
                                </ul>
                            </li>
                            @endcan


                            <li id="profileLink"
                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-769">
                            <a href="">{{ Auth::user()->name }}({{Auth::user()->role->name}})</a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <a href="/profile">个人信息</a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        @can('student',Auth::user())
                                        <a id="userManualChange" href="/manual/user/pc">使用帮助</a>
                                        @else
                                        <a href="/manual/manager">使用帮助</a>
                                        @endcan

                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-784">
                                        <li><a href="{{route('logout')}}">注销</a></li>
                                </ul>
                                    </li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
