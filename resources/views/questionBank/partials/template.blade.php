@extends('layout.master')

@section('content')
    @include('questionBank.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection