<div class="leftpanel">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">
                <h5 class="sidebar-title"></h5>
                <ul class="nav nav-pills nav-stacked nav-quirk">
                    <li id="logoNav">
                        <a href="/question-bank/{{$bank->id}}/edit">
                            <i class="fa fa-home"></i><span>{{ trans('bank.manage') }}</span>
                        </a>
                    </li>
                    <li id="messageNav">
                        <a href="/question-bank/{{$bank->id}}/questions">
                            <i class="fa fa-pencil"></i><span>{{ trans('question.manage') }}</span>
                        </a>
                    </li>
                    <li id="messageNav">
                        <a href="/question-bank/{{$bank->id}}">
                            <i class="fa fa-eye"></i><span>{{ trans('button.preview') }}</span>
                        </a>
                    </li>
                    <li id="messageNav">
                        <a href="/question-bank">
                            <i class="fa fa-reply"></i><span>{{ trans('button.return') }}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
