@extends('questionBank.partials.template')
@section('container')
    <div class="contentpanel">
        <div class="panel panel-announcement">
            <div class="panel-heading">
                <h2 class="panel-title">{{ trans('button.preview') }}</h2>
            </div>
        </div>
        
        <div class="panel panel-announcement">
            <div class="panel-body">
                <div class="col-md-10 col-md-offset-1">
                    <h5 style="color: #337ab7;"> {{$bank->title}}</h5>
                    
                    @foreach($bank->questions as $index=>$question)
                        <div style="border-top: solid 20px #efefef;">
                            <h5 style="color: #337ab7;">{{$index+1}}.{!!$question->title!!}</h5>
                            @include('questions.select_show')
                           
                        </div>

                    @endforeach
                    
                </div>
            </div>
        </div>
    </div>
@endsection

