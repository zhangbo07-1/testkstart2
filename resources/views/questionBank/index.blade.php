@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>
            <h3>{{ trans('bank.title') }}</h3>
            <div class="row">
                <div class="col-sm-12 main">
                    <div class="table-responsive">
                        <div align="right">
                            <a href="{{URL('question-bank/create')}}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-plus"></i>&nbsp;{{ trans('bank.create') }}</a>
                        </div>

                        <div class="courseTable text-center">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>{{ trans('bank.title') }}</th>
                                        <th>{{ trans('table.Qnum') }}</th>            
                                        <th>{{ trans('table.create_time') }}</th>
                                        <th>{{ trans('table.operation') }}</th>
                                    </tr>
                                </thead>
                                <tbody>



                                    @foreach($banks as $bank)


                                        <tr ng-repeat="trainee in trainees" class="ng-scope">
                                            <td class="vertical-align-middleg">{{ $bank->title }}</td>
                                            <td class="vertical-align-middle">{{ count($bank->questions) }}</td>
                                            <td class="vertical-align-middle">{{ $bank->created_at }}</td>
                                            <td class="vertical-align-middle">

                                                                                                                                         <a href="{{ url('/question-bank/'.$bank->id.'/edit') }}" class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-pencil"></i>&nbsp;{{ trans('button.edit') }}</a>

                                                <form action="{{ URL('question-bank/'.$bank->id) }}" method="POST" style="display: inline;">
                                                    <input name="_method" type="hidden" value="DELETE">
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                    <button type="submit"  onClick="return ifDelete()" class="dm3-btn dm3-btn-medium button-large dm3-btn-red"><i class="fa fa-times"></i>&nbsp;{{ trans('button.delete') }}</button>

                                                </form>

                                            </td>
                                        </tr><!-- end ngRepeat: trainee in trainees -->
                                    @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
            {!! $banks->render() !!}
        </div>
    @endsection


