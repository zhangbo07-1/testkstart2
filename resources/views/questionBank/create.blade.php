@extends('layout.master')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>
            <h3>{{ trans('bank.create') }}</h3>
            <div class="row">
                <div class="col-md-5 col-md-offset-3">
                    <div class="panel">

                        <div class="panel-body">
                            <a>{{ trans('bank.title') }}</a>
                            <form action="{{ URL('question-bank') }}" method="POST">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <input type="text" name="title" class="form-control" required="required">
                                {!! errors_for('title', $errors) !!}
                                <br>
                                <a>{{ trans('table.range') }}</a>
                                <br>
                                <div style="display:inline-block;">

                                    <a href="javascript:manageSelect();" class="dm3-btn dm3-btn-medium">{{ trans('button.range') }}</a>
                                </div>
                                <div style="display:inline-block;" id="manage_name"></div>
                                <input type="hidden" id="manage_id"  name="manage_id" value=""/>
                                <br>
                                <br>
                                <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                                <a href="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endsection
