@extends('layout.master')

@include('UMEditor.head')
@section('content')


    <div class="container">
        <br>
        <br>
        <h3>{{ trans('email.manage') }}</h3>
        <div class="row">
            <div class="col-md-120">
                <div class="panel">

                    <div class="panel-body">
                        <form id="TypeSelect"class="form-inline" action="/email-manage">
                            
                            <input type="radio" id="Select1" onClick="change()" name="Etype" value="1" />
                            {{ trans('email.type_1') }}&nbsp;&nbsp;

                            <input type="radio" id="Select2" onClick="change()" name="Etype" value="2" />
                            {{ trans('email.type_2') }}&nbsp;&nbsp;

                            <input type="radio" id="Select3" onClick="change()" name="Etype" value="3" />
                            {{ trans('email.type_3') }}&nbsp;&nbsp;
                            <input type="radio" id="Select4" onClick="change()" name="Etype" value="4" />
                            {{ trans('email.type_4') }}&nbsp;&nbsp;

                            <input type="radio" id="Select5" onClick="change()" name="Etype" value="5" />
                            {{ trans('email.type_5') }}&nbsp;&nbsp;

                            <input type="radio" id="Select6" onClick="change()" name="Etype" value="6" />
                            {{ trans('email.type_6') }}&nbsp;&nbsp;

                            <input type="radio" id="Select7" onClick="change()" name="Etype" value="7" />
                            {{ trans('email.type_7') }}&nbsp;&nbsp;
                            
                            <input type="radio" id="Select8" onClick="change()" name="Etype" value="8" />
                            {{ trans('email.type_8') }}&nbsp;&nbsp;
                            
                            <input type="radio" id="Select9" onClick="change()" name="Etype" value="9" />
                            {{ trans('email.type_9') }}&nbsp;&nbsp;
                        </form>
                        <br>
                        <br>
                        <br>
                        <form action="{{ URL('/email-manage/') }}" method="POST">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" id="type" name="type_id" value="{{ $type }}">
                            <div>
                                <a>{{ trans('table.title') }}</a>
                                <br>
                                <input type="text" name="title" class="form-control" required="required" value="{{$email->title}}">


                                <br>
                                <br>

                                <a>{{ trans('table.text') }}</a>
                                <br>

                                <textarea id="myEditor" name="body" class="form-control" style="height:240px;" required="required">{{$email->body}}</textarea>
                            </div>

                            <br>
                            <br>
                            <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@section('js')
    <script type = "text/javascript">
 
         type =  $("#type").attr("value");
         $("#Select"+type).attr("checked","checked");
  
     
     function change()
     {
         
         $('#TypeSelect').submit();

     }
     

    </script>

  @append  

@endsection
