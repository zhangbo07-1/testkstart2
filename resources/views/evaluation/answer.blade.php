@extends('layout.master')
    @section('js')

        {!! Html::script('assets/js/views/view.operation.js') !!}
        @append
@section('content')


    <div class="container">
        <br>
        <br>
        <h3>开始评估</h3>
        
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h4 style="color: #337ab7;">{{$survey->title}}</h4>
                <div class="panel-body">
                    <form action="{{ URL('/lessons/'.$course->course_id.'/evaluation/'.$survey->id.'/score') }}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                       
                        @foreach($survey->items as $index=>$item)
                            <div style="border-top: solid 20px #efefef;">
                                <h5 style="color: #337ab7;">{{$index+1}}.{{$item->title}}</h5>
                                {!! errors_for($item->id, $errors) !!}
                                @if($item->type == 1)
                                    @include('items.select_show')
                                @else
                                    @include('items.grade_show')
                                @endif
                            </div>
                        @endforeach
                        
                        <button class="dm3-btn dm3-btn-medium button-large" onClick="return checkResult({{count($survey->items)}})">完成</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

