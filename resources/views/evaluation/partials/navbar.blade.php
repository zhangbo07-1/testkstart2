<div class="leftpanel">
    <div class="leftpanelinner">
        <div class="tab-content">
            <div class="tab-pane active" id="mainmenu">
                <h5 class="sidebar-title"></h5>
                <ul class="nav nav-pills nav-stacked nav-quirk">
                    <li id="logoNav">
                        <a href="/evaluation/{{$survey->id}}/edit">
                            <i class="fa fa-home"></i><span>{{ trans('evaluation.manage') }}</span>
                        </a>
                    </li>
                    <li id="messageNav">
                        <a href="/evaluation/{{$survey->id}}/items">
                            <i class="fa fa-pencil"></i><span>{{ trans('question.manage') }}</span>
                        </a>
                    </li>
                    <li id="messageNav">
                        <a href="/evaluation/{{$survey->id}}">
                            <i class="fa fa-eye"></i><span>{{ trans('button.preview') }}</span>
                        </a>
                    </li>
                    @if($survey->pushed == 0)
                        <li id="messageNav">
                            <a href="/survey/{{$survey->id}}/push">
                                <i class="fa fa-save"></i><span>{{ trans('button.push') }}</span>
                            </a>
                        </li>
                    @endif

                    @if(count($survey->surveyResults))

                        <li id="messageNav">
                            <a href="/evaluation/{{$survey->id}}/result">
                                <i class="fa fa-question-circle"></i><span>{{ trans('button.result') }}</span>
                            </a>
                        </li>

                    @endif
                    <li id="messageNav">
                        <a href="/evaluation">
                            <i class="fa fa-reply"></i><span>{{ trans('button.return') }}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
