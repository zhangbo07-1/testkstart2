@extends('layout.master')

@section('content')
    @include('evaluation.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection