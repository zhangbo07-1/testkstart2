
@extends('layout.master')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>

            <div class="row">
     <div class="col-md-12 ">
                <div  align="right">
                    <a class="dm3-btn dm3-btn-medium button-large" href="javascript:window.location.href=document.referrer; " ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>

                </div>
</div>
            </div>
                <div class="row">
                    <div class="col-md-12 " align="center">
                        {!! Form::open(array('url' => 'users', 'method' => 'POST')) !!}
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>{{ trans('user.title') }}:</td>
                                    <td>
                                    {!! Form::text('name',null,['class'=>"form-control"]) !!}
                                        
                                        {!! errors_for('name', $errors) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.email') }}:</td>
                                    <td> {!! Form::text('email',null,['class'=>"form-control"]) !!}
                                        {!! errors_for('email', $errors) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.pwd') }}:</td>
<td> {!! Form::password('password',['class'=>"form-control"]) !!}
                                        {!! errors_for('password', $errors) !!}</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.real_name') }}:</td>
                                    <td> {!! Form::text('realname',null,['class'=>"form-control"]) !!}
                                        {!! errors_for('realname', $errors) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.role') }}:</td>
                                    <td>
                                    {!! Form::select('role_id', $roleSelect,null,['id'=>'role_id','onClick'=>'setRole()','class'=>"form-control"]) !!}
                                        {!! errors_for('role_id', $errors) !!}
                                    </td>
                                </tr>
                                    <tr id="user_ranger" style="display:none">
                                        <td>{{ trans('user.ranger') }}:</td>
                                        <td>
                                            <div style="display:inline-block;">
                                                <a href="javascript:manageSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.range') }}</a>
                                            </div>
                                            <div style="display:inline-block;" id="manage_name"></div>
                                            <input type="hidden" id="manage_id"  name="manage_id" value=""/>
                                            {!! errors_for('manage_id', $errors) !!}
                                        </td>
                                    </tr>
                                <tr>
                                    <td>{{ trans('user.department') }}:</td>
                                    <td>
                                        <div style="display:inline-block;">
                                            <a href="javascript:departmentSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.department') }}</a>
                                        </div>
                                        <div style="display:inline-block;" id="department_name"></div>
                                        <input type="hidden" id="department_id"  name="department_id" value=""/>
                                        {!! errors_for('manage_id', $errors) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.position') }}:</td>
                                    <td>{!! Form::text('position',null,['class'=>"form-control"]) !!}
                                        {!! errors_for('position', $errors) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.rank') }}:</td>
                                    <td>{!! Form::text('rank',null,['class'=>"form-control"]) !!}
                                        {!! errors_for('rank', $errors) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('table.language') }}:</td>
                                    <td>
                                    {!! Form::select('language_id', $languageSelect,null,['class'=>"form-control"]) !!}
                                        {!! errors_for('language_id', $errors) !!}
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        
                        <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                        <a href="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
    @endsection
