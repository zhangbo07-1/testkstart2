@extends('layout.master')
@section('js')

    {!! Html::script('assets/js/views/view.operation.js') !!}
    <script>
     setRole();
    </script>
    @append
    @section('content')
        <div class="container">
            <br>

            <div class="row">
    <div class="col-md-12" >
                <div  align="right">
                    <a class="dm3-btn dm3-btn-medium button-large" href="javascript:window.location.href=document.referrer; " ><i class="fa fa-reply"></i>&nbsp;{{ trans('button.return') }}</a>
                </div>
</div>
            </div>
                <div class="row">
                    <div class="col-md-12" align="center">
                        {!! Form::open(array('url' => 'users/'.$user->name, 'method' => 'put')) !!}
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>{{ trans('user.title') }}:</td>
                                    <td>{{ $user->name }}</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.pwd') }}:</td>
<td> {!! Form::password('password',['placeholder'=>'******','class'=>"form-control"]) !!}
                                        {!! errors_for('password', $errors) !!}</td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.role') }}:</td>
                                    <td>
                                    {!! Form::select('role_id', $roleSelect, $user->role_id,['id'=>'role_id','onClick'=>'setRole()','class'=>"form-control"]) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('table.status') }}:</td>
                                    <td>
                                        {!! Form::select('userStatus', $userStatusSelects, $user->status_id,['class'=>"form-control"]) !!}
                                    </td>
                                </tr>
                                <tr id="user_ranger" style={{$user->role_id > 1 ? "":"display:none"}}>
                                    <td>{{ trans('user.ranger') }}:</td>
                                    <td>
                                        <div style="display:inline-block;">
                                            <a href="javascript:manageSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.range') }}</a>
                                        </div>
                                        <div style="display:inline-block;" id="manage_name">{{$manages}}</div>
                                        <input type="hidden" id="manage_id"  name="manage_id" value=""/>
                                        {!! errors_for('manage_id', $errors) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.real_name') }}:</td>
                                    <td> {!! Form::text('realname',$user->profile->realname,['class'=>"form-control"]) !!}
                                        {!! errors_for('realname', $errors) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.department') }}:</td>
                                    <td>
                                        <div style="display:inline-block;">
                                            <a href="javascript:departmentSelect();" class="dm3-btn dm3-btn-medium button-large">{{ trans('button.department') }}</a>
                                        </div>
                                        <div style="display:inline-block;" id="department_name">{{$user->department->name}}</div>
                                        <input type="hidden" id="department_id"  name="department_id" value="{{$user->department_id}}"/>
                                        {!! errors_for('manage_id', $errors) !!}

                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.position') }}:</td>
                                    <td>{!! Form::text('position',$user->profile->position,['class'=>"form-control"]) !!}
                                        {!! errors_for('position', $errors) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.rank') }}:</td>
                                    <td>{!! Form::text('rank',$user->profile->rank,['class'=>"form-control"]) !!}
                                        {!! errors_for('rank', $errors) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('table.language') }}:</td>
                                    <td>
                                        {!! Form::select('language_id', $languageSelect, $user->language_id,['class'=>"form-control"]) !!}
                                    </td>
                                </tr>
                                <tr>
                                    <td>{{ trans('user.email') }}</td>
                                    <td><a href="mailto::{{ $user->email }}">{{ $user->email }}</a></td>
                                </tr>
                            </tbody>
                        </table>
                        <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        
    @endsection
