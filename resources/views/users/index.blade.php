@extends('layout.master')
@section('js')
    {!! Html::script('assets/js/views/view.operation.js') !!}
    @append
    @section('content')
        <div class="container">
            <br>
            <br>
            <h3>{{ trans('user.manage') }}</h3>
            <div class="row">
                <div class="col-sm-12 main">
                    <form class="form-inline" action="/users">

                        <input type="hidden" id="manage_id" class="form-control"  name="manage_id" value=""/>
                        <a href="javascript:manageSelect();" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-user"></i>&nbsp;{{ trans('button.department') }}</a>
                        <div style="display:inline-block;" id="manage_name"></div>
                        <br>

                        <div style="float: left;">

                        {!! Form::select('userStatus', $userStatusSelects, $status,['class'=>"form-control"]) !!}

                            <div class="form-group">
                                <input type="text"  class="form-control" name="findByUsername" placeholder="{{ trans('table.input_select_user') }}">
                            </div>
                            <button type="submit" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-search"></i>&nbsp;{{ trans('button.search') }}</button>
                        </div>

                        <div style="float: right;">

                            <a style="text-align:right" href="{{URL('users/create')}}" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-plus"></i>&nbsp;{{ trans('user.create') }}</a>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 main">
                    <div class="table-responsive">
                        <div class="courseTable text-center">
                            <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:100%;">
                                <table class="table table-striped table-bordered text-center">
                                    <thead>
                                        <tr>
                                            <th>{{ trans('table.user_name') }}</th>
                                            <th>{{ trans('user.real_name') }}</th>
                                            <th>{{ trans('user.role') }}</th>
                                            <th>{{ trans('table.email') }}</th>
                                            <th>{{ trans('table.status') }}</th>
                                            <th>{{ trans('table.create_time') }}</th>
                                            <th>{{ trans('table.operation') }}</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($users as $user)
                                            <tr>
                                                <td class="vertical-align-middle">{{$user->name}}</td>
                                                <td class="vertical-align-middle">{{$user->profile->realname}}</td>
                                                <td class="vertical-align-middle">{{$user->role->name}}</td>
                                                <td class="vertical-align-middle">{{$user->email}}</td>
                                                <td class="vertical-align-middle">{{$user->userStatus->title}}</td>
                                                <td class="vertical-align-middle">{{$user->created_at}}</td>
                                                <td class="vertical-align-middle">
                                                    <a href="{{ url('/users/'.$user->name.'/edit') }}" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-edit"></i>&nbsp;{{ trans('button.edit') }}</a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            {!! $users->render() !!}
                        </div>
 <h5 style="color: #337ab7;">人数:{{count($users)}}</h5>
                    </div>
                </div>
            </div>
        </div>
        {{--        <script id="userTemplate" type="text/x-handlebars-template">
        @{{#each data}}
        <tr>
        <td class="vertical-align-middle">@{{name}}</td>
        <td class="vertical-align-middle">@{{profile.realname}}</td>
        <td class="vertical-align-middle">@{{role.name}}</td>
        <td class="vertical-align-middle">@{{email}}</td>
        <td class="vertical-align-middle">@{{created_at}}</td>
        <td class="vertical-align-middle">
        <a href="/users/@{{name}}/edit"
        class="dm3-btn dm3-btn-medium button-large"><i class="fa fa-plus"></i>&nbsp;编辑</a>
        </td>
        </tr>
        @{{/each}}
        </script>  --}}
    @endsection
