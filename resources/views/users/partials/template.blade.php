@extends('layout.master')

@section('css')
    {!! Html::style('assets/bower_components/bootstrap/dist/css/bootstrap.min.css') !!}
    {!! Html::style('assets/jstree/dist/themes/default/style.min.css') !!}
    @append
    @section('js')
        {!! Html::script('assets/bower_components/handlebars/handlebars.min.js') !!}
        {!! Html::script('assets/bower_components/jquery-bootpag/lib/jquery.bootpag.min.js') !!}
        {!! Html::script('assets/bower_components/spin.js/spin.js') !!}
        {!! Html::script('assets/jstree/dist/jstree.js') !!}
        {!! Html::script('assets/js/user.js') !!}
        @append

@section('content')
    @include('users.partials.navbar')
    <div class="mainpanel">
        {{--@yield('title')--}}
        @yield('container')
    </div>
@endsection
