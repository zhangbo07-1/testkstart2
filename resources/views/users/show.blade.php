@extends('layout.master')
@section('content')
    <div class="container">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12 " align="center">
                    <br>
                    <br>
                    {!! Form::open(array('url' => 'users/'.$user->name, 'method' => 'put')) !!}
                    <table class="table">
                        <tbody>
                            <tr>
                                <td>{{ trans('user.title') }}:</td>
                                <td>{{ $user->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.pwd') }}:</td>
<td> {!! Form::password('password',['placeholder'=>'******','class'=>"form-control"]) !!}
                                    {!! errors_for('password', $errors) !!}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.real_name') }}:</td>
                                <td>
                                {!! Form::text('realname',$user->profile->realname,['class'=>"form-control"]) !!}
                                    {!! errors_for('realname', $errors) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.department') }}:</td>
                                <td>{{ $user->department->name }}</td>
                            </tr>
                            <tr>
                                <td>{{ trans('user.position') }}:</td>
                                <td>{!! Form::text('position',$user->profile->position,['class'=>"form-control"]) !!}
                                    {!! errors_for('position', $errors) !!}
                            </tr>
                            <tr>
                                <td>{{ trans('user.rank') }}:</td>
                                <td>{!! Form::text('rank',$user->profile->rank,['class'=>"form-control"]) !!}
                                    {!! errors_for('rank', $errors) !!}
                            </tr>
                            <tr>
                                <td>{{ trans('table.language') }}:</td>
                                <td>
                                    {!! Form::select('language_id', $languageSelect, $user->language_id,['class'=>"form-control"]) !!}
                                </td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                            </tr>

                        </tbody>
                    </table>
                    <button class="dm3-btn dm3-btn-medium button-large">{{ trans('button.submit') }}</button>
                    <a href="javascript:window.location.href=document.referrer; " class="dm3-btn dm3-btn-medium dm3-btn-red button-large">{{ trans('button.cancel') }}</a>
                        {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
