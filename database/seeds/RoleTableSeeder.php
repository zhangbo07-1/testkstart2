<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'tag' => 'S',

            'name'=> '学员'
        ]);
        Role::create([
            'tag' => 'I',

            'name'=> '教师'
        ]);
        Role::create([
            'tag' => 'A',

            'name'=> '普通管理员'
        ]);
        Role::create([
            'tag' => 'SA',

            'name'=> '超级管理员'
        ]);
    }
}
