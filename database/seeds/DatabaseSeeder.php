<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(RoleTableSeeder::class);
        $this->call(UserStatusTableSeeder::class);
        $this->call(OrganizationsTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(EmailTempletTableSeeder::class);
        $this->call(SystemInfoSeeder::class);
	$this->call(CatalogsTableSeeder::class);
        $this->call(LanguageTableSeeder::class);
        Model::reguard();
    }
}
