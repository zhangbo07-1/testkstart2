<?php

use Illuminate\Database\Seeder;
use App\Models\UserStatus;
class UserStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserStatus::create([
            'title' => '未激活',
        ]);

        UserStatus::create([
            'title' => '活动',
        ]);

        UserStatus::create([
            'title' => '待审核',
        ]);

        UserStatus::create([
            'title' => '关闭',
        ]);
    }
}
