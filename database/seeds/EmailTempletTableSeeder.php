<?php

use Illuminate\Database\Seeder;
use App\Models\EmailTemplet;

class EmailTempletTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmailTemplet::create([
            'title' => '课程分配提醒',
            'type'  => 1,
            'body'  => '<p>
    <span style="font-family:宋体, SimSun;font-size:16px;color:#1f497d">亲爱的</span><span style="font-family:宋体, SimSun;font-size:16px;color:#ff0000">{{用户名}}</span><span style="font-family:宋体, SimSun;font-size:16px;color:#1f497d">：</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;font-size:16px;color:#1f497d">您被邀请参与</span><span style="font-family:宋体, SimSun;font-size:16px;color:#ff0000">{{课程标题}}</span><span style="font-family: 宋体, SimSun; font-size: 16px;"><span style="color:#1f497d">课程的学习，请通过点击以下链接及时登入系统进行学习</span><a href="http://{{用户网址}}" target="_blank"><span style="color:#ff0000">{{用户网址}}</span></a><span style="color:#1f497d">。&nbsp;</span></span>
</p>
<p>
    <span style="font-family:宋体, SimSun;font-size:16px;color:#1f497d">请务必在收到此邮件后的一周内完成网上课程。否则您将会于每周收到您在系统中未完成学习内容的提醒通知。&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;font-size:16px;color:#1f497d">谢谢您的关注。祝您学习顺利！&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;font-size:16px;color:#1f497d">（本电子邮件由系统自动发送，请勿直接回复。）&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;font-size:16px;color:#1f497d">良好的祝愿。</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;font-size:16px;color:#1f497d"><a href="http://www.knowsurface.com" target="_blank">KnowSurface</a></span>
</p>
<p>
    <br/>
</p>'
        ]);

        EmailTemplet::create([
            'title' => '课程催促邮件',
            'type'  => 2,
            'body'  => '<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">亲爱的</span><span style="font-family:宋体, SimSun;color:#ff0000">{{用户名}}</span><span style="font-family:宋体, SimSun;color:#1f497d">:&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">有记录显示，您被要求登陆KnowSurface培训平台参加学习。但是到目前为止，您的以下课程的培训状态显示仍为“未完成”。请使用之前获得的用户名和密码，登陆</span><span style="font-family:宋体, SimSun;color:#ff0000"><a href="http://{{用户网址}}" target="_blank"><span style="color:#ff0000">{{用户网址}}</span></a></span><span style="font-family:宋体, SimSun;color:#1f497d">， 尽快完成培训。&nbsp;</span>
</p>
<p>
    <span style="font-family: 宋体, SimSun; line-height: 22.8571434020996px; white-space: normal;color:#ff0000">{{课程标题}}</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">谢谢您的关注。祝您学习顺利！&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">（本电子邮件由系统自动发送，请勿直接回复。）&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">良好的祝愿，</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d"><a href="http://www.knowsurface.com" target="_blank">KnowSurface</a></span>
</p>
<p>
    <br/>
</p>'
        ]);


        EmailTemplet::create([
            'title' => '课程完成提醒',
            'type'  => 3,
            'body'  => '<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">亲爱的</span><span style="font-family:宋体, SimSun;color:#ff0000">{{用户名}}</span><span style="font-family:宋体, SimSun;color:#1f497d">：&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">恭喜！您已经顺利完成了</span><span style="font-family:宋体, SimSun;color:#ff0000">{{课程标题}}</span><span style="font-family:宋体, SimSun;color:#1f497d">课程的学习，谢谢。&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">谢谢您的关注。祝您学习顺利！&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">（本电子邮件由系统自动发送，请勿直接回复。）&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">良好的祝愿，</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d"><a href="http://www.knowsurface.com" target="_blank">KnowSurface</a></span>
</p>
<p>
    <br/>
</p>'
        ]);

        EmailTemplet::create([
            'title' => '课程到期提醒',
            'type'  => 4,
            'body'  => '<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">亲爱的</span><span style="font-family:宋体, SimSun;color:#ff0000">{{用户名}}</span><span style="font-family:宋体, SimSun;color:#1f497d">：</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">您的培训课程</span><span style="font-family:宋体, SimSun;color:#ff0000">{{课程标题}}</span><span style="font-family:宋体, SimSun;color:#1f497d">将在 </span><span style="font-family:宋体, SimSun;color:#ff0000">{{到期日期}} </span><span style="font-family:宋体, SimSun;color:#1f497d">到期，请使用之前获得的用户名和密码，登陆</span><span style="font-family:宋体, SimSun;color:#ff0000"><a href="http://{{用户网址}}" target="_blank"><span style="color:#ff0000">{{用户网址}}</span></a></span><span style="font-family:宋体, SimSun;color:#1f497d">， 尽快完成培训。&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">谢谢您的关注。祝您学习顺利！&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">（本电子邮件由系统自动发送，请勿直接回复。）&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">良好的祝愿，</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d"><a href="http://www.knowsurface.com" target="_blank">KnowSurface</a></span>
</p>
<p>
    <br/>
</p>'
        ]);

        EmailTemplet::create([
        'title' => '调研问卷邀请',
        'type'  => 5,
        'body'  => '<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">亲爱的</span><span style="font-family:宋体, SimSun;color:#ff0000">{{用户名}}</span><span style="font-family:宋体, SimSun;color:#1f497d">：</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">诚挚邀请您参与本次的问卷的调查，您的参与和建议对我们非常重要，请通过点击以下链接登入系统参与调研</span><span style="font-family:宋体, SimSun;color:#ff0000"><a href="http://{{用户网址}}" target="_blank"><span style="color:#ff0000">{{用户网址}}</span></a></span><span style="font-family:宋体, SimSun;color:#1f497d">，请在7天内完成并提交线上问卷。</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">谢谢您的关注。祝您学习顺利！&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">（本电子邮件由系统自动发送，请勿直接回复。）&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">良好的祝愿，</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d"><a href="http://www.knowsurface.com" target="_blank">KnowSurface</a></span>
</p>
<p>
    <br/>
</p>'
    ]);

        EmailTemplet::create([
            'title' => '注册认证邮件',
            'type'  => 6,
            'body'  => '<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">亲爱的</span><span style="font-family:宋体, SimSun;color:#ff0000">{{用户名}}</span><span style="font-family:宋体, SimSun;color:#1f497d">：</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">请点击以下地址完成邮件注册:</span><span style="font-family:宋体, SimSun;color:#ff0000"><a href="http://{{认证网址}}" target="_blank"><span style="color:#ff0000">{{认证网址}}</span></a></span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">（本电子邮件由系统自动发送，请勿直接回复。）&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">良好的祝愿，</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d"><a href="http://www.knowsurface.com" target="_blank">KnowSurface</a></span>
</p>
<p>
    <br/>
</p>'
        ]);

        EmailTemplet::create([
            'title' => '重新发送邮件',
            'type'  => 7,
            'body'  => '<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">亲爱的</span><span style="font-family:宋体, SimSun;color:#ff0000">{{用户名}}</span><span style="font-family:宋体, SimSun;color:#1f497d">：</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">请点击以下地址完成邮件注册:</span><span style="font-family:宋体, SimSun;color:#ff0000"><a href="http://{{认证网址}}" target="_blank"><span style="color:#ff0000">{{认证网址}}</span></a></span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">（本电子邮件由系统自动发送，请勿直接回复。）&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">良好的祝愿，</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d"><a href="http://www.knowsurface.com" target="_blank">KnowSurface</a></span>
</p>
<p>
    <br/>
</p>'
        ]);

        EmailTemplet::create([
            'title' => '审核申请',
            'type'  => 8,
            'body'  => '<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">亲爱的</span><span style="font-family:宋体, SimSun;color:#ff0000">{{用户名}}</span><span style="font-family:宋体, SimSun;color:#1f497d">：</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">有新学员<span style="color: rgb(255, 0, 0); font-family: 宋体, SimSun; line-height: 22.8571434020996px; white-space: normal;">{{申请用户名}}</span>注册成功，用户邮箱为：<span style="color: rgb(255, 0, 0); font-family: 宋体, SimSun; line-height: 22.8571434020996px; white-space: normal;">{{申请邮箱}}</span>。</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">请点击以下地址完成用户审核过程:</span><span style="font-family:宋体, SimSun;color:#ff0000"><a href="http://{{用户网址}}" target="_blank"><span style="color:#ff0000">{{用户网址}}</span></a></span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">（本电子邮件由系统自动发送，请勿直接回复。）&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">良好的祝愿，</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d"><a href="http://www.knowsurface.com" target="_blank">KnowSurface</a></span>
</p>
<p>
    <br/>
</p>'
        ]);


EmailTemplet::create([
            'title' => '审核完成通知',
            'type'  => 9,
            'body'  => '<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">亲爱的</span><span style="font-family:宋体, SimSun;color:#ff0000">{{用户名}}</span><span style="font-family:宋体, SimSun;color:#1f497d">：</span>
</p>
<p>
    <span style="font-family: 宋体, SimSun;color:#1f497d">管理员已通过您的审核申请，请通过电脑或手机浏览器访问以下地址进行学</span><span style="font-family:宋体, SimSun;color:#1f497d">习:</span><span style="font-family:宋体, SimSun;color:#ff0000"><a href="http://{{用户网址}}" target="_blank"><span style="color:#ff0000">{{用户网址}}</span></a></span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">使用您注册时填写的邮箱和密码登入系统，用户手册在“主页”右上角点击用户名“使用帮助”中，祝您学习愉快！</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">（本电子邮件由系统自动发送，请勿直接回复。）&nbsp;</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d">良好的祝愿，</span>
</p>
<p>
    <span style="font-family:宋体, SimSun;color:#1f497d"><a href="http://www.knowsurface.com" target="_blank">KnowSurface</a></span>
</p>
<p>
    <br/>
</p>'
        ]);
    }
}
