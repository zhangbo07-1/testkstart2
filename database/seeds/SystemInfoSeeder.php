<?php

use Illuminate\Database\Seeder;
use App\Models\SystemInfo;
class SystemInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SystemInfo::create();
    }
}
