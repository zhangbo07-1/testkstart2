<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('filename')->nullable();
            $table->string('storagename');
            $table->integer('catalog_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('size');
            $table->integer('type');//1:file,2:url
            $table->timestamps();
        });
        Schema::table('resources', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('restrict');
            $table->foreign('catalog_id')->references('id')->on('organizations')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('resources', function(Blueprint $table) {
            $table->dropForeign('resources_user_id_foreign');
            $table->dropForeign('resources_catalog_id_foreign');
        });
        Schema::drop('resources');
    }
}
