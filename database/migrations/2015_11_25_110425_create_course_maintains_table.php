<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCourseMaintainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('course_maintains', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id')->unsigned();
            $table->integer('maintain_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('course_maintains', function(Blueprint $table) {
            $table->foreign('model_id')->references('id')->on('courses')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('maintain_id')->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('course_maintains', function(Blueprint $table) {
            $table->dropForeign('course_maintains_model_id_foreign');
            $table->dropForeign('course_maintains_maintain_id_foreign');
        });
        Schema::drop('course_maintains');
    }
}
