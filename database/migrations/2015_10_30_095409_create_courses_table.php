<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('course_id');
            $table->string('title')->nullable();
            $table->integer('language_id')->default(1);
            $table->integer('catalog_id')->nullable();
            $table->integer('user_id');
            $table->integer('hours')->default(0);
            $table->integer('minutes')->default(0);
            $table->integer('limitdays')->default(0);
            $table->string('target')->nullable();
            $table->text('description')->nullable();
            $table->integer('ware_id')->nullable();
            $table->boolean('emailed')->default(true);
            $table->boolean('if_send_new_user')->default(false);
            $table->boolean('if_order')->default(false);
            $table->boolean('if_open_overdue')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('courses');
    }
}
