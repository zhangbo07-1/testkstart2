<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserManagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_manages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id')->unsigned();
            $table->integer('manage_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('user_manages', function(Blueprint $table) {
            $table->foreign('model_id')->references('id')->on('users')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            $table->foreign('manage_id')->references('id')->on('organizations')
                  ->onDelete('restrict')
                  ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_manages', function(Blueprint $table) {
            $table->dropForeign('user_manages_model_id_foreign');
            $table->dropForeign('user_manages_manage_id_foreign');
        });
        Schema::drop('user_manages');
    }
}
