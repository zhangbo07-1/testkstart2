<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_info', function (Blueprint $table) {
            $table->increments('id');
            $table->string('phone', 255)->default('021-61031632');
            $table->string('email', 255)->default('contact@elearning99.com');
            $table->string('company', 255)->default('KNOWSURFACE');
            $table->integer('censor')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_info');
    }
}
