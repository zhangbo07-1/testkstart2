<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_results', function (Blueprint $table) {
            $table->increments('id');
            $table->float('score')->default(0);
            $table->integer('overed')->default(0);
            $table->integer('user_id')->unsigned();
            $table->integer('distribution_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('exam_results', function(Blueprint $table) {
            $table->foreign('distribution_id')->references('id')->on('exam_distributions')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_results', function(Blueprint $table) {
            $table->dropForeign('exam_results_user_id_foreign');
            $table->dropForeign('exam_results_distribution_id_foreign');

        });
        Schema::drop('exam_results');
    }
}
