<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->integer('course_id')->unsigned();
            $table->integer('survey_id')->unsigned();
            $table->timestamps();
        });
         Schema::table('survey_records', function(Blueprint $table) {
            $table->foreign('survey_id')->references('id')->on('surveys')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_records', function(Blueprint $table) {
            $table->dropForeign('survey_records_survey_id_foreign');

        });
        Schema::drop('survey_records');
    }
}
