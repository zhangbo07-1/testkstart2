<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('exam_id');
            $table->string('title');
            $table->text('description');
            $table->integer('pass_score')->default(0);
            $table->integer('exam_time')->default(0);
            $table->integer('limit_times')->default(0);
            $table->integer('catalog_id')->nullable();
            $table->integer('user_id');
            $table->boolean('if_link')->default(true);
            $table->boolean('emailed')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('exams');
    }
}
