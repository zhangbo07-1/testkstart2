<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('question_id')->unsigned();
            $table->integer('result_id')->unsigned();
            $table->text('answer')->nullable();
            $table->float('score')->default(0);
            $table->integer('if_true')->default(0);
            $table->timestamps();
        });
        Schema::table('question_results', function(Blueprint $table) {
            $table->foreign('question_id')->references('id')->on('questions')
                ->onDelete('cascade')
                ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('question_results', function(Blueprint $table) {
            $table->dropForeign('question_results_question_id_foreign');

        });
        Schema::drop('question_results');
    }
}
