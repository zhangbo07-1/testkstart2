<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('distribution_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->integer('process')->default(0);
            $table->integer('max_score')->default(0);
            $table->timestamps();
        });
        Schema::table('exam_records', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('distribution_id')->references('id')->on('exam_distributions')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_records', function(Blueprint $table) {
            $table->dropForeign('exam_records_user_id_foreign');
            $table->dropForeign('exam_records_distribution_id_foreign');
        });
        Schema::drop('exam_records');
    }
}
