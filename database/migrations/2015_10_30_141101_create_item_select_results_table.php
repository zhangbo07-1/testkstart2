<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemSelectResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_select_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('select_id')->unsigned();
            $table->integer('result_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('item_select_results', function(Blueprint $table) {
            $table->foreign('select_id')->references('id')->on('item_selects')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('result_id')->references('id')->on('survey_results')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_select_results', function(Blueprint $table) {
            $table->dropForeign('item_select_results_select_id_foreign');
            $table->dropForeign('item_select_results_result_id_foreign');

        });
        Schema::drop('item_select_results');
    }
}
