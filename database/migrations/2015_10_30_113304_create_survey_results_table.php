<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSurveyResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('record_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('survey_results', function(Blueprint $table) {
            $table->foreign('record_id')->references('id')->on('survey_records')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('survey_results', function(Blueprint $table) {
            $table->dropForeign('survey_results_user_id_foreign');
            $table->dropForeign('survey_results_record_id_foreign');

        });
        Schema::drop('survey_results');
    }
}
