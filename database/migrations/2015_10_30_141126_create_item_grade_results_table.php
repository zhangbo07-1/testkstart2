<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemGradeResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_grade_results', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('grade_id')->unsigned();
            $table->integer('result_id')->unsigned();
            $table->integer('choose')->unsigned();
            $table->timestamps();
        });
        Schema::table('item_grade_results', function(Blueprint $table) {
            $table->foreign('grade_id')->references('id')->on('item_grades')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('result_id')->references('id')->on('survey_results')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_grade_results', function(Blueprint $table) {
            $table->dropForeign('item_grade_results_grade_id_foreign');
            $table->dropForeign('item_grade_results_result_id_foreign');

        });
        Schema::drop('item_grade_results');
    }
}
