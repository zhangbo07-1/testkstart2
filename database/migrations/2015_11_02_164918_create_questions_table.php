<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->text('title');
            $table->integer('bank_id')->unsigned();
            /*
             *type = 1 单选题
             *type = 2 多选题
             *type = 3 是非题
             *type = 4 简答题
             */
            $table->integer('type');
            $table->integer('priority');
            $table->timestamps();
        });
        Schema::table('questions', function(Blueprint $table) {
            $table->foreign('bank_id')->references('id')->on('question_banks')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('questions', function(Blueprint $table) {
            $table->dropForeign('questions_bank_id_foreign');

        });
        Schema::drop('questions');
    }
}
