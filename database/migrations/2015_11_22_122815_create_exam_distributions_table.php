<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamDistributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_distributions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('model_id')->unsigned();
            $table->string('model_type');
            $table->integer('exam_id')->unsigned();
            $table->timestamps();
        });
        Schema::table('exam_distributions', function(Blueprint $table) {
            $table->foreign('exam_id')->references('id')->on('exams')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_distributions', function(Blueprint $table) {
            $table->dropForeign('exam_distributions_exam_id_foreign');
        });
        Schema::drop('exam_distributions');
    }
}
