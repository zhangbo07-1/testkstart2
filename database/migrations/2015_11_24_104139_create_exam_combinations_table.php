<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamCombinationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_combinations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bank_id')->unsigned();
            $table->integer('exam_id')->unsigned();
            $table->integer('numbers')->default(2);
            $table->integer('weight');
            $table->timestamps();
        });
        Schema::table('exam_combinations', function(Blueprint $table) {
            $table->foreign('bank_id')->references('id')->on('question_banks')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->foreign('exam_id')->references('id')->on('exams')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('exam_combinations', function(Blueprint $table) {
            $table->dropForeign('exam_combinations_bank_id_foreign');
            $table->dropForeign('exam_combinations_exam_id_foreign');
        });
        Schema::drop('exam_combinations');
    }
}
