<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. A "local" driver, as well as a variety of cloud
    | based drivers are available for your choosing. Just store away!
    |
    | Supported: "local", "ftp", "s3", "rackspace"
    |
    */

    'default' => env('FILE_SYSTEM','aliyun'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => 's3',

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root'   => storage_path('app'),
        ],

        'ftp' => [
            'driver'   => 'ftp',
            'host'     => 'ftp.example.com',
            'username' => 'your-username',
            'password' => 'your-password',

            // Optional FTP Settings...
            // 'port'     => 21,
            // 'root'     => '',
            // 'passive'  => true,
            // 'ssl'      => true,
            // 'timeout'  => 30,
        ],

        's3' => [
            'driver' => 's3',
            'key'    =>  env('S3_KEY','AKIAIUKUP6LHHI5HAAHA'),
            'secret' => env('S3_SECRET','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'),
            'region' => env('S3_REGION','us-west-2'),
            'bucket' => env('S3_BUCKET','elearningtest'),
        ],

        'rackspace' => [
            'driver'    => 'rackspace',
            'username'  => 'your-username',
            'key'       => 'your-key',
            'container' => 'your-container',
            'endpoint'  => 'https://identity.api.rackspacecloud.com/v2.0/',
            'region'    => 'IAD',
            'url_type'  => 'publicURL',
        ],
        'aliyun' => [
            'driver'    => 'aliyun',
            'access_id' => env('OSS_ACCESS_ID','KLFL3uL31nkVr4Df'),
            'access_key'=> env('OSS_ACCESS_KEY','pTFd89FFbz7Z6JhGhKHyNrvcFfgBdl'),
            'end_point' => env('OSS_END_POINT','oss-cn-shanghai.aliyuncs.com'),
            'bucket_name' => env('OSS_BUCKET_TEST','elearningtest'),
        ],

    ],

];
