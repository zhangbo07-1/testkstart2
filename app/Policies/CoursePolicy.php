<?php
namespace App\Policies;
use App\Models\User;
use App\Models\Course;
use App\Repositories\UserRepository;
class CoursePolicy extends BasePolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        Course $course,
        UserRepository $user_gestion

    )
    {
        $this->model = $course;
        $this->user_gestion = $user_gestion;
    }

    public function delete(User $user,$data)
    {
        if(($user->role_id > 2) && (parent::delete($user,$data)))
            return true;
        else
            return false;
    }
    public function modify(User $user,$data)
    {
        if(($user->role_id > 2) && (parent::modify($user,$data)))
            return true;
        else
            return false;
    }

    public function maintain(User $user,$data)
    {
        if($user->role_id > 2)
        {
            return $this->modify($user,$data);
        }
        if($user->id == $data->user_id)
        {
            return true;
        }
        elseif($user->role_id == 2 )
        {
            foreach($data->maintains as $maintain)
            {
                if($maintain->maintain_id = $user->id)
                    return true;
            }
        }
        return false;
    }


}

