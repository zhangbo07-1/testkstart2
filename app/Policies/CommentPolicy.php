<?php

namespace App\Policies;
use App\Models\User;
use App\Models\Comment;
use App\Repositories\UserRepository;
class CommentPolicy  extends BasePolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        Comment $comment,
        UserRepository $user_gestion
    )
    {
        $this->model = $comment;
        $this->user_gestion = $user_gestion;
    }

     public function modify(User $user, $data)
    {
        if($user->id === $data->user_id || $user->id === $data->post->user_id)
        {
            return true;
        }
        foreach($data->user->manages as $manage)
        {
            $data_id[] = $manage->manage_id;
        }
        foreach($user->manages as $manage)
        {
            $user_id[] = $manage->manage_id;
        }
        if(($user->role_id > 2) && ($this->user_gestion->ownerOrNot($user_id,$data_id)))
        {
            return true;
        }
        return false;
    }

    public function delete(User $user, $data)
    {
        return $this->modify($user,$data);
    }


}
