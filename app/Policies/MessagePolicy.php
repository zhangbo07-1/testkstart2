<?php
namespace App\Policies;
use App\Models\User;
use App\Models\Message;
use App\Repositories\UserRepository;
class MessagePolicy extends BasePolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        Message $message,
        UserRepository $user_gestion

    )
    {
        $this->model = $message;
        $this->user_gestion = $user_gestion;
    }

    public function delete(User $user,$data)
    {
        if(($user->role_id > 2) && (parent::delete($user,$data)))
            return true;
        else
            return false;
    }
    public function modify(User $user,$data)
    {
        if(($user->role_id > 2) && (parent::modify($user,$data)))
            return true;
        else
            return false;
    }


}

