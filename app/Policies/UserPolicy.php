<?php

namespace App\Policies;
use App\Models\User;
use App\Repositories\UserRepository;
class UserPolicy extends BasePolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        User $user,
        UserRepository $user_gestion
    )
    {
        $this->model = $user;
        $this->user_gestion = $user_gestion;
    }



    public function manage_report(User $user)
    {
        if($user->role_id > 2)
            return true;
        else
            return false;
    }
    public function manage_lesson(User $user)
    {
        if($user->role_id > 1)
            return true;
        else
            return false;
    }


    public function manage_evaluation(User $user)
    {
        if($user->role_id > 1)
            return true;
        else
            return false;
    }
    public function manage_bank(User $user)
    {
        if($user->role_id > 1)
            return true;
        else
            return false;
    }
    public function manage_exam(User $user)
    {
        if($user->role_id >1)
            return true;
        else
            return false;
    }
    public function manage_course(User $user)
    {
        if($user->role_id > 1)
            return true;
        else
            return false;
    }

    public function manage_user(User $user)
    {
        if($user->role_id > 2)
            return true;
        else
            return false;
    }

    public function import_users(User $user)
    {
        if($user->role_id > 3)
            return true;
        else
            return false;
    }

    public function import_organization(User $user)
    {
        if($user->role_id > 3)
            return true;
        else
            return false;
    }

    public function manage_system(User $user)
    {
        if($user->role_id > 3)
            return true;
        else
            return false;
    }

    public function manage_emailTemplet(User $user)
    {
        if($user->role_id > 3)
            return true;
        else
            return false;
    }

    public function manage_logs(User $user)
    {
        if($user->role_id > 3)
            return true;
        else
            return false;
    }

    public function manage_info(User $user)
    {
        if($user->role_id > 3)
            return true;
        else
            return false;
    }
    public function manage_catalog(User $user)
    {
        if($user->role_id > 3)
            return true;
        else
            return false;
    }


     public function student(User $user)
    {
        if($user->role_id == 1)
        {
            return true;
        }
        else
            return false;
    }

    public function delete(User $user,$data)
    {
        if(($user->role_id > 2) && (parent::delete($user,$data)))
        {
            return true;
        }
        else
            return false;
    }
    public function modify(User $user,$data)
    {
        if(($user->id == $data->id) || ($user->role_id > 2) && (parent::modify($user,$data)))
        {
            return true;
        }
        else
            return false;
    }

    public function create_post(User $user)
    {
        return true;
    }
    public function create_comment(User $user)
    {
        return true;
    }
    public function create_resource(User $user)
    {
        if($user->role_id > 1)
            return true;
        else
            return false;
    }
    public function create_message(User $user)
    {
        if($user->role_id > 1)
            return true;
        else
            return false;
    }





    public function manage_survey(User $user)
    {
        if($user->role_id > 3)
            return true;
        else
            return false;
    }


}
