<?php
namespace App\Policies;
use App\Models\User;
use App\Models\QuestionBank;
use App\Repositories\UserRepository;
class QuestionBankPolicy extends BasePolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        QuestionBank $bank,
        UserRepository $user_gestion

    )
    {
        $this->model = $bank;
        $this->user_gestion = $user_gestion;
    }



}

