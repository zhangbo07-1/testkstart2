<?php
namespace App\Policies;
use App\Models\User;
class BasePolicy
{
    protected $model;
    protected $user_gestion;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    /*
    public function __construct(
        UserRepository $user_gestion
    )
    {
        $this->user_gestion = $user_gestion;
    }
    */

    /*
      修改
     */
    public function modify(User $user, $data)
    {
        if($user->id === $data->user_id)
        {
            return true;
        }
        foreach($data->manages as $manage)
        {
            $data_id[] = $manage->manage_id;
        }
        foreach($user->manages as $manage)
        {
            $user_id[] = $manage->manage_id;
        }
        if(($user->role_id > 1) && ($this->user_gestion->ownerOrNot($user_id,$data_id)))
        {
            return true;
        }
        return false;
    }

    /*
      查看，使用，下载等
     */
    public function apply(User $user,$data)
    {
        if($user->id === $data->user_id)
        {
            return true;
        }
        foreach($data->manages as $manage)
        {
            $data_id[] = $manage->manage_id;
        }
        foreach($user->manages as $manage)
        {
            $user_id[] = $manage->manage_id;
        }
        if(($this->user_gestion->belongOrNot($user_id,$data_id))||($this->user_gestion->belongOrNot($user_id,$data_id)))
        {
            return true;
        }
        return false;
    }

    /*
      维护
     */
    public function maintain(User $user,$data)
    {
        // $list = explode(',',$data->manage_id);

        if(($this->modify($user,$data))||(in_array($user->id,$list)))
        {
            return true;
        }
        return false;
    }

    /*
      删除
     */
    public function delete(User $user,$data)
    {
        return $this->modify($user,$data);
    }
}
