<?php
namespace App\Policies;
use App\Models\User;
use App\Models\Survey;
use App\Repositories\UserRepository;
class SurveyPolicy extends BasePolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        Survey $survey,
        UserRepository $user_gestion

    )
    {
        $this->model = $survey;
        $this->user_gestion = $user_gestion;
    }


    public function modifySurvey(User $user,$data)
    {
        if(($user->role_id > 3) && (parent::modify($user,$data)))
            return true;
        else
            return false;
    }
     public function deleteSurvey(User $user,$data)
    {
        if(($user->role_id > 3) && (parent::delete($user,$data)))
            return true;
        else
            return false;
    }
    public function modifyEvaluation(User $user,$data)
    {
        if(($user->role_id > 1) && (parent::modify($user,$data)))
            return true;
        else
            return false;
    }
    public function deleteEvaluation(User $user,$data)
    {
        if(($user->role_id > 1) && (parent::delete($user,$data)))
            return true;
        else
            return false;
    }


}

