<?php
namespace App\Policies;
use App\Models\User;
use App\Models\Resource;
use App\Repositories\UserRepository;
class ResourcePolicy extends BasePolicy
{
    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct(
        Resource $resource,
        UserRepository $user_gestion

    )
    {
        $this->model = $resource;
        $this->user_gestion = $user_gestion;
    }


    public function delete(User $user,$data)
    {
        if(($user->role_id > 3) && (parent::delete($user,$data)))
            return true;
        else
            return false;
    }
}

