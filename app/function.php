<?php

function get_info()
{
    $info =DB::table('system_info')->first();
        return "Phone:{$info->phone} | {$info->email}";
}

function set_language()
{
    App::setLocale('cn');
}

function get_url()
{
    return "http://".env('OSS_BUCKET_TEST').".".env('OSS_END_POINT');
}
function errors_for($attribute,$errors)
{
    if($attribute == 'success')
    {
        return $errors->first($attribute, '<div class="alert alert-success alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>:message</div>');
    }
    else
        return $errors->first($attribute, '<div class="alert alert-warning alert-dismissible fade in" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>:message</div>');
    //   return $errors->first($attribute, '<div class="dm3-alert dm3-alert-error">:message</div>');
}

function errors_for_tag($attribute,$errors)
{
    return $errors->first($attribute, 'has-error has-feedback');
}

