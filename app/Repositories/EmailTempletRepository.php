<?php namespace App\Repositories;

use App\Models\EmailTemplet;
use App\Repositories\UserRepository;
use Storage;
class EmailTempletRepository extends BaseRepository
{

protected $user_gestion;
	/**
	 * Create a new EmailTempletRepository instance.
	 *
	 * @param  App\Models\Emailtemplet $emailtemplet
	 * @param  App\Models\Tag $tag
	 * @param  App\Models\Comment $comment
	 * @return void
	 */
	public function __construct(
		Emailtemplet $emailtemplet,
        UserRepository $user_gestion
    )
	{
		$this->model = $emailtemplet;
        $this->user_gestion = $user_gestion;
	}

    
    public function index($type)
    {
        switch($type)
        {
        case 1:
            $email = $this->model->where('type',1)->first();
            break;
        case 2:
            $email = $this->model->where('type',2)->first();
            break;
        case 3:
            $email = $this->model->where('type',3)->first();
            break;
        case 4:
            $email = $this->model->where('type',4)->first();
            break;
        case 5:
            $email = $this->model->where('type',5)->first();
            break;
        case 6:
            $email = $this->model->where('type',6)->first();
            break;
        case 7:
            $email = $this->model->where('type',7)->first();
            break;
        case 8:
            $email = $this->model->where('type',8)->first();
            break;
        case 9:
            $email = $this->model->where('type',9)->first();
            break;
        default:
            $email = $this->model->where('type',1)->first();
            break;
                    
        }
        if(!$email)
        {
            $email = new $this->model;
            $email->type =  $type;
            $email->save;
        }
        return $email;
        return view('email-manage/index',['email'=>$email,'type'=>$type]);
    }
    
	/**
	 * Create or update a emailtemplet.
	 *
	 * @param  App\Models\Emailtemplet $emailtemplet
	 * @param  array  $inputs
	 * @param  bool   $user_id
	 * @return App\Models\Emailtemplet
	 */
  	private function saveEmailtemplet($emailtemplet, $inputs)
	{
		$emailtemplet->title = $inputs['title'];
		$emailtemplet->body = $inputs['body'];
		$emailtemplet->save();
		return $emailtemplet;
	}

	/**
	 * Create a emailtemplet.
	 *
	 * @param  array  $inputs
	 * @param  int    $user_id
	 * @return void
	 */
	public function store($inputs)
	{
		$emailtemplet =$this->getById($inputs['type_id']);
		$emailtemplet = $this->saveEmailtemplet($emailtemplet, $inputs);
		return $emailtemplet;
	}

}
