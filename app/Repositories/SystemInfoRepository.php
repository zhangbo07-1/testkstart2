<?php namespace App\Repositories;

use App\Models\SystemInfo;
use Storage;
class SystemInfoRepository extends BaseRepository
{
    protected $destinationPath = '/img/';
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
        SystemInfo $systemInfo
    )
    {
        $this->model = $systemInfo;
    }

    public function saveLogo($inputs)
    {
        $res = "更改成功";
        if(!array_key_exists('image',$inputs))
        {
            $res = "请选择要修改png文件";
            return $res;
        }
        $extension = $inputs['image']->getClientOriginalExtension();
        if($extension != 'png')
        {
            $res =  "请上传png文件";
            return $res;

        }
        $imageInfo = getimagesize($inputs['image']);
        if($imageInfo[0]!=300 || $imageInfo[1]!= 100)
        {
            $res = "请上传300*100的png文件";
            return $res;
        }
        $res =  Storage::put(
            $this->destinationPath.'logo.png',
            file_get_contents($inputs['image'])
        );
        
        if($res)
        {
            $res = "更新成功！";
            return $res;
        }
        $res = "更新失败！";
        return $res;
    }

    public function info()
    {
        $info = $this->model->first();
        return $info;
    }
    public function saveInfo($inputs)
    {
        $info = $this->model->first();
        $info->company = $inputs['company'];
        $info->phone = $inputs['phone'];
        $info->email = $inputs['email'];
        $info->save();
        return $info;
    }

}
