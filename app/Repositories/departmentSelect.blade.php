@extends('layout.base')
@section('css')
    {!! Html::style('assets/jstree/dist/themes/default/style.min.css') !!}
    @append
    @section('js')

        {!! Html::script('assets/jstree/dist/jstree.js') !!}
        {!! Html::script('assets/js/department.js') !!}
        {!! Html::script('assets/js/views/view.operation.js') !!}
        @append
        @section('content')


                 <div class="col-md-6 col-md-offset-3">
                     <form class="form-inline" action="#">
                    <div class="form-group">
                        <input type="text" name="findByUsername" placeholder="{{ trans('table.input_select_department') }}">
                    </div>
                    <button type="submit" class="dm3-btn dm3-btn-medium button-large"><i class="glyphicon glyphicon-search"></i>&nbsp;{{ trans('button.search') }}</button>
                    </form>
                 </div>


            <div class="row" style="margin-right:0">
                <div class="col-md-6 col-md-offset-3">

                    <div style="overflow-x: auto; overflow-y: auto; height: 100%; width:100%;">
                        <div id="jstree_department_div"></div>
                    </div>
                </div>
            </div>
            <br>
            <br>
            <div align="center">
                <a class="dm3-btn dm3-btn-medium dm3-btn button-large" onClick="getSelect()">{{ trans('button.submit') }}</a>
                <a class="dm3-btn dm3-btn-medium dm3-btn-red button-large" href="javascript:window.close();">{{ trans('button.cancel') }}</a>
            </div>

        @endsection
