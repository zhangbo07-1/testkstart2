<?php namespace App\Repositories;

use App\Models\Survey;
use App\Models\Course;
use App\Models\SurveyManage;
use App\Models\SurveyRecord;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
class SurveyRepository extends BaseRepository
{
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
	Survey $survey,
        SurveyManage $survey_manage,
        UserRepository $user_gestion,
        OrganizationRepository $organization_gestion
    )
    {
	$this->model = $survey;
        $this->manage_model = $survey_manage;
        $this->user_gestion = $user_gestion;
        $this->organization_gestion = $organization_gestion;
    }
    
    public function index($n,$type,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $surveyname = isset($inputs['findBySurveyname'])?$inputs['findBySurveyname']:"";
        $list = $this->getModifyModelList();
        $surveys = $this->model->whereIn('id',$list)->whereType($type)->where('title','LIKE','%' . $surveyname . '%')->orderBy($orderby,$direction)->paginate($n);
        return $surveys;
    }

    public function lst($n,$type,$inputs="",$orderby = 'created_at', $direction = 'desc')
    {
        $surveyname = isset($inputs['findBySurveyname'])?$inputs['findBySurveyname']:"";
        $list = $this->getApplyModelList();
        $surveys = $this->model->whereIn('id',$list)->whereType($type)->wherePushed(1)->where('title','LIKE','%' . $surveyname . '%')->orderBy($orderby,$direction)->paginate($n);
        return $surveys;
    }

    public function getDistributeList($n,$inputs,$type = 2,$orderby = 'created_at', $direction = 'desc')
    {
        $surveyname = isset($inputs['search_word'])?$inputs['search_word']:"";
        $course_id = isset($inputs['course_id'])?$inputs['course_id']:0;
        $distributed = SurveyRecord::where('course_id',$course_id)->lists('survey_id')->all();
        $surveys = $this->model->with('items')->whereNotIn('id',$distributed)->whereType($type)->wherePushed(1)->where('title','LIKE','%' . $surveyname . '%')->orderBy($orderby,$direction)->paginate($n);
        return $surveys;
    }
    
    /**
     * Create or update a post.
     *
     * @param  App\Models\Post $post
     * @param  array  $inputs
     * @param  bool   $user_id
     * @return App\Models\Post
     */
    private function saveSurvey($survey, $inputs, $user_id = null)
    {
        $res = array('status'=>1,'error'=>'','id'=>'');
	$survey->title = $inputs['title'];
	if($user_id)
        {
            $survey->user_id = $user_id;
        }
        $survey->save();
        $this->setManageId($survey,$inputs['manage_id'],$user_id);
        $res['id'] = $survey->id;
	return $res;
    }

    /**
     * Update a post.
     *
     * @param  array  $inputs
     * @param  int    $id
     * @return void
     */
    public function update($inputs, $id)
    {
	$survey = $this->getById($id);
	$res = $this->saveSurvey($survey, $inputs);
	return $res;
    }

    /**
     * Create a survey.
     *
     * @param  array  $inputs
     * @param  int    $user_id
     * @return void
     */
    public function store($inputs,$type, $user_id)
    {
	$survey = new $this->model;
        $survey->type = $type;
	$res = $this->saveSurvey($survey, $inputs, $user_id);
        if($type == 1)
        {
            $this->distribute($survey->id);
        }
	return $res;
    }

    public function push($id)
    {
        $survey = $this->getById($id);
        $survey->pushed = 1;
        $survey->save();
        return $survey;
    }

    public function distribute($id,$course_id=null)
    {
        $record = new SurveyRecord;
        $survey = $this->getById($id);
        if($course_id)
        {
            $old_records = SurveyRecord::where('course_id',$course_id)->get();
            foreach($old_records as $old_record)
            {
                error_log($old_record->id);
                $old_record->delete();
            }
            
            $course = Course::find($course_id);
            
            $record->title = $course->title."_".$survey->title;
            $record->course_id = $course_id;
        }
        else
        {
            $record->title = $survey->title;
            $record->course_id = 0;
        }
        $record->survey_id = $id;
        $record->save();
        return $record;
    }

    public function getRecordBySurveyId($survey_id,$course_id=null)
    {
        if($course_id)
            {
                $record = SurveyRecord::where('course_id',$course_id)->where('survey_id',$survey_id)->first();
            }
        else
        {
            $record = SurveyRecord::where('survey_id',$survey_id)->first();
            }
        return $record;
    }


}
