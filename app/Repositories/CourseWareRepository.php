<?php namespace App\Repositories;

use App\Models\CourseWare;
use App\Repositories\GuidRepository;
use App\Repositories\UserRepository;
use App\Repositories\CourseRepository;
use Storage,Auth;
class CourseWareRepository extends BaseRepository
{
    protected $ZipPath = '/resourcefile/coursezip/';
    protected $WarePath = 'resourcefile/courses/';
    protected $guid_gestion;
    protected $course_gestion;
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
        CourseWare $ware,
        GuidRepository $guid_gestion,
        UserRepository $user_gestion,
        CourseRepository $course_gestion
    )
    {
        $this->model = $ware;
        $this->guid_gestion = $guid_gestion;
        $this->user_gestion = $user_gestion;
        $this->course_gestion = $course_gestion;
    }

    
    public function index($n,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $wares = $this->model->where('course_id',$id)->orderBy($orderby,$direction)->paginate($n);
        return $wares;
    }



    private function saveWare($ware,$inputs,$user_id = null)
    {
        $res = array('status'=>1,'error'=>'');
        if($user_id)
        {
            $ware->user_id = $user_id;
        }
        
        if(array_key_exists('file',$inputs))
        {
            $filename = $inputs['file']->getClientOriginalName();
            $extension = $inputs['file']->getClientOriginalExtension();
            $ware->filename = $filename;
            $ware->path = $this->guid_gestion->guid();
            $ware->storagename = $ware->path.'.'.$extension;
            
            Storage::disk('local')->put($this->ZipPath.$ware->course_id.'/'.$ware->storagename, file_get_contents($inputs['file']));
            $file = $this->WarePath . $ware->course_id;
            if(file_exists($file))
            {
                $this->deldir($file);
            }
            $zip = new \ZipArchiveEx;
            if ( $zip->open( storage_path()."/app".$this->ZipPath.$ware->course_id.'/'.$ware->storagename) === TRUE )
            {
                $zip->extractTo($this->WarePath.$ware->course_id.'/'.$ware->path);
            }
            else
            {
                $res['status'] = 0;
                $res['error'] = array('file'=>trans('error.00023'));
                return $res;
            }
            $result =  Storage::put(
                $this->ZipPath.$ware->course_id.'/'.$ware->storagename,
                file_get_contents($inputs['file'])
            );
            if($result)
            {
                if($ware->save())
                {
                    return $res;
                }
            }
            else
            {
                $res['status'] = 0;
                $res['error'] = array('file'=>trans('error.00019'));
                return $res;
            }
        }
        $res['status'] = 0;
        $res['error'] = array('file'=>trans('error.00021'));
        return $res;
    }


    public function useWare($id)
    {
        $ware = $this->getById($id);
        $coursePath = $this->WarePath.$ware->course_id.'/'.$ware->path;
        if(!file_exists($coursePath))
        {
            $file = Storage::get($this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename);
            Storage::disk('local')->put($this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename, $file);
            $file = $this->WarePath . $ware->course_id;
            if(file_exists($file))
            {
                $this->deldir($file);
            }
            $zip = new \ZipArchiveEx;
            if ( $zip->open(storage_path()."/app".$this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename) === TRUE )
            {
                $zip->extractTo($coursePath);
            }
            else
            {
                $this->destroy($ware->id);
                return false;
            }
        }
        return true;
    }

    

    public function store($course_id,$inputs,$user_id)
    {
        $ware = new $this->model;
        $ware->course_id = $course_id;
        $res = $this->saveWare($ware,$inputs,$user_id);
        if($res['status'])
        {
            $course = $this->course_gestion->getById($course_id);
            $course->ware_id = $ware->id;
            $course->save();
        }
        return $res;
    }

    public function destroy($id)
    {
	$ware = $this->getById($id);
        Storage::delete($this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename);
        $ware->delete();
    }

    public function downLoad($id)
    {
        $ware = $this->getById($id);
        $zipPath = storage_path()."/app".$this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename;
        if(!file_exists($zipPath))
        {
            $file = Storage::get($this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename);
            Storage::disk('local')->put($this->ZipPath.'/'.$ware->course_id.'/'.$ware->storagename, $file);
        }
        $url = $zipPath;
        error_log($url);
        return $url;
    }


    public function cleanFiles($id)
    {
        $basepath = storage_path()."/app";
        $file = $basepath.$this->ZipPath . $id;
        if(file_exists($file))
        {
            $this->deldir($file);
        }
        $file = $this->WarePath . $id;
        if(file_exists($file))
        {
            $this->deldir($file);
        }
    }

    public function deldir($dir)
    {
        //先删除目录下的文件：
        $dh=opendir($dir);
        while ($file=readdir($dh)) {
            if($file!="." && $file!="..")
            {
                $fullpath=$dir."/".$file;
                if(!is_dir($fullpath))
                {
                    unlink($fullpath);
                }
                else
                {
                    $this->deldir($fullpath);
                }
            }
        }
        closedir($dh);
        if(rmdir($dir))
        {
            return true;
        }
        else
        {
            return false;
        }
    }



    
    public function startWare($id)
    {
        $res = array('status'=>1,'data'=>'no');
        $course = $this->course_gestion->getById($id);
        $ware = $this->getById($course->ware_id);
        $user_id = Auth::user()->id;
        
        $suspend_data = '';

        $cmi_location = '';
        $cmi_mode = '';
        $completion_status = '';
        $this->useWare($course->ware_id);
        $filepath = $this->WarePath . $id . '/'.$ware->path.'/';
        $infofile = $filepath.'imsmanifest.xml';
        

        if(file_exists($infofile))
        {
            $xml = simplexml_load_file($infofile);
            $resources = $xml->resources->resource;
            $file = '';
            foreach($resources as $resource)
            {
                if(isset($resource['href']))
                {
                    $file = $resource['href'];
                }
            }
            if($file)
            {
                
                $firstfile = $filepath . $file;
                if ( file_exists($firstfile) )
                {
                    $filename =$firstfile;
                    $res['data'] = array(
                                    'course_id' => $id,
                                    'user_id'   => $user_id,
                                    'filename'  => $filename,
                                    'suspend_data'  => $suspend_data,
                                    'cmi_location'  => $cmi_location,
                                    'completion_status'=>$completion_status,
                                    'cmi_mode'     => $cmi_mode
                    );
                    return $res;
                }
            }
        }
        else
        {
            $res['status'] = 0;
            $res['data'] =  "找不到课件;Can't find course;";
        }
        return $res;
    }


}
