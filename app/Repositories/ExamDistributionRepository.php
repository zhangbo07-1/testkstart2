<?php namespace App\Repositories;

use App\Models\QuestionBank;
use App\Models\User;
use App\Models\Exam;
use App\Models\Course;
use App\Models\ExamDistribution;
use App\Repositories\UserRepository;
use App\Repositories\ExamRepository;
use App\Repositories\OrganizationRepository;

use Input,Auth;

class ExamdistributionRepository extends BaseRepository
{


    public function __construct(
        ExamDistribution $distribution,
        ExamRepository $exam_gestion,
        UserRepository $user_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->model = $distribution;
        $this->exam_gestion = $exam_gestion;
        $this->user_gestion = $user_gestion;
        $this->organization_gestion = $organization_gestion;
    }

     public function getAssignList($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
         $examname = isset($inputs['search_word'])?$inputs['search_word']:"";
         $course = Course::find($inputs['course_id']);
         $findList = $this->exam_gestion->findExamIdByInput( $examname);
         $distributed = '';
         if($course->examDistribution)
         {
             $distributed = $course->examDistribution->exam_id;
         }
         $exams = Exam::whereIn('id',$findList)->where('id','<>',$distributed)->where('if_link',1)->orderBy($orderby,$direction)->paginate($n);
        return $exams;
    }

    public function assignCourse($course_id,$id)
    {
        $course = Course::find($course_id);
        if($course->distribution)
        {
            $course->distribution->delete();
        }
        $distribute = new $this->model;        
        $distribute->exam_id = $id;
        $distribute->model_id = $course_id;
        $distribute->model_type = Course::class;
        $distribute->save();
        
        return $distribute->exam;
    }





       public function getUnDistributeUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getUnDistributeUserList($inputs,$id);
        $unDistributeUsers = User::with('profile')->with('department')->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $unDistributeUsers;
    }

    public function getUnDistributeUserList($inputs,$id)
    {
        $keyword = isset($inputs['search_word'])?$inputs['search_word']:"";
        $exam = Exam::find($id);
        $userList = $this->getApplyUserList($exam);
        $list = $this->user_gestion->getUserIdByInput($keyword);
        $choseIdArray = array();
        foreach($exam->distributions as $distribution)
        {
            $choseIdArray[] = $distribution->model_id;
        }
        $unDistributeUsers = User::whereIn('id',$list)->whereIn('id',$userList)->whereNotIn('id',$choseIdArray)->where('status_id',2)->lists('id')->all();
        return $unDistributeUsers;

    }




    public function getDistributeUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        
        $list = $this->getDistributeUserList($inputs,$id);
        $DistributeUsers = User::with('profile')->with('department')->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $DistributeUsers;
    }

    public function getDistributeUserList($inputs,$id)
    {
        $keyword = isset($inputs['search_word'])?$inputs['search_word']:"";
       $exam = Exam::find($id);
        $list = $this->user_gestion->getUserIdByInput($keyword);
        $choseIdArray = array();
       foreach($exam->distributions as $distribution)
        {
            $choseIdArray[] = $distribution->model_id;
        }
        $DistributeUsers = User::whereIn('id',$list)->whereIn('id',$choseIdArray)->where('status_id',2)->lists('id')->all();
        return $DistributeUsers;

    }




    
    public function adddistributions($inputs,$id)
    {

        error_log("in add");
        $res = array('result'=>true,'message'=>trans('record.distribute_success'));
        if($inputs['is_all'] === 'true')
        {
             $list = $this->getUnDistributeUserList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
        }

        foreach($list as $user_id)
        {
            $distribute = new $this->model;
            $distribute->exam_id = $id;
            $distribute->model_id = $user_id;
            $distribute->model_type = User::class;
            $distribute->save();
        }
        return $res;
    }

    public function deleteDistributions($inputs,$id)
    {
        error_log("in delete");
        $res = array('result'=>true,'message'=>trans('record.undistribute_success'));
        error_log($inputs['is_all']);
        if($inputs['is_all'] === 'true')
        {
             $records = $this->model->where('exam_id',$id)->get();
        }
        else
        {
            $list = $inputs['user_id'];
            $records = $this->model->where('exam_id',$id)->whereIn('model_id',$list)->get();
        }

        foreach($records as $record)
        {
            $record->delete();
        }
        return $res;
    }


}
