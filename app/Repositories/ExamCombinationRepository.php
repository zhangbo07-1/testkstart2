<?php namespace App\Repositories;

use App\Models\ExamCombination;
use App\Models\QuestionBank;
use App\Repositories\BankRepository;
use App\Repositories\ExamRepository;

class ExamCombinationRepository extends BaseRepository
{

    /**
     * The Role instance.
     *
     * @var App\Models\ExamRecord
     */
    protected $bank_gestion;
    protected $exam_gestion;

    public function __construct(
        ExamCombination $combination,
        ExamRepository $exam_gestion,
        BankRepository $bank_gestion )
    {
        $this->model = $combination;
        $this->exam_gestion = $exam_gestion;
        $this->bank_gestion = $bank_gestion;
    }

    public function getUNSelect($id)
    {
        $choseIdArray = $this->model->where('exam_id',$id)->lists('bank_id')->all();
        $questionBanks = QuestionBank::whereNotIn('id',$choseIdArray)->get();
        return $questionBanks;
    }

    public function saveCombination($inputs,$id)
    {
        $res = array('status'=>1,'error'=>'','id'=>'');
        $banks = $this->bank_gestion->getAll();
        $exam = $this->exam_gestion->getById($id);
        foreach($exam->combinations as $combination)
        {
            $combination->delete();
        }

        foreach($banks as $questionBank)
        {
            $Qnumber = $inputs['bankSelect'.$questionBank->id];
            error_log($Qnumber);
            if($Qnumber > 0)
            {
                if(!$combinate = $this->model->where('exam_id',$id)->where('bank_id',$questionBank->id)->first())
                {
                    $combinate = new $this->model;
                    $combinate->exam_id = $id;
                    $combinate->bank_id = $questionBank->id;
                    $combinate->weight = $inputs['bankWeight'.$questionBank->id];
                    $combinate->numbers = $Qnumber;
                }
                else
                {
                    $combinate = $this->model->where('exam_id',$id)->where('bank_id',$questionBank->id)->first();
                    $combinate->weight += $inputs['bankWeight'.$questionBank->id];
                    $combinate->numbers += $Qnumber;

                }
                if($combinate->numbers > count($questionBank->questions))
                {
                    $res['status'] = 1;
                    $res['error']=array('error'=>'题库《'.$questionBank->title.'》中数目不足');
                    return $res;
                }
                if($combinate->weight > 100)
                {
                    $res['status'] = 1;
                    $res['error']=array('error'=>'题库《'.$questionBank->title.'》比重超过100%');
                    return $res;
                }

                $combinate->save();
                return $res;
            }
        }

    }

}
