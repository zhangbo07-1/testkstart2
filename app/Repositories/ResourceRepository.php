<?php namespace App\Repositories;

use App\Models\Resource;
use App\Models\ResourceManage;
use App\Repositories\GuidRepository;
use App\Repositories\UserRepository;
use Storage;
class ResourceRepository extends BaseRepository
{
    protected $destinationPath = '/resourcefile/download/';
    protected $guid_gestion;
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
        Resource $resource,
        ResourceManage $resource_manage,
        GuidRepository $guid_gestion,
        UserRepository $user_gestion,
        OrganizationRepository $organization_gestion
    )
    {
	$this->model = $resource;
        $this->manage_model = $resource_manage;
        $this->guid_gestion = $guid_gestion;
        $this->user_gestion = $user_gestion;
        $this->organization_gestion = $organization_gestion;
    }

    
    public function index($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getModifyModelList();
         if(!empty($inputs['catalog_id']))
        {
            $catalogArray = explode(',',$inputs['catalog_id']);

            $list = $this->model->whereIn('id',$list)
                         ->whereIn('catalog_id',$catalogArray)
                         ->lists('id')->all();
        }
        $resources = $this->model->whereIn('id',$list)->orderBy($orderby,$direction)->paginate($n);
        return $resources;
    }


    
     public function lst($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getApplyModelList();
         if(!empty($inputs['catalog_id']))
        {
            $catalogArray = explode(',',$inputs['catalog_id']);

            $list = $this->model->whereIn('id',$list)
                         ->whereIn('catalog_id',$catalogArray)
                         ->lists('id')->all();
        }
        $resources = $this->model->whereIn('id',$list)->orderBy($orderby,$direction)->paginate($n);
        return $resources;
    }

    

    private function saveResource($resource,$inputs,$user_id = null)
    {
        $res = array('status'=>1,'error'=>'');
        $resource->name = $inputs['name'];
        if($user_id)
        {
            $resource->user_id = $user_id;
        }
        
        if(array_key_exists('file',$inputs))
        {
            $filename = $inputs['file']->getClientOriginalName();
            $extension = $inputs['file']->getClientOriginalExtension();
            $resource->filename = $filename;
            $resource->type = 1;
            $resource->storagename = $this->guid_gestion->guid().'.'.$extension;
            $resource->size = $inputs['file']->getSize();
            if(!isset($inputs['catalog_id']))
            {
                $res['status'] = 0;
                $res['error'] = array('catalog_id'=>trans('error.00022'));
                return $res;
            }
            $resource->catalog_id = $inputs['catalog_id'];

            $result =  Storage::put(
                $this->destinationPath.$resource->storagename,
                file_get_contents($inputs['file'])
            );

            if($result)
            {
                if($resource->save())
                {
                    $this->setManageId($resource,$inputs['manage_id'],$user_id);
                    return $res;
                }
            }
            else
            {
                $res['status'] = 0;
                $res['error'] = array('file'=>trans('error.00019'));
                return $res;
            }
        }
        else
        {
            if(isset($inputs['url']))
            {
                $resource->filename = "网络文件";
                $resource->type = 2;
                $resource->storagename = $inputs['url'];
                if(!isset($inputs['catalog_id']))
                {
                    $res['status'] = 0;
                    $res['error'] = array('catalog_id'=>trans('error.00022'));
                    return $res;
                }
                $resource->catalog_id = $inputs['catalog_id'];
                $resource->save();
                $this->setManageId($resource,$inputs['manage_id'],$user_id);
                return $res;
            }
            $res['status'] = 0;
            $res['error'] = array('file'=>trans('error.00020'));
            return $res;
        }
       
        $res['status'] = 0;
        $res['error'] = array('file'=>trans('error.00021'));
        return $res;
    }

    

    public function store($inputs,$user_id)
    {
        $resource = new $this->model;
        $res = $this->saveResource($resource,$inputs,$user_id);
        return $res;
    }

    public function destroy($id)
    {
	$resource = $this->getById($id);
        Storage::delete($this->destinationPath.$resource->storagename);
        $resource->delete();
    }

    public function downLoad($id)
    {
        $resource = $this->getById($id);
        
        $file = Storage::get($this->destinationPath.$resource->storagename);
        Storage::disk('local')->put($resource->storagename, $file);
        $url = storage_path()."/app/".$resource->storagename;
        error_log($url);
        return $url;
    }


}
