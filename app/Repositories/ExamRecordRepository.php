<?php namespace App\Repositories;

use App\Models\User;
use App\Models\ExamRecord;

use Input;

class ExamRecordRepository extends BaseRepository
{

    /**
     * The Role instance.
     *
     * @var App\Models\ExamRecord
     */
    protected $examRecord;

    public function __construct(
        User $user,
        ExamRecord $examRecord )
    {
        $this->model = $user;
        $this->examRecord = $examRecord;
    }

    public function add($usersNotCheckedIdArray, $exam_id){

        if (!empty($usersNotCheckedIdArray)) {
            foreach ( $usersNotCheckedIdArray as &$user_id ) {
                $examRecordExsit = ExamRecord::where('user_id', (int)$user_id)->where('exam_id',(int)$exam_id)->first();
                if ( $examRecordExsit === null  ) {
                    $examRecord = new ExamRecord;
                    $examRecord->user_id = (int)$user_id;
                    $examRecord->exam_id = (int)$exam_id;
                    $examRecord->save();
                }
            }
            return true;
        }else{
            return false;
        }
    }

    public function delete($usersCheckedIdArray, $exam_id){
        if (!empty($usersCheckedIdArray)) {
            foreach ( $usersCheckedIdArray as &$user_id ) {
                ExamRecord::where('user_id',$user_id)->where('exam_id',$exam_id)->delete();
            }
            return true;
        }else{
            return false;
        }
    }

     public function findRecordIdByCatalogId( $id )
    {
        switch ( true )
        {
            case (empty($id)):
            case ($id == '0'):
                $records = ExamRecord::all()->lists('id');

                break;
            case ($id != '0'):

                $records = ExamRecord::with('exam')->WhereHas('exam', function ( $query ) use ( $id ) {
                    $query->where('courseCatalog_id',$id );
                })->lists('id');

                break;
            default:
                $records = ExamRecord::all()->lists('id');
                break;
        }
        return $records;
    }
}