<?php namespace App\Repositories;
use App\Models\User;
use App\Models\Organization;
use App\Repositories\ImportLogRepository;
use Excel,Auth;
class OrganizationRepository extends BaseRepository
{

    protected $log_gestion;

    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
        Organization $organization,
        ImportLogRepository $log_gestion
    )
    {
        $this->model = $organization;
        $this->log_gestion = $log_gestion;
    }
    public function FetchRepeatMemberInArray($array)
    {
        // 获取去掉重复数据的数组
        $unique_arr = array_unique ( $array );
        // 获取重复数据的数组
        $repeat_arr = array_diff_assoc ( $array, $unique_arr );
        return $repeat_arr;
    }
    public function getUserNUM($id)
    {
        $node = Organization::find($id);
        $num = count($node->user);
        $nodes = Organization::where('parent_id',$id)->get();
        foreach($nodes as $child)
        {
            $num += $this->getUserNUM($child->id);
        }
        return $num;
    }
    /**
     * list a node.
     *
     * @param  array  $inputs
     * @param  int    $id
     * @return void
     */
    public function lst($id,$user_id, $manages,$NUM)
    {
        $res = array();
        $list = array();
        $user = User::find($user_id);
        foreach($user->manages as $manage)
        {
            $manageId[] = $manage->id;
            $list = array_merge($list,$this->getUnderArray($manage->manage_id));
            $list = array_merge($list,$this-> getUperArray($manage->manage_id));
        }
        $list = array_unique($list);
        
        if($manages =='/')
        {
            $manageList = $manageId;
        }
        else
        {
            $manageNames = explode(',',$manages);
            
            foreach($manageNames as $manageName)
            {
                $department = $this->model->where('name',$manageName)->first();
                if($department)
                {
                    $manageList[] = $department->id;
                }
            }
            $selectNodes = $this->getUperArray($manageList);
        }
        $nodes = Organization::where('parent_id',$id)->get();
        foreach($nodes as $node)
        {
            if(in_array($node->id,$list))
            {
                if(count(Organization::where('parent_id',$node->id)->get()))
                {
                    $children = true;
                }
                else
                {
                    $children = false;
                }
                if (in_array($node->id, $manageList))
                {
                    $checked = true;
                    error_log("selected ".$node->id);
                }
                else
                {
                    $checked = false;
                }
                $opened = false;
                if(isset($selectNodes))
                {
                    if(in_array($node->id, $selectNodes))
                    {
                        $opened = true;
                    }
                }
                if($NUM)
                {
                    $userNum = '('.$this->getUserNUM($node->id).')';
                }
                else
                {
                    $userNum = "";
                }
                $res[] = array('id'=>$node->id,'text' =>$node->name.$userNum, 'children' =>$children,'state'=>['opened'=>$opened,'selected'=>$checked]);
                
            }
        }
        
        return $res;
    }



    /**
     * ceate a new node.
     *
     * @param  int $id
     * @param  string $name
     * @return array
     */
    public function create($id, $name)
    {
        $node = new Organization;
        $node->parent_id = $id;
        $node->name = $name;
        $node->save();
        return array('id' => $node->id,'text' => $node->name);
    }

    /**
     * rename a node.
     *
     * @param  int $id
     * @param  string $name
     * @return array
     */
    public function rename($id, $name)
    {
        $node = Organization::find($id);
        $node->name = $name;
        $node->save();
        return array('id' => $node->id,'text' => $node->name);
    }

    /**
     * search a node.
     *
     * @param  int $id
     * @param  string $name
     * @return array
     */
    public function search($keyword)
    {
        
        $nodes = Organization::where('name','LIKE','%'.$keyword.'%')->lists('id')->all();
        $selectNodes = $this->getUperArray($nodes);
        foreach($selectNodes as $node)
        {
             $res[] = array('id'=>$node,'state'=>['opened'=>true]);
        }
        return $res;
    }

    /**
     * remove a node.
     *
     * @param  int $id
     * @return array
     */
    public function remove($id)
    {
        if($id == 1)
        {
            return array('status'=>'False','error'=>trans('error.00004'));
        }
        if($this->getUserNUM($id))
        {
            return array('status'=>'False','error'=>trans('error.00005'));
        }
        $node = Organization::find($id);
        $nodes = Organization::where('parent_id',$id)->get();
        foreach($nodes as $child)
        {
            $this->remove($child->id);
        }
        $node->delete();
        return array('status' => 'OK');
    }

    /**
     * import a arganization list.
     *
     * @param  xlsfile $file
     * @return array
     */
    public function import($file)
    {
        $result = array('type'=>0);
        $log = $this->log_gestion->createLog($file,2);
        $this->log_gestion->addLog($log,"文件上传成功");
        $progs = Excel::load($file)->sheet(0)->toArray();
        $tag = $progs[0];
        unset($progs[0]);
        foreach($progs as $prog)
        {
            if(!empty($prog[array_search('操作',$tag)]))
            {
                $organization[] = $prog[array_search('组织名称',$tag)];
                $parent[] = $prog[array_search('上级组织名称',$tag)];
                $data[] = array('organization'=> $prog[array_search('组织名称',$tag)],
                                'parent'=> $prog[array_search('上级组织名称',$tag)],
                                'act'=>$prog[array_search('操作',$tag)],
                );
            }
        }
        if((empty($organization)) || (empty($parent)))
        {
            $result['type'] = 1;
            $error = trans('error.00008');
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->FetchRepeatMemberInArray($organization);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = trans('error.00013').implode(",",$error);
            $this->log_gestion->addLog($log,$error);
        }
        
        if($result['type'])
        {
            $result['error'] =  trans('error.00012');
            return $result;
        }
        foreach($data as $info)
        {
            switch($info['act'])
            {
            case 'A':
                if(!empty($info['parent']))
                {
                    $parent = $this->model->where('name',$info['parent'])->first();
                    if($parent)
                    {
                        $node = $this->model->whereName($info['organization'])->first();
                        if(!$node)
                        {
                            $this->create($parent->id,$info['organization']);
                            $error = trans('error.00015').$info['organization'];
                            $this->log_gestion->addLog($log,$error);
                        }
                    }
                    else
                    {
                         $error = trans('error.00014').$info['organization'];
                         $this->log_gestion->addLog($log,$error);
                    }
                }
                else
                {
                    $this->create(0,$info['organization']);
                    $error = trans('error.00015').$info['organization'];
                    $this->log_gestion->addLog($log,$error);
                }
                break;
            case 'D':
                $node = $this->model->where('name',$info['organization'])->first();
                if($node)
                {
                    $this->remove($node->id);
                    $error = trans('error.00016').$info['organization'];
                    $this->log_gestion->addLog($log,$error);
                }
                break;    
                
            }
        }
    }


    /*
       获取下属列表
     */
    public function getUnderList($id)
    {
        $node = Organization::find($id);
        $list = $node->id;
        $nodes = Organization::where('parent_id',$id)->get();
        foreach($nodes as $child)
        {
            $list = $list.",".$this->getUnderList($child->id);
        }
        return $list;
    }

    /*
       获取下属列表
     */
    public function getUnderArray($ids)
    {
        $list = "";
        if(is_array($ids))
        {
            foreach($ids as $id)
            {
                $list = $list.','.$this->getUnderList($id);
            }
        }
        else
        {
            $list = $this->getUnderList($ids);
        }
        $list = explode(',',$list);
        $list = array_unique($list);
        return $list;
    }
    /*
       获取上方元素
     */
    public function getUperList($id)
    {
        $node = Organization::find($id);
        $list = $id;
        if($node)
        {
            $list = $list.','.$this->getUperList($node->parent_id);
        }
        return $list;
    }

    /*
       获取上方列表
     */
    public function getUperArray($ids)
    {
        $list = "";
        if(is_array($ids))
        {
            foreach($ids as $id)
            {
                $list = $list.','.$this->getUperList($id);
            }
        }
        else
        {
            $list = $this->getUperList($ids);
        }
        $list = explode(',',$list);
        $list = array_unique($list);
        return array_reverse($list);
    }


    
    /*
       a是否包含b
     */
    public function ownerOrNot($Aid,$Bid)
    {
        if($Aid === $Bid)
            return true;
        else
        {
            $node = $this->getById($Bid);
            if($node)
                return $this->ownerOrNot($Aid,$node->parent_id);
            else
                return false;
        }
    }

    public function checkManage($list)
    {
        $ids = explode(',',$list);
        $baselist=array();
        foreach(Auth::user()->manages as $manage)
        {
            $baselist = array_merge($baselist,$this->getUnderArray($manage->manage_id));
        }
        $baselist = array_unique($baselist);
        $result = array_diff($ids,$baselist);
        if(empty($result))
            return true;
        else
            return false;

    }

    public function setManage($list)
    {
        $ids = explode(',',$list);
        foreach(Auth::user()->manages as $manage)
        {
            foreach($ids as $id)
            {
                if($this->ownerOrNot($manage->manage_id,$id))
                {
                    $manageId[] = $id;
                }
                elseif($this->ownerOrNot($id,$manage->manage_id))
                {
                    $manageId[] = $manage->manage_id;
                }
            }
        }
        $manageId = array_unique($manageId);
        return $manageId;
    }


    public function getAllSelect()
    {
        $list = array();
        foreach(Auth::user()->manages as $manage)
        {
            $list = array_merge($list,$this->getUnderArray($manage->manage_id));
        }
        $list = array_unique($list);
        $userDepartmentSelects =  $this->model->whereIn('id',$list)->lists('name','id');
	return compact('userDepartmentSelects');
    }

    public function exampleExcel()
    {
        $prog[] = array('组织名称','上级组织名称','操作');
        $prog[] = array('','','A 增加');
        $prog[] = array('','','D 删除');
        return $prog;
    }


    
}
