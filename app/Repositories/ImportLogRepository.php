<?php namespace App\Repositories;

use App\Models\ImportLog;
use App\Repositories\GuidRepository;
use Auth,Storage;
class ImportLogRepository  extends BaseRepository
{
    protected $destinationPath = '/resourcefile/logs/';

	/**
	 * The Role instance.
	 *
	 * @var App\Models\Role
	 */
    protected $guid_gestion;

	/**
	 * Create a new RolegRepository instance.
	 *
	 * @param  App\Models\UserStatus $userStatus
	 * @return void
	 */
	public function __construct(
        ImportLog $importLog,
        GuidRepository $guid_gestion
    )
	{
		$this->model = $importLog;
        $this->guid_gestion = $guid_gestion;
	}

    public function createLog($file,$type)
    {
        $log = new $this->model;
        $log->type = $type;
        $log->user_id = Auth::user()->id;
        $filename = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $log->filename = $filename;
        $log->storagename = $this->guid_gestion->guid().'.'.$extension;
        $result =  Storage::put(
                $this->destinationPath.$log->storagename,
                file_get_contents($file)
                );
        $log->save();
        return $log->id;
    }

    public function addLog($id,$error)
    {
        error_log($error);
        $log = $this->getById($id);
        $log->error = $log->error.'<br>'.$error;
        $log->save();
        return $log->id;
    }


    public function index($n,$type,$orderby = 'created_at', $direction = 'desc')
    {
        $import = $this->model->whereType($type)->orderBy($orderby,$direction)->paginate($n);
        return $import;
    }


    public function destroy($id)
    {
        $import = $this->getById($id);
        Storage::delete($this->destinationPath.$import->storagename);
        $import->delete();
    }

    public function downLoad($id)
    {
        $import = $this->getById($id);
        $file = Storage::get($this->destinationPath.$import->storagename);
        Storage::disk('local')->put($import->storagename, $file);
        $url = storage_path()."/app/".$import->storagename;
        return $url;
    }

}