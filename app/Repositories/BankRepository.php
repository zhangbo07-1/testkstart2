<?php namespace App\Repositories;

use App\Models\QuestionBank;
use App\Models\QuestionBankManage;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
class BankRepository extends BaseRepository
{
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
        QuestionBank $bank,
        QuestionBankManage $bank_manage,
        UserRepository $user_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->model = $bank;
        $this->manage_model = $bank_manage;
        $this->user_gestion = $user_gestion;
        $this->organization_gestion = $organization_gestion;
    }
    
    public function index($n,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getModifyModelList();
        $banks = $this->model->whereIn('id',$list)->orderBy($orderby,$direction)->paginate($n);
        return $banks;
    }

    /**
     * Create or update a post.
     *
     * @param  App\Models\Post $post
     * @param  array  $inputs
     * @param  bool   $user_id
     * @return App\Models\Post
     */
    private function saveBank($bank, $inputs, $user_id = null)
    {
        $res = array('status'=>1,'error'=>'','id'=>'');
	$bank->title = $inputs['title'];
	if($user_id)
        {
            $bank->user_id = $user_id;
        }
	$bank->save();
        $this->setManageId($bank,$inputs['manage_id'],$user_id);
        $res['id'] = $bank->id;
	return $res;
    }

    /**
     * Update a post.
     *
     * @param  array  $inputs
     * @param  int    $id
     * @return void
     */
    public function update($inputs, $id)
    {
	$bank = $this->getById($id);
	$res = $this->saveBank($bank, $inputs);
	return $res;
    }

    /**
     * Create a bank.
     *
     * @param  array  $inputs
     * @param  int    $user_id
     * @return void
     */
    public function store($inputs,$user_id)
    {
	$bank = new $this->model;
	$res = $this->saveBank($bank, $inputs, $user_id);
	return $res;
    }

}
