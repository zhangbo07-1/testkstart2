<?php namespace App\Repositories;

use App\Models\QuestionBank;
use App\Models\User;
use App\Models\Exam;
use App\Models\CourseRecord;
use App\Models\ExamResult;
use App\Models\ExamManage;
use App\Models\ExamDistribution;
use App\Models\ExamMaintain;
use App\Models\Question;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;

use Input,Auth;

class ExamRepository extends BaseRepository
{


    public function __construct(
        Exam $exam,
        ExamManage $exam_manage,
        ExamMaintain $exam_maintain,
        UserRepository $user_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->model = $exam;
        $this->manage_model = $exam_manage;
        $this->maintain_model = $exam_maintain;
        $this->user_gestion = $user_gestion;
        $this->organization_gestion = $organization_gestion;
    }

    public function index($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getModifyModelList();
        if(Auth::user()->role_id == 2)
        {
            $user = Auth::user();
            $list = $this->model->whereIn('id',$list)->where('user_id',$user->id)
                  ->orWhereHas('maintains',function($q) use ($user)
                                        {
                                            $q->where('maintain_id',$user->id);
                                        })
                  ->lists('id')->all();
        }
        if(!empty($inputs['catalog_id']))
        {
            $catalogArray = explode(',',$inputs['catalog_id']);
            
            $list = $this->model->whereIn('id',$list)
                         ->whereIn('catalog_id',$catalogArray)
                         ->lists('id')->all();
        }
        $examname = isset($inputs['findByExamname'])?$inputs['findByExamname']:"";
        $exams = $this->model->whereIn('id',$list)
                      ->where('title', 'LIKE', '%' . $examname . '%')
                      ->orderBy($orderby,$direction)->paginate($n);
        return $exams;
        
    }



    
    private function saveExam($exam, $inputs, $user_id = null)
    {
        $res = array('status'=>1,'error'=>'','id'=>'');
        $exam->exam_id = $inputs['exam_id'];
        $exam->title = $inputs['title'];
        $exam->description = $inputs['description'];
        if($user_id)
        {
            $exam->user_id = $user_id;
        }
        $exam->exam_time = $inputs['exam_time'];
        $exam->limit_times = $inputs['limit_times'];
        $exam->pass_score = $inputs['pass_score'];
        $exam->catalog_id = $inputs['catalog_id'];
        $exam->emailed = isset($inputs['emailed']);
        $exam->if_link = isset($inputs['if_link']);
        $exam->save();
        $this->setManageId($exam,$inputs['manage_id'],$user_id);

        $res['id'] = $exam->exam_id;
        return $res;
    }



    public function store($inputs, $user_id)
    {
        $exam = new $this->model;
        $exam = $this->saveExam($exam, $inputs, $user_id);
        return $exam;
    }

    public function update($inputs, $id)
    {
        $exam = $this->getById($id);
        $exam = $this->saveExam($exam, $inputs);
        return $exam;
    }


    public function findExamIdByInput( $input_data )
    {
         $list = $this->getModifyModelList();
         return Exam::whereIn('id',$list)->where('exam_id', 'LIKE', '%' . $input_data . '%')
            ->orWhere('title', 'LIKE', '%' . $input_data . '%')
            ->orWhere('description', 'LIKE', '%' . $input_data . '%')
            ->lists('id');
    }

    public function filterChosenCombination( $id )
    {
        return ExamCombination::where('exam_id', $id)->get();
    }

    public function filterNotChosenCombination( $id )
    {
        $chosenIdArray = ExamCombination::where('exam_id', $id)->lists('question_bank_id');
        return QuestionBank::whereNotIn('id', $chosenIdArray)->get();
    }

    public function getExamByExamId($exam_id)
    {
        $exam = $this->model->where('exam_id',$exam_id)->first();
        return $exam;
    }

    public function preview($id)
    {
        $exam = $this->getById($id);
        $num = 0;
        $questions = array();
        foreach($exam->combinations as $combination)
        {
            if($combination->numbers > 0)
            {
                $id = Question::where('bank_id',$combination->bank_id)->lists('id')->toArray();

                if($combination->numbers>1)
                {
                    $key = array_rand($id,$combination->numbers);
                    for($i = 0;$i < $combination->numbers;$i++)
                    {
                        $questions[$num] = Question::findOrFail($id[$key[$i]]);
                        $list[$num] = $id[$key[$i]];
                        $num++;
                    }
                }
                else
                {
                    $key = array_rand($id);
                    $questions[$num] = Question::findOrFail($id[$key]);
                    $list[$num] = $id[$key];
                    $num++;

                }
            }
        }
        return $questions;
    }


    public function answer($course_id,$exam_id)
    {
        $res = array('status'=>0,'message'=>'');
        $exam = $this->getById($exam_id);
        $courseRecord = CourseRecord::where('course_id',$course_id)->where('user_id',Auth::user()->id)->first();
        if($courseRecord->course->if_order == 1)
        {
            if($courseRecord->process < 2)
            {
                $res['status'] = 1;
                $res['message'] = array('error'=>'请按照《课程->考试->评估》的顺序进行学习');
                return $res;
            }
        }
        $distribution = ExamDistribution::where('exam_id',$exam_id)->where('model_id',$course_id)->first();
        $num = 0;
        $resultNUM = ExamResult::where('user_id',Auth::user()->id)->where('distribution_id',$distribution->id)->count();
        if($exam->limit_times == 0 ||  $resultNUM < $exam->limit_times)
        {
            foreach($exam->combinations as $combination)
            {
                $Pid = Question::where('bank_id',$combination->bank_id)->where('priority',1)->lists('id')->toArray();
                $Nid = Question::where('bank_id',$combination->bank_id)->where('priority',0)->lists('id')->toArray();
                $Aid = array_merge($Pid,$Nid);
                if(count($Pid) >= $combination->numbers)
                {
                    $key = array_rand($Pid,$combination->numbers);
                    if(!is_array($key))
                    {
                        $key = explode(",",$key);
                    }
                }
                else
                {
                    $key = array_values($Pid);

                    $key_FL = array_rand($Nid,$combination->numbers-count($Pid));
                    if(!is_array($key_FL))
                    {
                        $key_FL = explode(",",$key_FL);
                    }
                    $key = array_merge($key,$key_FL);
                }
                $score = $combination->weight/$combination->numbers;
                for($i = 0;$i < $combination->numbers;$i++)
                {
                    $questions[$num] = Question::findOrFail($Aid[$key[$i]]);
                    $list[$Aid[$key[$i]]] =$score;
                    $num++;
                }

            }
            if($num == 0)
            {
                $res['status'] = 1;
                $res['message'] = array('error'=>'该考试未分配题目');
                return $res;
            }
            //$list = implode(',',$list);
            $res['questions'] = $questions;
            $res['list'] = json_encode($list);
            return $res;
                    
        }
        else
        {
            $res['status'] = 1;
            $res['message'] = array('error'=>'考试次数以达到上限，无法再次打开，谢谢！');
            return $res;
        }
            
    }
}
