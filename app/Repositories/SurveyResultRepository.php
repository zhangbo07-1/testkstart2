<?php namespace App\Repositories;

use App\Models\SurveyResult;
use App\Models\SurveyRecord;
use App\Models\ItemSelectResult;
use App\Models\ItemGradeResult;
use App\Repositories\SurveyRepository;
use Auth;
class SurveyResultRepository extends BaseRepository
{
    protected $survey_gestion;
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
	SurveyResult $surveyResult,
        SurveyRepository $survey_gestion
    )
    {
        $this->model = $surveyResult;
        $this->survey_gestion = $survey_gestion;
    }
    public function score($record_id,$inputs)
    {
        $surveyResult = new $this->model;
        $surveyResult->record_id = $record_id;
        $surveyResult->user_id = Auth::user()->id;
        $surveyResult->save();
        $record = SurveyRecord::find($record_id); 
        $survey = $record->survey;
        foreach($survey->items as $item)
        {
            if($item->type == 1)
            {
                $results = $inputs[$item->id];
                foreach($results as $result)
                {
                    $selectResult = new ItemSelectResult;
                    $selectResult->select_id = $result;
                    $selectResult->result_id = $surveyResult->id;
                    $selectResult->save();
                }
            }
            else
            {
                $gradeResult = new ItemGradeResult;
                $gradeResult->grade_id = $item->grade->id;
                $gradeResult->result_id = $surveyResult->id;
                $gradeResult->choose = $inputs[$item->id];
                $gradeResult->save();
            }
        }
        return $surveyResult;
       
    }


    public function result($id)
    {
        $survey = $this->survey_gestion->getById($id);
        $result['resultNumber'] = count($survey->records[0]->surveyResults);
        foreach($survey->items as $item)
        {
            if($item->type == 1)
            {
                foreach($item->selects as $select)
                {
                    $result['number'][$item->id][$select->id] = count($select->selectResults);
                }
            }
            else
            {
                $grade = $item->grade;
                for($i = 1;$i<=5;$i++)
                {
                    $result['number'][$item->id][$i] = ItemGradeResult::where('grade_id',$grade->id)->where('choose',$i)->count();
                    $sum[$i] = $result['number'][$item->id][$i]*$i;
                }
                $result['avg'][$item->id] = round(array_sum($sum)/$result['resultNumber'],2);
            }
            
        }
        return $result;
    }

    

}
