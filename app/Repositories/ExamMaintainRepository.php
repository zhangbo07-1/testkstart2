<?php namespace App\Repositories;

use App\Models\Exam;
use App\Models\User;
use App\Models\UserManage;
use App\Models\ExamMaintain;
use App\Repositories\ExamRepository;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
class ExamMaintainRepository extends BaseRepository
{

    protected $exam_gestion;
    protected $user_gestion;

    public function __construct(
        ExamMaintain $maintain,
        UserRepository $user_gestion,
        ExamRepository $exam_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->model = $maintain;
        $this->exam_gestion = $exam_gestion;
        $this->user_gestion = $user_gestion;
        $this->organization_gestion = $organization_gestion;
    }

    public function getUnAsssignedTeacher($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getUnAsssignedTeacherList($inputs,$id);
        $unAssignTeachers = User::with('role')->with('profile')->with('department')->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $unAssignTeachers;
    }

    public function getUnAsssignedTeacherList($inputs,$id)
    {
        $keyword = isset($inputs['search_word'])?$inputs['search_word']:"";
        $exam = Exam::find($id);
        $userList = $this->getApplyUserList($exam);
        $list = $this->user_gestion->getUserIdByInput($keyword);
        $choseIdArray = $this->model->where('model_id',$id)->lists('maintain_id')->all();
        $AssignTeachers = User::whereIn('id',$list)->whereIn('id',$userList)->whereNotIn('id',$choseIdArray)->where('role_id',2)->where('status_id',2)->lists('id')->all();
        return $AssignTeachers;

    }




    public function getAsssignedTeacher($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {

        $list = $this->getAsssignedTeacherList($inputs,$id);
        $AssignTeachers = User::with('role')->with('profile')->with('department')->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $AssignTeachers;
    }

    public function getAsssignedTeacherList($inputs,$id)
    {
        $keyword = isset($inputs['search_word'])?$inputs['search_word']:"";
        $list = $this->user_gestion->getUserIdByInput($keyword);
        $choseIdArray = $this->model->where('model_id',$id)->lists('maintain_id')->all();
        $AssignTeachers = User::whereIn('id',$list)->whereIn('id',$choseIdArray)->where('role_id',2)->where('status_id',2)->lists('id')->all();
        return $AssignTeachers;

    }


    public function addTeachers($inputs,$id)
    {


        $res = array('result'=>true,'message'=>trans('teacher.assign_success'));
        error_log($inputs['is_all']);
        if($inputs['is_all'] === 'true')
        {
            $list = $this->getUnAsssignedTeacherList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
        }

        foreach($list as $user_id)
        {
            $maintain = new $this->model;
            $maintain->model_id = $id;
            $maintain->maintain_id = $user_id;
            $maintain->save();
        }
        return $res;
    }



    public function deleteTeachers($inputs,$id)
    {


        $res = array('result'=>true,'message'=>trans('teacher.unassign_success'));
        error_log($inputs['is_all']);
        if($inputs['is_all'] === 'true')
        {
            $maintains = $this->model->where('model_id',$id)->get();
        }
        else
        {
            $list = $inputs['user_id'];
            $maintains = $this->model->where('model_id',$id)->whereIn('maintain_id',$list)->get();
        }

        foreach($maintains as $maintain)
        {
            $maintain->delete();
        }
        return $res;
    }

}
