<?php namespace App\Repositories;

use App\Models\User;
use App\Models\Role;
use App\Models\Profile;
use App\Models\UserStatus;
use App\Models\UserManage;
use App\Models\Organization;
use App\Models\Language;
use App\Repositories\OrganizationRepository;
use App\Repositories\ImportLogRepository;
use Excel,Auth;

class UserRepository extends BaseRepository
{
    /**
     * The Role instance.
     *
     * @var App\Models\Role
     */
    protected $role;
    protected $log_gestion;
    protected $table = array('');
    /**
     * Create a new UserRepository instance.
     *
     * @param  App\Models\User $user
     * @param  App\Models\Role $role
     * @return void
     */
    public function __construct(
        User $user,
        UserManage $user_manage,
        Role $role,
        OrganizationRepository $organization_gestion,
        ImportLogRepository $log_gestion
    )
    {
        $this->model = $user;
        $this->manage_model = $user_manage;
        $this->role = $role;
        $this->log_gestion = $log_gestion;
        $this->organization_gestion = $organization_gestion;
    }


    public function FetchRepeatMemberInArray($array)
    {
        // 获取去掉重复数据的数组
        $unique_arr = array_unique ( $array );
        // 获取重复数据的数组
        $repeat_arr = array_diff_assoc ( $array, $unique_arr );
        return $repeat_arr;
    }

    public function checkDepartment($departments)
    {
        $error = array();
        foreach($departments as $department)
        {
            $result = Organization::where('name',$department)->first();
            if(!$result)
            {
                $error[] = $department;
            }
        }
        return $error;
    }


    public function checkRole($roles)
    {
        $error = array();
        foreach($roles as $role)
        {
            $result = Role::where('tag',$role)->first();
            if(!$result)
            {
                $error[] = $role;
            }
        }
        return $error;
    }
    public function getUserByName($name)
    {
        $user = $this->model->whereName($name)->first();
        return $user;
    }

    public function index($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        foreach(Auth::user()->manages as $manage)
        {
            $manageList[] = $manage->manage_id;
        }
        $manageArray = $this->organization_gestion->getUnderArray($manageList);
        $list = $this->model->whereIn('department_id',$manageArray)->lists('id')->all();
        if(!empty($inputs['manage_id']))
        {
            $department_ids = explode(',',$inputs['manage_id']);
            $departmentArray = $this->organization_gestion->getUnderArray($department_ids);
            $list = $this->model->whereIn('id',$list)
                         ->whereIn('department_id',$departmentArray)
                         ->lists('id')->all();
        }

        if(isset($inputs['userStatus']))
        {
            if($inputs['userStatus'] != 0)
            {
                $list = $this->model->whereIn('id',$list)
                             ->where('status_id',$inputs['userStatus'])
                             ->lists('id')->all();
            }
        }
        else
        {
            $list = $this->model->whereIn('id',$list)
                         ->where('status_id',2)
                         ->lists('id')->all();
        }
        $username = isset($inputs['findByUsername'])?$inputs['findByUsername']:"";

        $users = $this->model->whereIn('id',$list)
                      ->where(function($query) use ($username)
                          {
                              $query->WhereHas('profile', function ( $q ) use ( $username )
                                  {
                                      $q->where('realname', 'LIKE', '%' . $username . '%');
                                  })
                                    ->orWhereHas('role',function($q) use ($username)
                                        {
                                            $q->where('name','LIKE','%'.$username.'%');
                                        })
                                    ->orWhere('name', 'LIKE', '%' . $username . '%')
                                    ->orWhere('email', 'LIKE', '%' . $username . '%');
                          })
                      ->orderBy($orderby,$direction)->paginate($n);
        return $users;
    }





    public function saveUser($user,$inputs,$user_id=null)
    {
        $res = array('status'=>1,'error'=>'');
        if(isset($inputs['name']))
        {
            $user->name=$inputs['name'];
        }
        if(isset($inputs['userStatus']))
        {
            $user->status_id=$inputs['userStatus'];
        }
        if(isset($inputs['email']))
        {
            $user->email=$inputs['email'];
        }
        if(isset($inputs['password']) && !empty($inputs['password']))
        {
            $user->password=bcrypt($inputs['password']);
        }
        if(isset($inputs['pwd'])&&$user_id)
        {
            $user->password=bcrypt($inputs['pwd']);
        }
        if(isset($inputs['role_id']))
        {
            $user->role_id = $inputs['role_id'];
        }
        elseif(isset($inputs['role']))
        {
            $role = Role::whereTag($inputs['role'])->first();
            $user->role_id = $role->id;
        }

        if(isset($inputs['department_id'])&&!empty($inputs['department_id']))
        {
            $user->department_id= $inputs['department_id'];
        }
        elseif(isset($inputs['department']))
        {
            $department = Organization::whereName($inputs['department'])->first();
            $user->department_id= $department->id;
        }
        else
        {
            $user->department_id= Auth::user()->department_id;
        }
        if(isset($inputs['language_id'])&&!empty($inputs['language_id']))
        {
            $user->language_id= $inputs['language_id'];
        }
        elseif(isset($inputs['language']))
        {
            $language = Language::where('tag','LIKE',$inputs['language'])->first();
            $user->language_id= $language->id;
        }
        $user->save();

        if(isset($inputs['manage_id'])&&!empty($inputs['manage_id'])&&($user->role_id > 1) && ($user->role_id < 4))
        {
            $this->setManageId($user,$inputs['manage_id'],$user_id);
        }
        else
        {
            if($user->role_id == 1 || $user->role_id == 4)
            {
                foreach($user->manages as $manage)
                {
                    $manage->delete();
                }
            }
            if($user_id || $user->role_id == 1)
            {
                $manage = new UserManage;
                $manage->model_id = $user->id;
                $manage->manage_id = $user->department_id;
                $manage->save();
            }
            if($user->role_id == 4)
            {
                $manage = new UserManage;
                $manage->model_id = $user->id;
                $manage->manage_id = 1;
                $manage->save();
            }

        }

        if($user->profile)
        {
            $profile = $user->profile;
        }
        else
        {
            $profile = new Profile;
            $profile->user_id=intval($user->id);
        }
        $profile->realname=isset($inputs['realname'])?$inputs['realname']:"";
        $profile->position= isset($inputs['position'])?$inputs['position']:"";
        $profile->rank= isset($inputs['rank'])?$inputs['rank']:"";
        $profile->save();
        return $res;
    }
    public function create($inputs)
    {
        $user = new User;
        $user->status_id = 2;
        $user_id = Auth::user()->id;
        $res = $this->saveUser($user, $inputs, $user_id);
        return $res;

    }
    public function update($id,$inputs)
    {
        $user = $this->getById($id);
        $res = $this->saveUser($user, $inputs);
        return $res;

    }
    public function changeStatus($id,$status)
    {
        $user = $this->getById($id);
        $user->status_id = $status;
        $user->save();
    }

    public function closeUser($id)
    {
        $status = UserStatus::whereTitle('关闭')->first();
        $this->changeStatus($id,$status->id);
    }

    /**
     * import a user list.
     *
     * @param  xlsfile $file
     * @return array
     */
    public function import($file)
    {
        $result = array('type'=>0);
        $log = $this->log_gestion->createLog($file,1);
        $this->log_gestion->addLog($log,"文件上传成功");
        $progs = Excel::load($file)->sheet(0)->toArray();
        $tag = $progs[0];
        unset($progs[0]);
        foreach($progs as $prog)
        {
            if(!empty($prog[array_search('操作',$tag)]))
            {
                $name[] = $prog[array_search('用户名',$tag)];
                $email[] = $prog[array_search('邮箱',$tag)];
                $department[] = $prog[array_search('组织',$tag)];
                $role[] = $prog[array_search('角色',$tag)];
                $data[] = array('act'=>$prog[array_search('操作',$tag)],
                                'name'=>$prog[array_search('用户名',$tag)],
                                'email'=>$prog[array_search('邮箱',$tag)],
                                'pwd'=>$prog[array_search('密码',$tag)],
                                'realname'=> $prog[array_search('姓名',$tag)],
                                'role'=> $prog[array_search('角色',$tag)],
                                'department'=> $prog[array_search('组织',$tag)],
                                'position'=> $prog[array_search('职位',$tag)],
                                'language'=> $prog[array_search('语言',$tag)],
                                'rank'=> $prog[array_search('职级',$tag)],
                );
            }
        }
        if((empty($name)) || (empty($email)) || (empty($department)))
        {
            $result['type'] = 1;
            $error = trans('error.00008');
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->FetchRepeatMemberInArray($name);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = trans('error.00008').implode(",",$error);
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->FetchRepeatMemberInArray($email);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = trans('error.00009').implode(",",$error);
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->checkDepartment($department);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = trans('error.00010').implode(",",$error);
            $this->log_gestion->addLog($log,$error);
        }
        $error = $this->checkRole($role);
        if(!empty($error))
        {
            $result['type'] = 1;
            $error = trans('error.00018').implode(",",$error);
            $this->log_gestion->addLog($log,$error);
        }
        if($result['type'])
        {
            $result['error'] =  trans('error.00012');
            return $result;
        }
        foreach($data as $info)
        {
            switch($info['act'])
            {
                case 'A':
                $user = $this->model->whereName($info['name'])->first();
                if($user)
                    $this->update($user->id,$info);
                else
                    $this->create($info);
                $error = trans('error.00015').$info['name'];
                $this->log_gestion->addLog($log,$error);
                break;
                case 'D':
                $user = $this->model->whereName($info['name'])->first();
                if($user)
                {
                    $this->destroy($user->id);
                    $error = trans('error.00016').$info['name'];
                    $this->log_gestion->addLog($log,$error);
                }
                break;
                case 'C':
                $user = $this->model->whereName($info['name'])->first();
                if($user)
                    $this->closeUser($user->id);
            }
        }
        return $result;
    }


    public function searchByDepatment($n,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->organization_gestion->getUnderArray($id);
        $users =  $this->model->with('role')->with('profile')->whereIn('department_id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $users;

    }


    public function userExcel($user)
    {
        $prog[] = array();
    }

    public function ownerOrNot($Alist,$Blist)
    {
        foreach($Alist as $Aid)
        {
            foreach($Blist as $Bid)
            {
                if($this->organization_gestion->ownerOrNot($Aid,$Bid))
                {
                    return true;
                }
            }
        }
        return false;

    }

    public function belongOrNot($Alist,$Blist)
    {
        foreach($Alist as $Aid)
        {
            foreach($Blist as $Bid)
            {
                if($this->organization_gestion->ownerOrNot($Bid,$Aid))
                    return true;
            }
        }
        return false;
    }



    public function getUserIdByInput( $input_data )
    {
        foreach(Auth::user()->manages as $manage)
        {
            $manageList[] = $manage->manage_id;
        }
        $manageArray = $this->organization_gestion->getUnderArray($manageList);
        $list = $this->model->whereIn('department_id',$manageArray)->lists('id')->all();
        return User::whereIn('id',$list)->whereHas('profile', function ( $query ) use ( $input_data ) {
            $query->where('realname', 'LIKE', '%' . $input_data . '%');
        })
                                        ->orWhere('name', 'LIKE', '%' . $input_data . '%')
                                        ->orWhere('email', 'LIKE', '%' . $input_data . '%')
                                        ->lists('id')->all();
    }

    public function exampleExcel()
    {
        $prog[] = array('操作','用户名','邮箱','密码','姓名','角色',
                        '组织','职位','审批人','职级','语言','属性1','属性2','属性3');
        $prog[] = array('A代表增加或更新','','','更新将不改变原密码',
                        '','S 学员','组织名称必须对应组织结构中现有组织',
                        '','','','CN 代表中文','','','');
        $prog[] = array('D代表删除','','','',
                        '','I 教师','',
                        '','','','EN 代表英文','','','');
        $prog[] = array('C代表关闭','','','',
                        '','A 普通管理员','',
                        '','','','','','','');
        $prog[] = array('','','','',
                        '','SA 超级管理员','',
                        '','','','','','','');
        return $prog;

    }


}
