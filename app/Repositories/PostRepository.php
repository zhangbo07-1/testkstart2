<?php namespace App\Repositories;

use App\Models\Post;
use App\Repositories\UserRepository;
use Storage;
class PostRepository extends BaseRepository
{

protected $user_gestion;
	/**
	 * Create a new BlogRepository instance.
	 *
	 * @param  App\Models\Post $post
	 * @param  App\Models\Tag $tag
	 * @param  App\Models\Comment $comment
	 * @return void
	 */
	public function __construct(
		Post $post,
        UserRepository $user_gestion
    )
	{
		$this->model = $post;
        $this->user_gestion = $user_gestion;
	}

    
    public function index($n,$orderby = 'created_at', $direction = 'desc')
    {
        $posts = $this->model->orderBy($orderby,$direction)->paginate($n);
        return $posts;
    }
    
	/**
	 * Create or update a post.
	 *
	 * @param  App\Models\Post $post
	 * @param  array  $inputs
	 * @param  bool   $user_id
	 * @return App\Models\Post
	 */
  	private function savePost($post, $inputs, $user_id = null)
	{
		$post->title = $inputs['title'];
		$post->summary = $inputs['summary'];
		$post->body = $inputs['body'];
		if($user_id)
        {
            $post->user_id = $user_id;
            $user = $this->user_gestion->getById($user_id);
            $post->manage_id = $user->manage_id;
        }
        if(array_key_exists('manage_id',$inputs))
        {
            $post->manage_id = $inputs['manage_id'];
        }
		$post->save();
        if(array_key_exists('image',$inputs))
        {
            $res = Storage::put(
                'posts/'.$post->id.'/title.jpg',
                file_get_contents($inputs['image'])
            );
        }
        elseif(!Storage::has('posts/'.$post->id.'/title.jpg'))
        {
            Storage::copy('img/slides/0.jpg','posts/'.$post->id.'/title.jpg');
        }
		return $post;
	}

	/**
	 * Update a post.
	 *
	 * @param  array  $inputs
	 * @param  int    $id
	 * @return void
	 */
	public function update($inputs, $id)
	{
		$post = $this->getById($id);
		$post = $this->savePost($post, $inputs);
		return $post;
	}

	/**
	 * Create a post.
	 *
	 * @param  array  $inputs
	 * @param  int    $user_id
	 * @return void
	 */
	public function store($inputs, $user_id)
	{
		$post = new $this->model;
		$post = $this->savePost($post, $inputs, $user_id);
		return $post;
	}


    public function destroy($id)
    {
        $post = $this->getById($id);
        Storage::delete('posts/'.$post->id.'/title.jpg');
        $post->delete();
        
    }

}
