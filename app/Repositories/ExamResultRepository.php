<?php namespace App\Repositories;

use App\Models\ExamResult;
use App\Models\Question;
use App\Models\QuestionSelect;
use App\Models\QuestionResult;
use App\Models\ExamRecord;
use App\Models\ExamDistribution;
use App\Repositories\ExamRepository;
use Auth;
class ExamResultRepository extends BaseRepository
{
    protected $exam_gestion;
    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
	ExamResult $examResult,
        ExamRepository $exam_gestion
    )
    {
        $this->model = $examResult;
        $this->exam_gestion = $exam_gestion;
    }
    public function score($exam_id,$inputs,$course_id=null)
    {
        $exam = $this->exam_gestion->getById($exam_id);
        $user = Auth::user();
        if($exam->if_link && $course_id)
        {
            $distribution = ExamDistribution::where('exam_id',$exam_id)->where('model_id',$course_id)->first();
        }
        else
        {
            $distribution = ExamDistribution::where('exam_id',$exam_id)->where('model_id',$user->id)->first();
        }
        $record = ExamRecord::where('distribution_id',$distribution->id)->where('user_id',$user->id)->first();
        if(!$record)
        {
            $record = new ExamRecord;
            $record->distribution_id = $distribution->id;
            $record->user_id = $user->id;
        }

        $result = new $this->model;
        $result->user_id = $user->id;
        $result->distribution_id = $distribution->id;
        $result->save();
        $list = $inputs['lists'];
        $list = json_decode($list);
        $totalScore = 0;
        foreach($list as $question_id=>$score)
        {
            $error = 0;
            $question = Question::find($question_id);
            $questionResult = new QuestionResult;
            $questionResult->question_id = $question->id;
            $questionResult->result_id = $result->id;
            if($question->type == 2)
            {
                $true = array();
                foreach($question->selects as $select)
                {
                    if($select->if_true == 1)
                    {
                        $true[] = $select->id;
                    }
                }
                $res = $inputs[$question->id];
                $resA = array_diff($true,$res);
                $resB = array_diff($res,$true);
                if(empty($resA) && empty($resB))
                {
                    $questionResult->if_true = 1;
                    $totalScore += $score;
                }
            }
            elseif($question->type == 4)
            {
                $questionResult->answer = $inputs[$question->id];
                $questionResult->score = $score;
                $result->overed = 2;
            }
            else
            {
                $select = QuestionSelect::find($inputs[$question->id]);
                if($select->if_true == 1)
                {
                    $questionResult->if_true = 1;
                    $totalScore += $score;
                }
            }
            $questionResult->save();
        }
        $result->score = round($totalScore,1);
        if($result->overed == 0)
        {
            $result->overed = 1;
        }
        $result->save();


         $resultNUM = ExamResult::where('user_id',$user->id)->where('distribution_id',$distribution->id)->count();
        
        if($record->max_score < $result->score)
        {
            $record->max_score = $result->score;
        }
        if($record->max_score > $exam->pass_score)
        {
            $record->process = 2;
        }
        if($record->process < 2)
        {
            if($exam->limit_times == 0 ||  $resultNUM < $exam->limit_times)
            {
                $record->process = 1;
            }
            else
            {
                $record->process = 3;
            }
        }

        $record->save();
        return true;
    }

    public function mark($id)
    {
        $exam = $this->exam_gestion->getById($id);
        $list = array();
        foreach($exam->distributions as $distribution)
        {
            $list[] = $distribution->id;
        }
        $results = $this->model->whereIn('distribution_id',$list)->where('overed',2)->lists('id')->all();
        
        $questionResults = QuestionResult::whereIn('result_id',$results)->where('answer','<>','')->get();
        return $questionResults;
    }

    public function getQuestionResult($id)
    {
        $result = QuestionResult::find($id);
        return $result;
    }

    public function scoreQuestion($exam_id,$id,$inputs)
    {
        $exam = $this->exam_gestion->getById($exam_id);
        $result = QuestionResult::find($id);
        if($result->examResult->overed == 2)
        {
            $score = round($result->score/5*$inputs['mark'],1);
            $result->examResult->score += $score;
            $result->examResult->overed = 1;
            $result->examResult->save();
        }
        $record = ExamRecord::where('distribution_id',$result->examResult->distribution_id)->where('user_id',Auth::user()->id)->first();
        if($record->max_score < $result->examResult->score)
        {
           $record->max_score < $result->examResult->score;
        }
        
        if($record->max_score > $exam->pass_score)
        {
            $record->process = 2;
        }
        return true;
    }

}
