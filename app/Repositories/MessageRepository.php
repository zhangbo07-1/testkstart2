<?php namespace App\Repositories;

use App\Models\Message;
use App\Repositories\UserRepository;
use Storage;
class MessageRepository extends BaseRepository
{

protected $user_gestion;
	/**
	 * Create a new MessageRepository instance.
	 *
	 * @param  App\Models\Message $message
	 * @param  App\Models\Tag $tag
	 * @param  App\Models\Comment $comment
	 * @return void
	 */
	public function __construct(
		Message $message,
        UserRepository $user_gestion
    )
	{
		$this->model = $message;
        $this->user_gestion = $user_gestion;
	}

    
    public function index($n,$orderby = 'created_at', $direction = 'desc')
    {
        $messages = $this->model->orderBy($orderby, $direction)->paginate($n);
        return $messages;
    }
    
	/**
	 * Create or update a message.
	 *
	 * @param  App\Models\Message $message
	 * @param  array  $inputs
	 * @param  bool   $user_id
	 * @return App\Models\Message
	 */
  	private function saveMessage($message, $inputs, $user_id = null)
	{
		$message->title = $inputs['title'];
		$message->body = $inputs['body'];
		if($user_id)
        {
            $message->user_id = $user_id;
            $user = $this->user_gestion->getById($user_id);
            $message->manage_id = $user->manage_id;
        }
        if(array_key_exists('manage_id',$inputs))
        {
            $message->manage_id = $inputs['manage_id'];
        }
		$message->save();
		return $message;
	}

	/**
	 * Update a message.
	 *
	 * @param  array  $inputs
	 * @param  int    $id
	 * @return void
	 */
	public function update($inputs, $id)
	{
		$message = $this->getById($id);
		$message = $this->saveMessage($message, $inputs);
		return $message;
	}

	/**
	 * Create a message.
	 *
	 * @param  array  $inputs
	 * @param  int    $user_id
	 * @return void
	 */
	public function store($inputs, $user_id)
	{
		$message = new $this->model;
		$message = $this->saveMessage($message, $inputs, $user_id);
		return $message;
	}

}
