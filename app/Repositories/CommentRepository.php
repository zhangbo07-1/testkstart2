<?php namespace App\Repositories;

use App\Models\Comment;

class CommentRepository extends BaseRepository {

	/**
	 * Create a new CommentRepository instance.
	 *
	 * @param  App\Models\Comment $comment
	 * @return void
	 */
	public function __construct(Comment $comment)
	{
		$this->model = $comment;
	}


	/**
	 * Store a comment.
	 *
	 * @param  array $inputs
	 * @param  int   $user_id
	 * @return void
	 */
 	public function store($inputs, $user_id)
	{
		$comment = new $this->model;	

		$comment->body = $inputs['body'];
		$comment->post_id = $inputs['post_id'];
		$comment->user_id = $user_id;
		$comment->save();
	}


}
