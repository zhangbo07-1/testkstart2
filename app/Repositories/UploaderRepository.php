<?php namespace App\Repositories;

use App\Models\Resource;
use App\Repositories\GuidRepository;
use App\Repositories\UserRepository;
use Storage;
class UploaderRepository extends BaseRepository
{
    private $config = array(
        "savePath" => "/uploads/umeditor/upload/image/" ,             //存储文件夹
        "maxSize" => 1000 ,                   //允许的文件最大尺寸，单位KB
        "allowFiles" => array( "gif" , "png" , "jpg" , "jpeg" , "bmp" )  //允许的文件格式
    );



    private $fileField = "upfile";            //文件域名
    private $file;                 //文件上传对象
    private $oriName;              //原始文件名
    private $fileName;             //新文件名
    private $fullName;             //完整文件名,即从当前配置目录开始的URL
    private $fileSize;             //文件大小
    private $fileType;             //文件类型
    private $stateInfo;            //上传状态信息,
    private $stateMap = array(    //上传状态映射表，国际化用户需考虑此处数据的国际化
        "SUCCESS" ,                //上传成功标记，在UEditor中内不可改变，否则flash判断会出错
                                  "文件大小超出 upload_max_filesize 限制" ,
                                  "文件大小超出 MAX_FILE_SIZE 限制" ,
                                  "文件未被完整上传" ,
                                  "没有文件被上传" ,
                                  "上传文件为空" ,
                                  "POST" => "文件大小超出 post_max_size 限制" ,
                                  "SIZE" => "文件大小超出网站限制" ,
                                  "TYPE" => "不允许的文件类型" ,
                                  "DIR" => "目录创建失败" ,
                                  "IO" => "输入输出错误" ,
                                  "UNKNOWN" => "未知错误" ,
                                  "MOVE" => "文件保存时出错",
                                  "DIR_ERROR" => "创建目录失败"
    );


    public function __construct()
    {
        $this->stateInfo = $this->stateMap[ 0 ];
    }

    /**
     * 获取当前上传成功文件的各项信息
     * @return array
     */
    public function getFileInfo()
    {
        return array(
            "originalName" => $this->oriName ,
            "name" => $this->fileName ,
            "url" => get_url().$this->fullName ,
            "size" => $this->fileSize ,
            "type" => $this->fileType ,
            "state" => $this->stateInfo
            
        );
    }

    /**
     * 上传错误检查
     * @param $errCode
     * @return string
     */
    private function getStateInfo( $errCode )
    {
        return !$this->stateMap[ $errCode ] ? $this->stateMap[ "UNKNOWN" ] : $this->stateMap[ $errCode ];
    }

    /**
     * 重命名文件
     * @return string
     */
    private function getName()
    {
        return $this->fileName = time() . rand( 1 , 10000 ) .'.'. $this->fileType;
    }

    /**
     * 文件类型检测
     * @return bool
     */
    private function checkType()
    {
        return in_array( $this->fileType , $this->config[ "allowFiles" ] );
    }

    /**
     * 文件大小检测
     * @return bool
     */
    private function  checkSize()
    {
        return $this->fileSize <= ( $this->config[ "maxSize" ] * 1024 );
    }

    /**
     * 获取文件扩展名
     * @return string
     */
    private function getFileExt()
    {
        return strtolower( strrchr( $this->file[ "name" ] , '.' ) );
    }

    /**
     * 按照日期自动创建存储文件夹
     * @return string
     */
    private function getFolder()
    {
        $pathStr = $this->config[ "savePath" ];
        if ( strrchr( $pathStr , "/" ) != "/" ) {
            $pathStr .= "/";
        }
        $pathStr .= date( "Ymd" );
        
        return $pathStr;
        
    }

    


    public function upFile($inputs)
    {
        error_log("get in ");

        //处理普通上传
        $file = $this->file = $inputs[ $this->fileField ];

        if ( !$file ) {
            $this->stateInfo = $this->getStateInfo( 'POST' );
            return;
        }



        $this->oriName = $file->getClientOriginalName();
        $this->fileSize = $file->getSize();
        $this->fileType = $file->getClientOriginalExtension();

        error_log($this->oriName. $this->fileSize.$this->fileType);
        
        if ( !$this->checkSize() ) {
            $this->stateInfo = $this->getStateInfo( "SIZE" );
            return;
        }
        if ( !$this->checkType() ) {
            $this->stateInfo = $this->getStateInfo( "TYPE" );
            return;
        }

        $folder = $this->getFolder();
        error_log($folder);
        if ( $folder === false ) {
            $this->stateInfo = $this->getStateInfo( "DIR_ERROR" );
            return;
        }

        $this->fullName = $folder . '/' . $this->getName();
        error_log($this->fullName);
        if ( $this->stateInfo == $this->stateMap[ 0 ] ) {
            if(!Storage::put(
                $this->fullName,
                file_get_contents($file)
            ))
            {
                $this->stateInfo = $this->getStateInfo( "MOVE" );
            }
            
        }
    }


}

