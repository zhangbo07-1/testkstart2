<?php namespace App\Repositories;

use App\Models\User, App\Models\Role;
use App\Models\EmailRecord;
use App\Models\LoginRecord;
class LogRepository extends BaseRepository
{

    protected $login;
    protected $email;

    /**
     * Create a new UserRepository instance.
     *
     * @param  App\Models\User $user
     * @param  App\Models\Role $role
     * @return void
     */
    public function __construct(
        LoginRecord $login,
        EmailRecord $email)
    {
        $this->login = $login;
        $this->email = $email;
    }


    public function findEmailLogIdByInput($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $userName = isset($inputs['findByUsername']) ? $inputs['findByUsername'] : "";
        $emailTitle = isset($inputs['findByEmailtitle']) ? $inputs['findByEmailtitle'] : "" ;
        $logList = $this->email->where('hostname', $_SERVER['HTTP_HOST'])
                        ->where('username','LIKE', '%' . $userName . '%')
                        ->lists('id');


        $logList = $this->email->whereIn('id',$logList)->where('emailTitle','LIKE','%'.$emailTitle.'%')->lists('id');
        $logs = $this->email->whereIn('id',$logList)->orderBy($orderby, $direction)->paginate($n);


        return $logs;
    }
    public function findLoginIdByInput($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $userName = isset($inputs['findByUsername']) ? $inputs['findByUsername'] : "";
        $loginType = isset($inputs['loginType']) ? $inputs['loginType'] : "";

        $logList = $this->login->whereHas('user', function ( $query ) use ( $userName )
            {
                $query->where('name', 'LIKE', '%' . $userName . '%');
            })
                        ->orWhereHas('user', function ( $query ) use ( $userName )
                            {
                                $query->where('email', 'LIKE', '%' . $userName . '%');
                            })
                        ->lists('id');
        if($loginType)
        {
            $logList = $this->login->whereIn('id',$logList)->where('status',$loginType)->lists('id');
        }
        $logs = $this->login->whereIn('id',$logList)->orderBy($orderby, $direction)->paginate($n);

        return $logs;


    }

    public function newLoginRecord($status,$id)
    {
        $loginRecord = new $this->login;
        $loginRecord->user_id = $id;
        $loginRecord->status = $status;
        $loginRecord->save();
    }

}
