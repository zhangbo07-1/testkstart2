<?php namespace App\Repositories;

use App\Models\Course;
use App\Models\CourseManage;
use App\Models\CourseMaintain;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
use Input,Auth;
class CourseRepository extends BaseRepository
{

    /**
     * Create a new UserRepository instance.
     *
     * @param  App\Models\Course $course
     * @param  App\Models\Record $record
     * @return void
     */
    public function __construct(
        Course $course,
        CourseManage $course_manage,
        CourseMaintain $course_maintain,
        UserRepository $user_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->model = $course;
        $this->manage_model = $course_manage;
        $this->maintain_model = $course_maintain;
        $this->user_gestion = $user_gestion;
        $this->organization_gestion = $organization_gestion;
    }




    public function index($n,$inputs,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getModifyModelList();
        if(Auth::user()->role_id == 2)
        {
            $user = Auth::user();
            $list = $this->model->whereIn('id',$list)->where('user_id',$user->id)
                  ->orWhereHas('maintains',function($q) use ($user)
                                        {
                                            $q->where('maintain_id',$user->id);
                                        })
                  ->lists('id')->all();
        }
        if(!empty($inputs['catalog_id']))
        {
            $catalogArray = explode(',',$inputs['catalog_id']);

            $list = $this->model->whereIn('id',$list)
                         ->whereIn('catalog_id',$catalogArray)
                         ->lists('id')->all();
        }
        $keyword = isset($inputs['findByCoursename'])?$inputs['findByCoursename']:"";
        $choseList = $this->getListByInput($keyword);
        $courses = $this->model->whereIn('id',$list)
                      ->whereIn('id', $choseList)
                      ->orderBy($orderby,$direction)->paginate($n);
        return $courses;

    }


    private function saveCourse($course, $inputs, $user_id = null)
    {
        $res = array('status'=>1,'error'=>'','id'=>'');
        $course->course_id = $inputs['course_id'];
        $course->language_id = $inputs['language_id'];
        $course->catalog_id = $inputs['catalog_id'];
        if($user_id)
        {
            $course->user_id = $user_id;
        }
        $course->hours = $inputs['hours'];
        $course->minutes = $inputs['minutes'];
        $course->title = $inputs['title'];
        $course->target = $inputs['target'];
        $course->description = $inputs['description'];
        $course->limitdays = $inputs['limitdays'];
        $course->emailed = isset($inputs['emailed']);
        $course->if_order= isset($inputs['if_order']);
        $course->if_send_new_user = isset($inputs['if_send_new_user']);
        $course->if_open_overdue = isset($inputs['if_open_overdue']);
        $course->save();
        $this->setManageId($course,$inputs['manage_id'],$user_id);

        $res['id'] = $course->course_id;
        return $res;
    }



    public function store($inputs, $user_id)
    {
        $course = new $this->model;
        $course = $this->saveCourse($course, $inputs, $user_id);
        return $course;
    }

    public function update($inputs, $id)
    {
        $course = $this->getById($id);
        $course = $this->saveCourse($course, $inputs);
        return $course;
    }


    public function destroy($id)
    {
        $course = $this->getById($id);

    }





    public function countAllScore($course = null)
    {
        $score = 0;
        if($course)
        {
            $records =  $this->record
                             ->where('course_id',$course->id)->get();

            foreach($records as $r){
                if ( $r->score !== null ) {
                    $score = $score + $r->$score;
                }
            }
        }
        return $score;
    }
    public function getListByInput($input)
    {

        $list = $this->model->where('title', 'LIKE', '%' . $input . '%')
              ->orWhere('course_id', 'LIKE', '%' . $input . '%')
              ->orWhere('description', 'LIKE', '%' . $input . '%')
              ->lists('id')->all();
        return $list;
    }

    public function getCourseByCourseId($course_id)
    {
        $course = $this->model->where('course_id',$course_id)->firstOrFail();
        return $course;
    }






}
