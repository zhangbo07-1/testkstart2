<?php namespace App\Repositories;
use App\Models\User;
use App\Models\Catalog;
use Auth;
class CatalogRepository extends BaseRepository{


    /**
     * Create a new BlogRepository instance.
     *
     * @param  App\Models\Post $post
     * @param  App\Models\Tag $tag
     * @param  App\Models\Comment $comment
     * @return void
     */
    public function __construct(
	Catalog $catalog)
    {
	$this->model = $catalog;
    }


    /**
     * list a node.
     *
     * @param  array  $inputs
     * @param  int    $id
     * @return void
     */
    public function lst($id, $catalogName)
    {
        $res = array();
        $catalogId = 0;
        error_log($catalogName);
        if($catalogName != '/')
        {
            $catalog = $this->model->where('name',$catalogName)->first();
            $catalogId = $catalog->id;
            $selectNodes = $this->getUperArray($catalog);
        }
        $nodes = Catalog::where('parent_id',$id)->get();
        foreach($nodes as $node)
        {
            if(count(Catalog::where('parent_id',$node->id)->get()))
            {
                $children = true;
            }
            else
            {
                $children = false;
            }
            if ($node->id == $catalogId)
            {
                $checked = true;
            }
            else
            {
                $checked = false;
            }
            $opened = false;
            
            if(isset($selectNodes))
            {
                if(in_array($node->id, $selectNodes))
                {
                    $opened = true;
                }
            }
            $res[] = array('id'=>$node->id,'text' =>$node->name, 'children' =>$children,'state'=>['opened'=>$opened,'selected'=>$checked]);
        }
        
        return $res;
    }

    /**
     * ceate a new node.
     *
     * @param  int $id
     * @param  string $name
     * @return array
     */
    public function create($id, $name)
    {
        $node = new Catalog;
        $node->parent_id = $id;
        $node->name = $name;
        $node->save();
        return array('id' => $node->id,'text' => $node->name);
    }

    /**
     * rename a node.
     *
     * @param  int $id
     * @param  string $name
     * @return array
     */
    public function rename($id, $name)
    {
        $node = Catalog::find($id);
        $node->name = $name;
        $node->save();
        return array('id' => $node->id,'text' => $node->name);
    }

    /**
     * remove a node.
     *
     * @param  int $id
     * @return array
     */
    public function remove($id)
    {
        if($id == 1)
        {
            return array('status'=>'False','error'=>'不能删除根目录');
        }
        $node = Catalog::find($id);
        $nodes = Catalog::where('parent_id',$id)->get();
        foreach($nodes as $child)
        {
            $this->remove($child->id);
        }
        $node->delete();
        return array('status' => 'OK');
    }


    /*
       获取下属列表
     */
    public function getUnderList($id)
    {
        $node = Catalog::find($id);
        $list = $node->id;
        $nodes = Catalog::where('parent_id',$id)->get();
        foreach($nodes as $child)
        {
            $list = $list.",".$this->getUnderList($child->id);
        }
        return $list;
    }

    /*
       获取下属列表
     */
    public function getUnderArray($ids)
    {
        $list = "";
        if(is_array($ids))
        {
            foreach($ids as $id)
            {
                $list = $list.','.$this->getUnderList($id);
            }
        }
        else
        {
            $list = $this->getUnderList($ids);
        }
        $list = explode(',',$list);
        $list = array_unique($list);
        return $list;
    }
    /*
       获取上方元素
     */
    public function getUperList($id)
    {
        $node = Catalog::find($id);
        $list = $id;
        if($node)
        {
            $list = $list.','.$this->getUperList($node->parent_id);
        }
        return $list;
    }

    /*
       获取上方列表
     */
    public function getUperArray($ids)
    {
        $list = "";
        if(is_array($ids))
        {
            foreach($ids as $id)
            {
                $list = $list.','.$this->getUperList($id);
            }
        }
        else
        {
            $list = $this->getUperList($ids);
        }
        $list = explode(',',$list);
        $list = array_unique($list);
        return array_reverse($list);
    }


    
    /*
       a是否包含b
     */
    public function ownerOrNot($Aid,$Bid)
    {
        if($Aid === $Bid)
            return true;
        else
        {
            $node = $this->getById($Bid);
            if($node)
                return $this->ownerOrNot($Aid,$node->parent_id);
            else
                return false;
        }
    }

    public function checkManage($list)
    {
        $ids = explode(',',$list);
        $baselist=array();
        foreach(Auth::user()->catalog as $manage)
        {
            $baselist = array_merge($baselist,$this->getUnderArray($manage->manage_id));
        }
        $baselist = array_unique($baselist);
        $result = array_diff($ids,$baselist);
        if(empty($result))
            return true;
        else
            return false;

    }

    public function setManage($list)
    {
        $ids = explode(',',$list);
        foreach(Auth::user()->catalog as $manage)
        {
            foreach($ids as $id)
            {
                if($this->ownerOrNot($manage->manage_id,$id))
                {
                    $manageId[] = $id;
                }
                elseif($this->ownerOrNot($id,$manage->manage_id))
                {
                    $manageId[] = $manage->manage_id;
                }
            }
        }
        $manageId = array_unique($manageId);
        return $manageId;
    }


    public function getAllSelect()
    {
        $list = array();
        foreach(Auth::user()->manages as $manage)
        {
            $list = array_merge($list,$this->getUnderArray($manage->manage_id));
        }
        $list = array_unique($list);
        $userDepartmentSelects =  $this->model->whereIn('id',$list)->lists('name','id');
	return compact('userDepartmentSelects');
    }
}
