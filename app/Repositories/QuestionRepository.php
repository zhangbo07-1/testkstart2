<?php namespace App\Repositories;

use App\Models\Question;
use App\Models\QuestionSelect;

class QuestionRepository extends BaseRepository {

    /**
     * Create a new CommentRepository instance.
     *
     * @param  App\Models\Question $question
     * @return void
     */
    public function __construct(Question $question)
    {
	$this->model = $question;
    }


    /**
     * Save a item.
     *
     * @param  array $inputs

     * @return void
     */
    public function saveQuestion($question, $inputs, $bank_id)
    {
        if(array_key_exists('Qtype', $inputs))
        {
 	    switch( $inputs['Qtype'])
            {
                case 'radio':
                $question->type = 1;
                break;
                case 'multiple':
                $question->type = 2;
                break;
                case 'false':
                $question->type = 3;
                break;
                case 'essay':
                $question->type = 4;
                break;
            }
        }
        $question->title = $inputs['title'];
        $question->bank_id = $bank_id;
        $question->priority = $inputs['priority'];
        $question->save();
        foreach($question->selects as $select)
        {
            $select->delete();
        }

        if($question->type == 1)
        {
            $radioNum = $inputs['radioNUM'];
            for($i = 1;$i<=$radioNum; $i++)
            {
                
                $select = new QuestionSelect;
                $select->title = $inputs['radioselect'.$i];
                $select->question_id = $question->id;
                if(array_key_exists('radioture', $inputs))
                {
                    if($inputs['radioture']== $i)
                    {
                        $select->if_true = 1;
                    }
                }
                else
                {
                    $question->delete();
                    return false;
                }
                $select->save();
            }
        }



        if($question->type==2)
        {
            $mulitNum = $inputs['mulitNUM'];
            $mulitTrue = $inputs['multipleture'];
            for($i = 1;$i<=$mulitNum; $i++)
            {
                $select = new QuestionSelect;
                $select->title = $inputs['mulitselect'.$i];
                $select->question_id = $question->id;
                if(in_array($i, $mulitTrue))
                {
                    $select->if_true = 1;
                }
                
                $select->save();
            }
        }
        if($question->type == 3)
        {
            for($i = 1;$i<3; $i++)
            {
                $select = new QuestionSelect;
                if($i == 1)
                {
                    $select->title = "对";
                }
                else
                {
                    $select->title = "错";
                }
                $select->question_id = $question->id;
                if(array_key_exists('falseture', $inputs))
                {
                    if($inputs['falseture']==$i)
                    {
                        $select->if_true = 1;
                    }
                }
                else
                {
                    $question->delete();
                    return false;
                }
                $select->save();
            }
        }
        return true;

    }

    /**
     * Store a question.
     *
     * @param  array $inputs

     * @return void
     */
    public function store($inputs, $bank_id)
    {
        $result = 1;
        $question = new $this->model;
        $result = $this->saveQuestion($question, $inputs, $bank_id);
        return $result;
    }


    /**
     * update a item.
     *
     * @param  array $inputs

     * @return void
     */
    public function update($inputs, $bank_id, $question_id)
    {
        $result = 1;
        $question = $this->getById($question_id);
        $result = $this->saveQuestion($question, $inputs, $bank_id);
        return $result;
    }

}
