<?php namespace App\Repositories;

use App\Models\Language;

class LanguageRepository {

	/**
	 * The Language instance.
	 *
	 * @var App\Models\Language
	 */
	protected $language;

	/**
	 * Create a new LanguagegRepository instance.
	 *
	 * @param  App\Models\Language $language
	 * @return void
	 */
	public function __construct(Language $language)
	{
		$this->language = $language;
	}

	/**
	 * Get all languages.
	 *
	 * @return Illuminate\Support\Collection
	 */
	public function all()
	{
		return $this->language->all();
	}

	/**
	 * Update languages.
	 *
	 * @param  array  $inputs
	 * @return void
	 */
	public function update($inputs)
	{
		foreach ($inputs as $key => $value)
		{
			$language = $this->language->where('slug', $key)->firstOrFail();

			$language->title = $value;
			
			$language->save();
		}
	}

	/**
	 * Get languages collection.
	 *
	 * @param  App\Models\User
	 * @return Array
	 */
	public function getAllSelect()
	{
		$languageSelect = $this->all()->lists('title', 'id');

		return compact('languageSelect');
	}

}
