<?php namespace App\Repositories;

use App\Models\Course;
use App\Models\User;
use App\Models\CourseRecord;
use App\Models\CourseSpan;
use App\Repositories\CourseRepository;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
use Auth,DateInterval,Carbon\Carbon;
class CourseRecordRepository extends BaseRepository
{

    protected $course_gestion;

    public function __construct(
        CourseRecord $record,
        UserRepository $user_gestion,
        CourseRepository $course_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->model = $record;
        $this->course_gestion = $course_gestion;
        $this->user_gestion = $user_gestion;
        $this->organization_gestion = $organization_gestion;
    }

    public function getUnDistributeUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        $list = $this->getUnDistributeUserList($inputs,$id);
        $unDistributeUsers = User::with('profile')->with('department')->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $unDistributeUsers;
    }

    public function getUnDistributeUserList($inputs,$id)
    {
        $keyword = isset($inputs['search_word'])?$inputs['search_word']:"";
        $course = Course::find($id);
        $userList = $this->getApplyUserList($course);
        $list = $this->user_gestion->getUserIdByInput($keyword);
        $choseIdArray = $this->model->where('course_id',$id)->lists('user_id')->all();
        $unDistributeUsers = User::whereIn('id',$list)->whereIn('id',$userList)->whereNotIn('id',$choseIdArray)->where('status_id',2)->lists('id')->all();
        return $unDistributeUsers;

    }




    public function getDistributeUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        
        $list = $this->getDistributeUserList($inputs,$id);
        $DistributeUsers = User::with('profile')->with('department')->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $DistributeUsers;
    }

    public function getDistributeUserList($inputs,$id)
    {
        $keyword = isset($inputs['search_word'])?$inputs['search_word']:"";
       
        $list = $this->user_gestion->getUserIdByInput($keyword);
        $choseIdArray = $this->model->where('course_id',$id)->lists('user_id')->all();
        $DistributeUsers = User::whereIn('id',$list)->whereIn('id',$choseIdArray)->where('status_id',2)->lists('id')->all();
        return $DistributeUsers;

    }


    public function getOverdueUser($n,$inputs,$id,$orderby = 'created_at', $direction = 'desc')
    {
        
        $list = $this->getOverdueUserList($inputs,$id);
        $DistributeUsers = User::with('profile')->with('department')->whereIn('id',$list)->orderBy($orderby, $direction)->paginate($n);
        return $DistributeUsers;
    }


    public function getOverdueUserList($inputs,$id)
    {
        $keyword = isset($inputs['search_word'])?$inputs['search_word']:"";
       
        $list = $this->user_gestion->getUserIdByInput($keyword);
        $oldRecords = $this->model->where('course_id',$id)->whereIn('process', array(0,1,2))->get();
        foreach($oldRecords as $record)
        {
            if($record->course->limitdays > 0)
            {
                $limitDay = ((strtotime($record->created_at))
                             +$record->course->limitdays*24*60*60);

                $limittime =($limitDay-time());
                if($limittime < 0)
                {
                    $record->process = 4;
                    $record->save();
                }
            }
        }

        
        $choseIdArray = $this->model->where('course_id',$id)->where('process',4)->lists('user_id')->all();
        $DistributeUsers = User::whereIn('id',$list)->whereIn('id',$choseIdArray)->where('status_id',2)->lists('id')->all();
        return $DistributeUsers;

    }



    public function addRecords($inputs,$id)
    {

        error_log("in add");
        $res = array('result'=>true,'message'=>trans('record.distribute_success'));
        if($inputs['is_all'] === 'true')
        {
             $list = $this->getUnDistributeUserList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
        }

        foreach($list as $user_id)
        {
            $record = new $this->model;
            $record->course_id = $id;
            $record->user_id = $user_id;
            $record->save();
        }
        return $res;
    }
    public function redistribute($inputs,$id)
    {

        error_log("in rediis");
        $res = array('result'=>true,'message'=>trans('record.redistribute_success'));
        if($inputs['is_all'] === 'true')
        {
             $list = $this->getOverdueUserList($inputs,$id);
        }
        else
        {
            $list = $inputs['user_id'];
        }
         $records = $this->model->where('course_id',$id)->whereIn('user_id',$list)->get();
        foreach($records as $record)
        {
            $record->delete();
        }

        foreach($list as $user_id)
        {
            $record = new $this->model;
            $record->course_id = $id;
            $record->user_id = $user_id;
            $record->save();
        }
        return $res;
    }

    public function deleteRecords($inputs,$id)
    {
        error_log("in delete");
        $res = array('result'=>true,'message'=>trans('record.undistribute_success'));
        error_log($inputs['is_all']);
        if($inputs['is_all'] === 'true')
        {
             $records = $this->model->where('course_id',$id)->get();
        }
        else
        {
            $list = $inputs['user_id'];
            $records = $this->model->where('course_id',$id)->whereIn('user_id',$list)->get();
        }

        foreach($records as $record)
        {
            $record->delete();
        }
        return $res;
    }




    public function saveRecordMessage($id,$inputs)
    {
        $variable = $inputs["variable"];
        $value = $inputs["value"];
        $scormtype = $inputs["scormtype"];
        if ( $variable == "initialize" and $value == "0" )
        {
            error_log("in start");
            $record = $this->model->where('user_id', Auth::user()->id)->where('course_id', $id)->firstOrFail();
            if(!$record->started_at)
            {
                $record->started_at = Carbon::now();
                $record->process = 1;
            }

            $record->study_times++;
            $record->save();
        }
        //If completed

        if( ( $variable == "cmi.completion_status" and $value == "completed" )||( $variable == "cmi.success_status" and $value == "1"))
        {
            error_log("in completion");
            $record = $this->model->where('user_id', Auth::user()->id)->where('course_id', $id)->firstOrFail();
            if($record->process == 1)
            {
                $record->end_at = Carbon::now();
                $record->process = 2;

                $record->save();

                $spans = CourseSpan::where('course_id',$id)->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();
                if($spans)
                {
                    $record->study_span += $spans[0]->study_span;
                    $record->save();
                    foreach($spans as $span)
                    {
                        $span->delete();
                    }
                }
               
            }


        }

        if($variable == "cmi.score.raw")
        {
            error_log("score study");
            $record = $this->model->where('user_id', Auth::user()->id)->where('course_id', $id)->firstOrFail();
            if($record->process < 2)
            {
                if($record->max_score < $value)
                {
                    $record->max_score = $value;
                }
                $record->save();
            }
                    
        }
        if ( $variable == "cmi.session_time" )
        {
            error_log("span");
            $span = new CourseSpan;
            $span->course_id = $id;
            $span->user_id = Auth::user()->id;

            //$record->study_span += substr($value,2,-1);

            $timespan = strstr($value,'.',TRUE);
            if($timespan)
            {
                $dv = new DateInterval($timespan.'S');
            }
            else
            {
                $dv = new DateInterval($value);
            }
            $span->study_span = $dv->h * 3600 + $dv->i * 60 + $dv->s;
            $span->save();
        }
        if($variable == "terminate" && $value == "start")
        {
            error_log("in over");
            $record = $this->model->where('user_id', Auth::user()->id)->where('course_id', $id)->firstOrFail();
            if($record->process < 2)
            {
                $spans = CourseSpan::where('course_id',$id)->where('user_id',Auth::user()->id)->orderBy('created_at','desc')->get();
                if($spans)
                {
                    $record->study_span += $spans[0]->study_span;


                    $record->save();

                    foreach($spans as $span)
                    {
                        $span->delete();
                    }
                }
            }

        }

        return response()->json([ 'result' => true ]);
    }




    public function lessons()
    {
        $oldRecords = $this->model->with("course")->where('user_id',Auth::user()->id)->whereIn('process', array(0,1,2))->get();

        foreach($oldRecords as $record)
        {

            if($record->process == 2)
            {
                $status = 0;
                if($record->course->examDistribution)
                {
                    $resultNUM = ExamResult::where('user_id',Auth::user()->id)->where('exam_id',$record->course->exam->id)->count();
                    if($record->exam_max_score > $record->course->exam->pass_score || ($resultNUM >= $record->course->exam->limit_times&&$record->course->exam->limit_times>0))
                    {
                        $status = 1;

                    }

                                    
                }
                else
                {
                    $status = 1;
                }
                if($record->course->evaluationRecord)
                {
                    if(Auth::user()->evaluationRecordResult->where('record_id',$record->course->evaluationRecord->id)->first())
                    {
                        $status++;
                    }
                }
                else
                {
                    $status++;
                }
                if($status == 2)
                {
                    $record->process = 3;
                }
                $record->save();
                            
            }
                    
        }
        $records =  $this->model->with("course")->where('user_id',Auth::user()->id)->whereIn('process', array(0,1,2))->orderBy('created_at', 'desc')->paginate(20);
       
        return $records;
            
    }



    public function lessonsDone()
    {
        $oldRecords = $this->model->with("course")->where('user_id',Auth::user()->id)->whereIn('process', array(0,1,2))->get();

        foreach($oldRecords as $record)
        {

            if($record->process == 2)
            {
                $status = 0;
                if($record->course->exam)
                {
                    $resultNUM = ExamResult::where('user_id',Auth::user()->id)->where('exam_id',$record->course->exam->id)->count();
                    if($record->exam_max_score > $record->course->exam->pass_score || ($resultNUM >= $record->course->exam->limit_times&&$record->course->exam->limit_times>0))
                    {
                        $status = 1;

                    }

                                    
                }
                else
                {
                    $status = 1;
                }
                if($record->course->evaluationRecord)
                {
                    if(Auth::user()->evaluationRecordResult->where('record_id',$record->course->evaluationRecord->id)->first())
                    {
                        $status++;
                    }
                                    
                }
                else
                {
                    $status++;
                }
                if($status == 2)
                {
                    $record->process = 3;
                }
                $record->save();
                            
            }
                    
        }

        $records = $this->model->with("course")->where('user_id',Auth::user()->id)->where('process', 3)->orderBy('created_at', 'desc')->paginate(20);
       
        return $records;
            
    }






    public function lessonsOverdue()
    {
        $oldRecords = $this->model->with("course")->where('user_id',Auth::user()->id)->whereIn('process', array(0,1,2))->get();

        foreach($oldRecords as $record)
        {
            if($record->course->limitdays > 0)
            {
                $limitDay = ((strtotime($record->created_at))
                             +$record->course->limitdays*24*60*60);

                $limittime =($limitDay-time());
                if($limittime < 0)
                {
                    $record->process = 4;
                    $record->save();
                }
            }
        }

        $records = $this->model->with("course")->where('user_id',Auth::user()->id)->where('process', 4)->orderBy('created_at', 'desc')->paginate(20);
       
        return $records;
            
    }





     public function getRecordByCourseId( $id )
    {
        $course = Course::findOrFail($id);
        $record = $this->model->where('course_id',$id)
                ->where('user_id',Auth::user()->id)->first();
        return $record;

    }



}
