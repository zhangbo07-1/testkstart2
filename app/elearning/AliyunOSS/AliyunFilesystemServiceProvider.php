<?php
namespace App\elearning\AliyunOSS;

use League\Flysystem\Filesystem;
use Illuminate\Support\ServiceProvider;

class AliyunFilesystemServiceProvider extends ServiceProvider
{

    public function boot()
    {
        \Storage::extend(
            'aliyun',
            function ($app, $config) {

                $qiniu_adapter = new AliyunAdapter(
                    $config['access_id'],
                    $config['access_key'],
                    $config['end_point'],
$config['bucket_name']
                );
                $file_system = new Filesystem($qiniu_adapter);

                return $file_system;
            }
        );
    }

    public function register()
    {
        //
    }
}
