<?php

namespace App\elearning\AliyunOSS;
use League\Flysystem\Adapter\AbstractAdapter;
use League\Flysystem\Adapter\Polyfill\NotSupportingVisibilityTrait;
use League\Flysystem\Adapter\Polyfill\StreamedCopyTrait;
use League\Flysystem\Adapter\Polyfill\StreamedReadingTrait;
use League\Flysystem\Adapter\Polyfill\StreamedWritingTrait;
use League\Flysystem\Config;
use App\elearning\AliyunOSS\SDK\ALIOSS;



class AliyunAdapter extends AbstractAdapter
{

    use NotSupportingVisibilityTrait, StreamedWritingTrait, StreamedReadingTrait;

    private $access_id = null;
    private $access_key = null;
    private $end_point = null;
    private $bucket = null;

    private $client = null;
    private $upload_manager = null;
    private $bucket_manager = null;
    private $operation = null;

    private $prefixedDomains = [];

    public function __construct($access_id, $access_key, $end_point)
    {
        $this->access_id = $access_id;
        $this->access_key = $access_key;
        $this->end_point = $end_point;
    }
    private function getClient()
    {
        if ($this->client == null) {
            $this->client = new ALIOSS($this->access_id,$this->access_key,$this->end_point);
        }

        return $this->client;
    }
    private function getBucket()
    {
        if ($this->bucket == null) {
            $this->bucket = 'elearningtest';//$_SERVER['HTTP_HOST'];
        }
        return $this->bucket;
    }

    private function creatBucket()
    {
	$bucket_name = $_SERVER['HTTP_HOST'];
	$acl = ALIOSS::OSS_ACL_TYPE_PRIVATE;
	$options = null;
	$response = $client->create_bucket($bucket_name,$acl,$options);
    }

    public function write($path, $contents, Config $config)
    {
        $client = $this->getClient();
        $bucket = $this->getBucket();
        $options = array(
            'content'=>$contents,
            'length'=>strlen($contents),
        );
        $response = $client->upload_file_by_content($bucket,$path,$options);
        return $response;
    }

    public function update($path, $contents, Config $config)
    {
        return $this->write($path, $contents, $config);
    }

    public function copy($path, $newpath)
    {
        $client = $this->getClient();
        $bucket = $this->getBucket();
        $response = $client->copy_object($bucket,$path,$bucket,$newpath);
        return $response;
    }

    public function delete($path)
    {
        $client = $this->getClient();
        $bucket = $this->getBucket();
        $response = $client->delete_object($bucket,$path);
    }

    public function listFiles($dirname,$recursive = false)
    {
        $client = $this->getClient();
        $bucket = $this->getBucket();
        $prefix = '';
        $marker = '';
        $delimiter = $dirname;
        $next_marker = '';
        $maxkeys = 1000;
        do
        {
            $options = array(
                'delimiter' => $delimiter,
                'prefix' => $prefix,
                'max-keys' => $maxkeys,
                'marker' => $next_marker,
            );

            $res = $client->list_object($bucket, $options);
            if ($res->isOk())
            {
                $body = $res->body;
                $xml = new SimpleXMLElement($body);
                foreach ($xml->Contents as $content)
                {
                    $files[] = $content->Key;
                }
                foreach ($xml->CommonPrefixes as $content)
                {
                    $contents[] =  $content->Prefix;
                }

            }
            if (empty($next_marker))
            {
                break;
            }
        }while($recursive);
        return array('files'=>$files,'contents'=>$contents);
    }


    public function deleteDir($dirname)
    {
        $res = $this->listFiles($dirname,true);
        $client = $this->getClient();
        $bucket = $this->getBucket();
	$options = array(
            'quiet' => true,
        );
        $files = $res['files'];
        $response = $client->delete_objects($bucket_name,$filess,$options);

        return $response;
    }

    public function createDir($dirname, Config $config)
    {
        return ['path' => $dirname];
    }

    public function rename($path, $newpath)
    {
        $response = $this->copy($path, $newpath);
        if($response)
        {
            $response = $this->delete($path);
        }
        return $response;
    }

    public function has($path)
    {
        $client = $this->getClient();
        $bucket = $this->getBucket();
        $res = $client->is_object_exist($bucket, $path);
        if ($res->status === 404)
        {
            return false;
        }
        if ($res->status === 200)
        {
            return true;;
        }
    }
    public function read($path)
    {
        $client = $this->getClient();
        $bucket = $this->getBucket();
        $options = array();
        $res = $client->get_object($bucket, $path, $options);
        $data = $res->body;
        return array('contents' => $data);
    }
    public function listContents($directory = '', $recursive = false)
    {
        $res = $this->listFiles($directory,$recursive);
        return  $res['contents'];
    }

    public function getMetadata($path)
    {
        $client = $this->getClient();
        $bucket = $this->getBucket();
        $options = null;
        $response = $client->get_object_meta($bucket,$path,$options);
        return $response;
    }

    public function getSize($path)
    {
        $stat = $this->getMetadata($path);
        if ($stat)
        {
            return array('size' => $stat->header['content-length']);
        }
        return false;
    }

    public function getMimetype($path)
    {
        $stat = $this->getMetadata($path);
        if ($stat)
        {
            return array('mimetype' =>$stat->header['content-type']);
        }
        return false;
    }


    public function getTimestamp($path)
    {
        $stat = $this->getMetadata($path);
        if ($stat)
        {
            return array('timestamp' => $stat->header['date']);
        }
        return false;
    }
}
