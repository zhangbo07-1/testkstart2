<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    public function manages()
    {
        return $this->hasMany('App\Models\ResourceManage','model_id');
    }
}
