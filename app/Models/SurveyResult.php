<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyResult extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
    public function Record()
    {
        return $this->belongsTo('App\Models\SurveyRecord','record_id');
    }
    public function selectResult()
    {
        return $this->hasMany('App\Models\ItemSelectResult','result_id');
    }
    public function gradeResult()
    {
        return $this->hasMany('App\Models\ItemGradeResult','result_id');
    }
}
