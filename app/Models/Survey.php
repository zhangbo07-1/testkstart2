<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    public function items()
    {
        return $this->hasMany('App\Models\Item');
    }


    public function records()
    {
        return $this->hasMany('App\Models\SurveyRecord');

    }
    public function manages()
    {
        return $this->hasMany('App\Models\SurveyManage','model_id');
    }
}
