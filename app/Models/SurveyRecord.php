<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyRecord extends Model
{
    public function course()
    {
        return $this->belongsTo('App\Models\course','course_id');
    }
    public function survey()
    {
        return $this->belongsTo('App\Models\survey','survey_id');
    }
    public function surveyResults()
    {
        return $this->hasMany('App\Models\SurveyResult','record_id');

    }
}
