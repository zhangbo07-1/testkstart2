<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamCombination extends Model
{
    public function bank()
    {
        return $this->belongsTo('App\Models\QuestionBank','bank_id');
    }
}
