<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSelect extends Model
{
    public function item()
    {
        return $this->belongsTo('App\Models\SurveyItem','item_id');
    }
    public function selectResults()
    {
        return $this->hasMany('App\Models\ItemSelectResult','select_id');
    }
}
