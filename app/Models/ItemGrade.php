<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemGrade extends Model
{
    public function item()
    {
        return $this->belongsTo('App\Models\SurveyItem','item_id');
    }
    public function gradeResult()
    {
        return $this->hasOne('App\Models\ItemGradeResult','grade_id');
    }
}
