<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    public function manages()
    {
        return $this->hasMany('App\Models\ExamManage','model_id');
    }
    public function catalog()
    {
        return $this->belongsTo('App\Models\Catalog','catalog_id');
    }
    public function combinations()
    {
        return $this->hasMany('App\Models\ExamCombination', 'exam_id');

    }
    public function maintains()
    {
        return $this->hasMany('App\Models\ExamMaintain','model_id');
    }
    public function distributions()
    {
        return $this->hasMany('App\Models\ExamDistribution','exam_id');
    }
}
