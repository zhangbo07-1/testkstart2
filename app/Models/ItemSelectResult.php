<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSelectResult extends Model
{
    public function select()
    {
        return $this->belongsTo('App\Models\ItemSelect','select_id');
    }
    public function result()
    {
        return $this->belongsTo('App\Models\SurveyResult','result_id');
    }
}
