<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    public function user()
    {
        return $this->hasMany('App\Models\User','department_id');
    }

}
