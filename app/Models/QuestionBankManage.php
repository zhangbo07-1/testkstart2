<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionBankManage extends Model
{
     public function department()
    {
        return $this->belongsTo('App\Models\Organization','manage_id');
    }
}
