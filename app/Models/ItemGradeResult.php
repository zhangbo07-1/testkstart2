<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemGradeResult extends Model
{
    public function grade()
    {
        return $this->belongsTo('App\Models\ItemGrade','grade_id');
    }
    public function result()
    {
        return $this->belongsTo('App\Models\SurveyResult','result_id');
    }
}
