<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionResult extends Model
{
    public function question()
    {
        return $this->belongsTo('App\Models\Question','question_id');

    }
    public function examResult()
    {
        return $this->belongsTo('App\Models\ExamResult','result_id');

    }
}
