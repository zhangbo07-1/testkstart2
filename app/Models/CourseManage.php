<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CourseManage extends Model
{
    public function department()
    {
        return $this->belongsTo('App\Models\Organization','manage_id');
    }
}
