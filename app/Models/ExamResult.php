<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExamResult extends Model
{
    public function user()
    {
        return $this->belongsTo('App\Models\User','user_id');

    }
    public function distribution()
    {
        return $this->belongsTo('App\Models\ExamDistribution','distribution_id');

    }
    public function questionResult()
    {
        return $this->hasMany('App\Models\QuestionResult','result_id');

    }
}
