<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function manages()
    {
        return $this->hasMany('App\Models\CourseManage','model_id');
    }
    public function catalog()
    {
        return $this->belongsTo('App\Models\Catalog','catalog_id');
    }

    public function evaluationRecord()
    {
        return $this->hasOne('App\Models\SurveyRecord');
    }
    public function examDistribution()
    {
        return $this->morphOne('App\Models\ExamDistribution','model');

    }
    public function maintains()
    {
        return $this->hasMany('App\Models\CourseMaintain','model_id');
    }
    public function wares()
    {
        return $this->hasMany('App\Models\CourseWare','course_id');
    }
    public function record()
    {
        return $this->hasMany('App\Models\CourseRecord','course_id');

    }
}
