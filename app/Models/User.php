<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];
    public function profile()
    {
        return $this->hasOne('App\Models\Profile');

    }
    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }
    public function language()
    {
        return $this->belongsTo('App\Models\Language');
    }
    public function userStatus()
    {
        return $this->belongsTo('App\Models\UserStatus','status_id');
    }
    public function department()
    {
        return $this->belongsTo('App\Models\Organization','department_id');
    }
    public function manages()
    {
        return $this->hasMany('App\Models\UserManage','model_id');
    }
    public function posts()
    {
        return $this->hasMany('App\Models\Post');
    }
    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }
    public function loginRecords()
    {
        return $this->hasMany('App\Models\LoginRecord');
    }
    public function surveyResult()
    {
        return $this->hasMany('App\Models\SurveyResult');
    }

    public function maintainCourses()
    {
        return $this->hasMany('App\Models\CourseMaintain','maintain_id');
    }

    public function maintainExams()
    {
        return $this->hasMany('App\Models\ExamMaintain','maintain_id');
    }

    public function courseRecord()
    {
        return $this->hasMany('App\Models\CourseRecord');

    }

    public function examRecord()
    {
        return $this->hasMany('App\Models\ExamRecord');
    }
    public function examDistributions()
    {
        return $this->morphMany('App\Models\ExamDistribution','model');

    }

}
