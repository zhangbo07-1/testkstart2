<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function survey()
    {
        return $this->belongsTo('App\Models\survey','survey_id');

    }
    public function selects()
    {
        return $this->hasMany('App\Models\ItemSelect');

    }
    public function grade()
    {
        return $this->hasOne('App\Models\ItemGrade');

    }
}
