<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailRecord extends Model
{
    protected $connection = 'mysqlLogs';
    protected $table = 'email_logs';
}
