<?php 

namespace App\Providers;
use League\Flysystem\Filesystem;
use Illuminate\Support\ServiceProvider;
use Storage;
use elearning\AliyunStorage\AliyunAdapter;
class AliyunFilesystemServiceProvider extends ServiceProvider
{

    public function boot()
    {
error_log("in aliyun");
        Storage::extend(
            'aliyun',
            function ($app, $config) {

                $qiniu_adapter = new AliyunAdapter(
                    $config['access_id'],
                    $config['access_key'],
                    $config['end_point'],
$config['bucket_name']
                );
                $file_system = new Filesystem($qiniu_adapter);

                return $file_system;
            }
        );
    }

    public function register()
    {
        //
    }
}
