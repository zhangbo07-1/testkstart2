<?php

namespace App\Providers;
use App\Policies\PostPolicy;
use App\Policies\UserPolicy;
use App\Policies\CommentPolicy;
use App\Policies\ResourcePolicy;
use App\Policies\MessagePolicy;
use App\Policies\SurveyPolicy;
use App\Policies\QuestionBankPolicy;
use App\Policies\CoursePolicy;
use App\Policies\ExamPolicy;
use App\Models\User;
use App\Models\Post;
use App\Models\Comment;
use App\Models\Resource;
use App\Models\Message;
use App\Models\Survey;
use App\Models\QuestionBank;
use App\Models\Course;
use App\Models\Exam;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;



class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Post::class => PostPolicy::class,
        User::class => UserPolicy::class,
        Comment::class => CommentPolicy::class,
        Resource::class => ResourcePolicy::class,
        Message::class => MessagePolicy::class,
        Survey::class => SurveyPolicy::class,
        QuestionBank::class => QuestionBankPolicy::class,
        Course::class => CoursePolicy::class,
        Exam::class => ExamPolicy::class,
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
        /* $gate->define('view-post', function ($user) {
            return $user->role_id == 1;

        });
        */
        // $gate->define('view-post', 'App\Policies\PostPolicy@view');

        //
    }
}
