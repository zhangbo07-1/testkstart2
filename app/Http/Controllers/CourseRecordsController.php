<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CourseRepository;
use App\Repositories\CourseRecordRepository;
use Input,Gate,Auth,Redirect;
class CourseRecordsController extends Controller
{

    protected $course_gestion;

    protected $record_gestion;



    public function __construct(
        CourseRepository $course_gestion,
        CourseRecordRepository $record_gestion
    )
    {
        $this->record_gestion = $record_gestion;
        $this->course_gestion = $course_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( $course_id )
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('modify',$course))
        {
            return Redirect::back();
        }
        return view('courses.distribute', compact('course'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $id = $request->input('course_id');
        $course = $this->course_gestion->getById($id);
        if (Gate::denies('modify',$course))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->record_gestion->addRecords($request->all(),$id);
        return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {

        $id = $request->input('course_id');
        error_log($id);
        $course = $this->course_gestion->getById($id);
        if (Gate::denies('modify',$course))
        {
            return response()->json(['result'=>false]);
        }
        error_log("in delete");
        $res = $this->record_gestion->deleteRecords($request->all(),$id);
        return response()->json($res);

    }


    public function redistribute(Request $request,$id)
    {
        $id = $request->input('course_id');
        $course = $this->course_gestion->getById($id);
        if (Gate::denies('modify',$course))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->record_gestion->redistribute($request->all(),$id);
        return response()->json($res);
    }




    public function getDistributeUserList(Request $request,$id)
    {
        $distributeUsers = $this->record_gestion->getDistributeUser(5,$request->all(),$id);

        return response()->json($distributeUsers);
    }

    public function getUnDistributeUserList(Request $request,$id)
    {
        $unDistributeUsers = $this->record_gestion->getUnDistributeUser(5,$request->all(),$id);
         return response()->json($unDistributeUsers);
    }

    public function getOverdueUserList(Request $request,$id)
    {
        $overdueUsers = $this->record_gestion->getOverdueUser(5,$request->all(),$id);
         return response()->json($overdueUsers);
    }

    public function saveRecordMessage(Request $request,$id)
    {
         $course = $this->course_gestion->getById($id);
         $this->record_gestion->saveRecordMessage($course->id,$request->all());
         return response()->json([ 'result' => true ]);
    }


    public function lessons()
    {
        $records = $this->record_gestion->lessons();
        $examRecords = array();

        return view('courses.lesson',compact('records','examRecords'));

    }


    public function lessonsDone()
    {
        $records = $this->record_gestion->lessonsDone();
        $examRecords = array();
        return view('courses.lesson-done',compact('records','examRecords'));
    }

    public function lessonsOverdue()
    {
        $records = $this->record_gestion->lessonsOverdue();
        $examRecords = array();
        return view('courses.lesson-overdue',compact('records','examRecords'));
    }




     public function getLog( $course_id )
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $record = $this->record_gestion->getRecordByCourseId($course->id);
        $examresult = '';
        $evaresult = '';


        return view('courses.log',['record'=>$record,'examResults'=>$examresult,'evaResults'=>$evaresult]);

    }





}
