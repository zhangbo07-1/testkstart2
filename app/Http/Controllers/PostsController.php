<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PostRepository;
use Gate,Auth,Redirect;

class PostsController extends Controller
{
    /**
     * The PostRepository instance.
     *
     * @var App\Repositories\PostRepository
     */
    protected $post_gestion;

    /**
     * Create a new BlogController instance.
     *
     * @param  App\Repositories\PostRepository $blog_gestion
     * @return void
     */
    public function __construct(
        PostRepository $post_gestion
    )
    {
        $this->post_gestion = $post_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = $this->post_gestion->index(6);
        return view('posts.index',compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (!Gate::denies('create_post',Auth::user()))
        {
            return view('posts/create');
        }
        return Redirect::to('posts');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        if (Gate::denies('create_post',Auth::user()))
        {
            return Redirect::back();
        }
        $post = $this->post_gestion->store($request->all(), $request->user()->id);
        return redirect()->route('posts.show', ['id' => $post->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = $this->post_gestion->getById($id);
        return view('posts/show')->withPost($post);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->post_gestion->getById($id);
        if (Gate::denies('modify',$post))
        {
           return Redirect::back();
        }
        return view('posts/edit')->withPost($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $post = $this->post_gestion->getById($id);
        if (Gate::denies('modify',$post))
        {
           return Redirect::back();
        }
        $post = $this->post_gestion->update($request->all(), $id);
        return redirect()->route('posts.show', ['id' => $post->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = $this->post_gestion->getById($id);
        if (!Gate::denies('delete',$post))
        {
            $this->post_gestion->destroy($id);
        }
        return Redirect::to('posts');
    }

    public function manage()
    {
        $posts = $this->post_gestion->index(6);
        return view('posts/manage',compact('posts'));
    }

}
