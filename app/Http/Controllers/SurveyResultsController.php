<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CourseRepository;
use App\Repositories\SurveyResultRepository;
use App\Repositories\SurveyRepository;
use Gate,Auth,Redirect,Validator;
class SurveyResultsController extends Controller
{
    protected $result_gestion;
    protected $survey_gestion;
    protected $course_gestion;
    public function __construct(
        CourseRepository $course_gestion,
        SurveyResultRepository $result_gestion,
        SurveyRepository $survey_gestion
    )
    {
        $this->result_gestion = $result_gestion;
        $this->survey_gestion = $survey_gestion;
        $this->course_gestion = $course_gestion;

    }

    public function score(Request $request, $id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('apply',$survey))
        {
           return Redirect::back();
        }
        $record = $this->survey_gestion->getRecordBySurveyId($survey->id);
        $this->result_gestion->score($record->id,$request->all());
        return Redirect::to('/');


    }

    public function CourseScore(Request $request, $course_id,$id)
    {
        $survey = $this->survey_gestion->getById($id);
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        $record = $this->survey_gestion->getRecordBySurveyId($survey->id,$course->id);
        $this->result_gestion->score($record->id,$request->all());
        return redirect('lessons/');

    }

    public function result($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifySurvey',$survey))
        {
           return Redirect::back();
        }
        $result = $this->result_gestion->result($id);
        return view('survey.result',compact('survey','result'));
    }

}
