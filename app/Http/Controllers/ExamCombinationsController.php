<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\BankRepository;
use App\Repositories\ExamRepository;
use App\Repositories\ExamCombinationRepository;
use Input,Gate,Auth,Redirect;
class ExamCombinationsController extends Controller
{
    protected $exam_gestion;
    protected $bank_gestion;
    protected $combination_gestion;


    public function __construct(
        BankRepository $bank_gestion,
        ExamRepository $exam_gestion,
        ExamCombinationRepository $combination_gestion )
    {
        $this->exam_gestion = $exam_gestion;
        $this->combination_gestion = $combination_gestion;
        $this->bank_gestion = $bank_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        if (Gate::denies('modify',$exam))
        {
            return Redirect::back();
        }
        $unSelectBanks = $this->combination_gestion->getUNSelect($exam->id);
        return view('exams.combinate',compact('exam','unSelectBanks'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $exam = $this->exam_gestion->getById($id);
        if (Gate::denies('modify',$exam))
        {
            return Redirect::back();
        }
        $res = $this->combination_gestion->saveCombination($request->all(),$id);
        if($res['status'])
            return redirect()->back()->withErrors($res['error'])->withInput();
        return Redirect::back();



}

/**
 * Display the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function show($id)
{
    //
}

/**
 * Show the form for editing the specified resource.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function edit($id)
{
    //
}

/**
 * Update the specified resource in storage.
 *
 * @param  \Illuminate\Http\Request  $request
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function update(Request $request, $id)
{
    //
}

/**
 * Remove the specified resource from storage.
 *
 * @param  int  $id
 * @return \Illuminate\Http\Response
 */
public function destroy($id)
{
    //
}
}
