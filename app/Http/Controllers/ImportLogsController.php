<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ImportLogRepository;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
use Input,Redirect,Gate,Auth,Response;
class ImportLogsController extends Controller
{
     /**
     * The UserRepository instance.
     *
     * @var App\Repositories\UserRepository
     */
    protected $user_gestion;

    /**
     * The Organization instance.
     *
     * @var App\Repositories\OrganizationRepository
     */
    protected $organization_gestion;
    /**
     * The ImportLog instance.
     *
     * @var App\Repositories\ImportLogRepository
     */
    protected $import_gestion;
    /**
     * Create a new UserController instance.
     *
     * @param  App\Repositories\UserRepository $user_gestion

     * @return void
     */
    public function __construct(
        UserRepository $user_gestion,
        ImportLogRepository $import_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->user_gestion = $user_gestion;
        $this->import_gestion = $import_gestion;
        $this->organization_gestion = $organization_gestion;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $import = $this->import_gestion->getById($id);
        return view('import.log',compact('import'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        error_log("deletesssssssssssssssssssss");
        $this->import_gestion->destroy($id);
        return redirect()->back();
    }

    public function downLoad($id)
    {
        $import = $this->import_gestion->getById($id);
        $file = $this->import_gestion->downLoad($id);
        return Response::download($file,$import->filename);
    }


    /**
     * Import a user list xls file.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function userImport(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('import_users',$user))
        {
            return Redirect::back();
        }
        if(Input::hasFile('file'))
        {
            $extension = Input::file('file')->getClientOriginalExtension();
            if($extension != 'xls'&&$extension != 'xlsx')
            {
                return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00006')));
            }
            $rslt =$this->user_gestion->import(Input::file('file'));
            if($rslt['type']>0)
                return redirect()->back()->withInput()->withErrors(array('file'=>$rslt['error']));
            return Redirect::back();
        }
        else
        {
            return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00007')));
        }
    }


     public function userImportView()
    {
        $imports = $this->import_gestion->index(10,1);
        return view('users.import',compact('imports'));
    }



        public function organizationImportView()
    {
        $imports = $this->import_gestion->index(10,2);
        return view('organization.import',compact('imports'));
    }

     /**
     * Import a organization list xls file.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function organizationImport(Request $request)
    {
        $user = Auth::user();
        if (Gate::denies('import_organization',$user))
        {
            return Redirect::back();
        }
        if(Input::hasFile('file'))
        {
            $extension = Input::file('file')->getClientOriginalExtension();
            if($extension != 'xls')
            {
                return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00006')));
            }
            $rslt =$this->organization_gestion->import(Input::file('file'));
            if($rslt['type']>0)
                return redirect()->back()->withInput()->withErrors(array('file'=>$rslt['error']));
            return Redirect::back();
        }
        else
        {
            return redirect()->back()->withInput()->withErrors(array('file'=>trans('error.00007')));
        }
    }

}
