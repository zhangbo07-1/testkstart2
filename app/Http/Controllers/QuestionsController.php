<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\BankRepository;
use App\Repositories\QuestionRepository;
use Gate,Auth,Redirect;
class QuestionsController extends Controller
{
    /**
     * The PostRepository instance.
     *
     * @var App\Repositories\PostRepository
     */
    protected $bank_gestion;

    /**
     * The ItemRepository instance.
     *
     * @var App\Repositories\ItemRepository
     */
    protected $question_gestion;
    /**
     * Create a new BlogController instance.
     *
     * @param  App\Repositories\PostRepository $blog_gestion
     * @return void
     */
    public function __construct(
        BankRepository $bank_gestion,
        questionRepository $question_gestion
    )
    {
        $this->bank_gestion = $bank_gestion;
        $this->question_gestion = $question_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $bank = $this->bank_gestion->getById($id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        return view('questions.index',compact('bank'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $bank = $this->bank_gestion->getById($id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        return view('questions.create',compact('bank'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $this->validate($request, [
            'title' => 'required|unique:questions',
        ]);
        $bank = $this->bank_gestion->getById($id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        $this->question_gestion->store($request->all(), $id);
        return redirect()->route('question-bank.questions.index', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($bank_id,$question_id)
    {
        $bank = $this->bank_gestion->getById($bank_id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        $question = $this->question_gestion->getById($question_id);
        return view('questions.edit',compact('question','bank'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$bank_id, $question_id)
    {
        $this->validate($request, [
            'title' => 'required|unique:questions,title,'.$question_id.'',
        ]);
        $bank = $this->bank_gestion->getById($bank_id);
        if (Gate::denies('maintain',$bank))
        {
            return Redirect::back();
        }
        $this->question_gestion->update($request->all(), $bank_id,$question_id);
        return redirect()->route('question-bank.questions.index', ['id' => $bank_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($bank_id,$question_id)
    {
        $bank = $this->bank_gestion->getById($bank_id);
        if (!Gate::denies('maintain',$bank))
        {
            $this->question_gestion->destroy($question_id);
        }
        return Redirect::back();
    }
}
