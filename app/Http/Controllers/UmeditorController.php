<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\UploaderRepository;
use Input;
class UmeditorController extends Controller
{

    protected $upfile_gestion;
    public function __construct(
        UploaderRepository $upfile_gestion
    )
    {
        $this->upfile_gestion = $upfile_gestion;

    }
    public function fileUpdateAPI()
    {
        $this->upfile_gestion->upFile(Input::all());
        $info = $this->upfile_gestion->getFileInfo();
        return response()->json($info);
    }
}
