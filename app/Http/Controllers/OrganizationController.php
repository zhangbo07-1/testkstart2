<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\OrganizationRepository;
use Input,Redirect,Auth,Excel;
class OrganizationController extends Controller
{

    /**
     * The OrganizationRepository instance.
     *
     * @var App\Repositories\OrganizationRepository
     */
    protected $organization_gestion;

    /**
     * Create a new UserController instance.
     *
     * @param  App\Repositories\OrganizationRepository $organization_gestion

     * @return void
     */
    public function __construct(
        OrganizationRepository $organization_gestion)
    {
        $this->organization_gestion = $organization_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('organization/index');
    }

    public function manageSelect()
    {
        return view('organization/manageSelect');
    }
    public function departmentSelect()
    {
        return view('organization/departmentSelect');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        if(Input::has('operation'))
        {
            $operation = Input::get('operation');
            try {
                $rslt = null;
                switch($operation) {
                    case 'get_node':
                        if(Input::has('user_id'))
                        {
                            $user_id = Input::get('user_id');
                        }
                        else
                        {
                            $user_id = Auth::user()->id;
                        }
                        $node = Input::has('id') && Input::get('id') !== '#' ? Input::get('id') : 0;
                        $manage = Input::has('manage') && Input::get('manage') !== '' ? Input::get('manage') : '/';
                        $NUM = Input::has('NUM') && Input::get('NUM') !== '' ? Input::get('NUM') : false;
                        $rslt = $this->organization_gestion->lst($node,$user_id,$manage,$NUM);
                        break;
                    case 'create_node':
                        $node = Input::has('id') && Input::get('id') !== '#' ? Input::get('id') : '/';
                        $rslt =$this->organization_gestion->create($node, Input::has('text') ? Input::get('text') : '');
                        break;
                    case 'rename_node':
                        $node = Input::has('id') && Input::get('id') !== '#' ? Input::get('id') : '/';
                        $rslt = $this->organization_gestion->rename($node,Input::has('text') ? Input::get('text') : '');
                        break;
                    case 'delete_node':
                        $node = Input::has('id') && Input::get('id') !== '#' ? Input::get('id') : '/';
                        $rslt = $this->organization_gestion->remove($node);
                        break;
                    case 'search_node':
                        $keyword = Input::has('keyword') ? Input::get('keyword') : '/';
                        $rslt = $this->organization_gestion->search($keyword);
                        break;
                    default:
                        break;
                }
                return response()->json($rslt);

            }
            catch (Exception $e) {
                header($_SERVER["SERVER_PROTOCOL"] . ' 500 Server Error');
                header('Status:  500 Server Error');
                // echo $e->getMessage();
            }

        }
    }


     public function exampleExcel()
    {
        error_log("excel");
        $progs = $this->organization_gestion->exampleExcel();
        Excel::create('Organizational Template', function($excel) use($progs)
                      {

                          $excel->sheet('Organizational Template', function($sheet) use($progs)
                                        {
                                            $sheet->fromArray($progs, null, 'A1', true,false);
                                        });
                      })->export('xls');

    }


}
