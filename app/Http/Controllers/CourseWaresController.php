<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CourseRepository;
use App\Repositories\CourseWareRepository;
use Input,Gate,Auth,Redirect,Response;
class CourseWaresController extends Controller
{

    protected $course_gestion;

    protected $courseware_gestion;



    public function __construct(
        CourseRepository $course_gestion,
        CourseWareRepository $courseware_gestion
    )
    {
        $this->course_gestion = $course_gestion;
        $this->courseware_gestion = $courseware_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $coursewares = $this->courseware_gestion->index(5,$course->id);
        return view('courses.upload',compact('course','coursewares'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $course = $this->course_gestion->getById($id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $res = $this->courseware_gestion->store($id,$request->all(),$request->user()->id);
        if($res['status'])
            $error = array('success'=>'保存成功');
        else
            $error = $res['error'];
        return redirect()->back()->withErrors($error)->withInput();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function downLoad($id)
    {
        $ware = $this->courseware_gestion->getById($id);
        $file = $this->courseware_gestion->downLoad($id);
        return Response::download($file,$ware->filename);

    }


    public function launch($course_id)
    {
         $course = $this->course_gestion->getCourseByCourseId($course_id);
         $res = $this->courseware_gestion->startWare($course->id);
         if($res['status'])
         {
             return view('courses.launch',array_merge($res['data']));
         }
         else
             return $res['data'];
    }


}
