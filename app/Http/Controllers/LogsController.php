<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\LogRepository;
use Gate,Auth,Redirect;
class LogsController extends Controller
{
    /**
     * The EmailtempletRepository instance.
     *
     * @var App\Repositories\EmailtempletRepository
     */
    protected $log_gestion;

    /**
     * Create a new BlogController instance.
     *
     * @param  App\Repositories\LogRepository $blog_gestion
     * @return void
     */
    public function __construct(
        LogRepository $log_gestion
    )
    {
        $this->log_gestion = $log_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function email(Request $request)
    {
        if (Gate::denies('manage_logs',Auth::user()))
        {
            return Redirect::back();
        }
        $logs =  $this->log_gestion->findEmailLogIdByInput(20,$request->all());
        return view('logs.email',compact('logs'));
    }
    public function login(Request $request)
    {
         if (Gate::denies('manage_logs',Auth::user()))
        {
            return Redirect::back();
        }
        $logs = $this->log_gestion->findLoginIdByInput(20,$request->all());
        return view('logs.login')->withLogs($logs);
    }
}
