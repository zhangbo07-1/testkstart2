<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\LanguageRepository;
use App\Repositories\CatalogRepository;
use App\Repositories\CourseRepository;
use App\Repositories\SurveyRepository;
use App\Repositories\CourseMaintainRepository;
use Input,Gate,Auth,Redirect;
class CoursesController extends Controller
{
        /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\CourseLanguageRepository
     */
    protected $language_gestion;

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\CourseCatalogRepository
     */
    protected $catalog_gestion;

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\CourseCatalogRepository
     */
    protected $user_gestion;

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\CourseRepository
     */
    protected $course_gestion;

    protected $survey_gestion;





    public function __construct(
        LanguageRepository $language_gestion,
        CourseRepository $course_gestion,
        CatalogRepository $catalog_gestion,
        SurveyRepository $survey_gestion,
        CourseMaintainRepository $maintain_gestion
    )
    {
        $this->language_gestion = $language_gestion;
        $this->catalog_gestion = $catalog_gestion;
        $this->course_gestion = $course_gestion;
        $this->survey_gestion = $survey_gestion;
        $this->maintain_gestion = $maintain_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('manage_course',Auth::user()))
        {
            return Redirect::back();
        }
        $courses = $this->course_gestion->index(10,Input::all());
        return view('courses.index',compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $manages = "";
        foreach(Auth::user()->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
        }
        return view('courses.create', array_merge(compact('manages'),
                                                   $this->language_gestion->getAllSelect()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manage_course',Auth::user()))
        {
            return Redirect::back();
        }
        $this->validate($request, [
            'course_id'         => 'required|regex:/^[a-zA-Z\d\-\_]+$/|max:100|unique:courses,course_id',
            'language_id' => 'required|integer',
            'title'              => 'required',
            'hours'             => 'required|integer|between:0,1000',
            'minutes'           => 'required|integer|between:0,60',
            'limitdays'         => 'required|integer|between:0,1000',
            'if_order'          => 'integer|between:0,1',
            'emailed'           => 'integer|between:0,1',
            'if_send_new_user'  => 'integer|between:0,1',
        ]);
        if(!$request->has('catalog_id'))
        {
            return redirect()->back()->withErrors(array('catalog_id'=>trans('error.00017')))->withInput();
        }

         $res = $this->course_gestion->store($request->all(),$request->user()->id);
         if(!$res['status'])
        {
            return redirect()->back()->withErrors($res['error'])->withInput();
        }
         return redirect()->route('courses.edit', ['id' => $res['id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $manages = "";
        foreach($course->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
        }

        return view('courses.edit', array_merge(compact('course','manages'),
                                                $this->language_gestion->getAllSelect()));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'course_id'         => 'required|regex:/^[a-zA-Z\d\-\_]+$/|max:100|unique:courses,course_id,'.$id,
            'language_id' => 'required|integer',
            'title'              => 'required',
            'hours'             => 'required|integer|between:0,1000',
            'minutes'           => 'required|integer|between:0,60',
            'limitdays'         => 'required|integer|between:0,1000',
            'if_order'          => 'integer|between:0,1',
            'emailed'           => 'integer|between:0,1',
            'if_send_new_user'  => 'integer|between:0,1',

        ]);
        if(!$request->has('catalog_id'))
        {
            return redirect()->back()->withErrors(array('catalog_id'=>trans('error.00017')))->withInput();
        }

        $course = $this->course_gestion->getById($id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }

         $res = $this->course_gestion->update($request->all(),$id);
         if(!$res['status'])
        {
            return redirect()->back()->withErrors($res['error'])->withInput();
        }
         return redirect()->route('courses.edit', ['id' => $res['id']]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('modify',$course))
        {
            return Redirect::back();
        }
        $this->course_gestion->destroy($id);
    }

    public function assignTeacher($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        return view('courses.teachers', compact('course'));
    }


    public function assignEvaluation($course_id)
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        $evaluations = $this->survey_gestion->lst(5,2,Input::all());
        return view('courses.evaluation', compact('course','evaluations'));
    }


    public function assignExam( $course_id )
    {
        $course = $this->course_gestion->getCourseByCourseId($course_id);
        if (Gate::denies('maintain',$course))
        {
            return Redirect::back();
        }
        return view('courses.exam',compact('course'));

    }







}
