<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CommentRequest;
use App\Http\Controllers\Controller;
use App\Repositories\CommentRepository;
use Gate,Auth,Redirect;
class CommentsController extends Controller
{
    protected $comment_gestion;

    /**
     * Create a new BlogController instance.
     *
     * @param  App\Repositories\CommentRepository $blog_gestion
     * @return void
     */
    public function __construct(
        CommentRepository $comment_gestion
    )
    {
        $this->comment_gestion = $comment_gestion;

    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CommentRequest $request)
    {
        if (Gate::denies('create_comment',Auth::user()))
        {
            return Redirect::back();
        }
        $this->comment_gestion->store($request->all(), Auth::user()->id);
        return Redirect::back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = $this->comment_gestion->getById($id);
        if (!Gate::denies('delete',$comment))
        {
           $this->comment_gestion->destroy($id);
        }
        return Redirect::back();
    }
}
