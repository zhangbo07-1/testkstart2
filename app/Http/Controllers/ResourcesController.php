<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ResourceRepository;
use Gate,Auth,Redirect,Response;
use Validator;
class ResourcesController extends Controller
{
    /**
     * The ResourceRepository instance.
     *
     * @var App\Repositories\ResourceRepository
     */
    protected $resource_gestion;
    /**
     * Create a new RsourceController instance.
     *
     * @param  App\Repositories\ResourceRepository $resource_gestion
     * @return void
     */
    public function __construct(
        ResourceRepository $resource_gestion
    )
    {
        $this->resource_gestion = $resource_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('create_resource',Auth::user()))
        {
            return Redirect::to('/');
        }
        $resources = $this->resource_gestion->index(10,$request->all());
        return view('resources.index',compact('resources'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('create_resource',Auth::user()))
        {
            return Redirect::to('/');
        }
        $manages = "";
        foreach(Auth::user()->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
        }

        return view('resources.create',compact('manages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('create_resource',Auth::user()))
        {
            return Redirect::to('/');
        }
        $this->validate($request, [
            'name' => 'required|max:255',
        ]);
        $res = $this->resource_gestion->store($request->all(),$request->user()->id);
        if($res['status'])
            $error = array('success'=>'保存成功');
        else
            $error = $res['error'];
        return redirect()->back()->withErrors($error)->withInput();

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    public function downLoad($id)
    {
        $resource = $this->resource_gestion->getById($id);
        if (Gate::denies('apply',$resource))
        {
            return redirect()->back();
        }
        if($resource->type == 1)
        {
            $file = $this->resource_gestion->downLoad($id);
            return Response::download($file,$resource->filename);
        }
        else
        {
            return redirect()->to($resource->storagename);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $resource = $this->resource_gestion->getById($id);
        if (!Gate::denies('delete',$resource))
        {
            $this->resource_gestion->destroy($id);
        }
        return redirect()->back();
    }
    public function lists(Request $request)
    {
        $resources = $this->resource_gestion->lst(6,$request->all());
        return view('resources.lists',compact('resources'));
    }



}
