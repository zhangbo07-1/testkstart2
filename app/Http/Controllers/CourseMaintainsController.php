<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\CourseMaintainRepository;
use App\Repositories\CourseRepository;
use Gate;
class CourseMaintainsController extends Controller
{
    protected $maintain_gestion;
    protected $course_gestion;

    public function __construct(
        CourseMaintainRepository $maintain_gestion,
        CourseRepository $course_gestion
    )
    {
        $this->maintain_gestion = $maintain_gestion;
        $this->course_gestion = $course_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id = $request->input('course_id');
        $course = $this->course_gestion->getById($id);
        if (Gate::denies('modify',$course))
        {
            return response()->json(['result'=>false]);
        }
         $res = $this->maintain_gestion->addTeachers($request->all(),$id);
         return response()->json($res);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('course_id');
        $course = $this->course_gestion->getById($id);
        if (Gate::denies('modify',$course))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->maintain_gestion->deleteTeachers($request->all(),$id);
        return response()->json($res);
    }


    public function getAsssignedTeacherList(Request $request,$id)
    {
        $assignedTeachers = $this->maintain_gestion->getAsssignedTeacher(5,$request->all(),$id);

        return response()->json($assignedTeachers);
    }

    public function getUnAsssignedTeacherList(Request $request,$id)
    {
        $unAssignedTeachers = $this->maintain_gestion->getUnAsssignedTeacher(5,$request->all(),$id);
         error_log(count($unAssignedTeachers));
         return response()->json($unAssignedTeachers);
    }
}
