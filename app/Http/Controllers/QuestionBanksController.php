<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\BankRepository;
use Gate,Auth,Redirect;
class QuestionBanksController extends Controller
{
     /**
     * The PostRepository instance.
     *
     * @var App\Repositories\PostRepository
     */
    protected $bank_gestion;

    /**
     * Create a new BlogController instance.
     *
     * @param  App\Repositories\PostRepository $blog_gestion
     * @return void
     */
    public function __construct(
        BankRepository $bank_gestion
    )
    {
        $this->bank_gestion = $bank_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Gate::denies('manage_bank',Auth::user()))
        {
            return Redirect::back();
        }
        $banks = $this->bank_gestion->index(10);
        return view('questionBank.index',compact('banks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manage_bank',Auth::user()))
        {
            return Redirect::back();
        }
        return view('questionBank.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manage_bank',Auth::user()))
        {
            return Redirect::back();
        }
        $this->validate($request, [
            'title' => 'required|max:255',
        ]);
        $res = $this->bank_gestion->store($request->all(),Auth::user()->id);
        if(!$res['status'])
            return redirect()->back()->withErrors($res['error'])->withInput();
        return redirect()->route('question-bank.edit', ['id' => $res['id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bank = $this->bank_gestion->getById($id);
        if (Gate::denies('maintain',$bank))
        {
           return Redirect::back();
        }
        return view('questionBank.show',compact('bank'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bank = $this->bank_gestion->getById($id);
        if (Gate::denies('maintain',$bank))
        {
           return Redirect::back();
        }
        $manages = "";
        foreach($bank->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
        }
        return view('questionBank.edit',compact('bank','manages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $bank = $this->bank_gestion->getById($id);
        if (Gate::denies('maintain',$bank))
        {
           return Redirect::back();
        }
       $this->validate($request, [
            'title' => 'required|max:255',
        ]);
        $res = $this->bank_gestion->update($request->all(),$id);
        if(!$res['status'])
            return redirect()->back()->withErrors($res['error'])->withInput();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $bank = $this->bank_gestion->getById($id);
        if (!Gate::denies('delete',$bank))
        {
           $this->bank_gestion->destroy($id);
        }
        return Redirect::back();
    }
}
