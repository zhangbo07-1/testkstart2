<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\UserRepository;
use App\Repositories\OrganizationRepository;
use App\Repositories\RoleRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\UserStatusRepository;
use Input,Redirect,Excel,Gate,Auth;
class UsersController extends Controller
{

    /**
     * The UserRepository instance.
     *
     * @var App\Repositories\UserRepository
     */
    protected $user_gestion;
    /**
     * The RoleRepository instance.
     *

     * @var App\Repositories\RoleRepository
     */
    protected $role_gestion;

    /**
     * The RoleRepository instance.
     *
     * @var App\Repositories\UserStatusRepository
     */
    protected $userStatus_gestion;
    protected $language_gestion;
    /**
     * The Organization instance.
     *
     * @var App\Repositories\OrganizationRepository
     */
    protected $organization_gestion;

    /**
     * Create a new UserController instance.
     *
     * @param  App\Repositories\UserRepository $user_gestion

     * @return void
     */
    public function __construct(
        UserRepository $user_gestion,
        RoleRepository $role_gestion,
        LanguageRepository $language_gestion,
        UserStatusRepository $userStatus_gestion,
        OrganizationRepository $organization_gestion
    )
    {
        $this->user_gestion = $user_gestion;
        $this->userStatus_gestion = $userStatus_gestion;
        $this->language_gestion = $language_gestion;
        $this->role_gestion = $role_gestion;
        $this->organization_gestion = $organization_gestion;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = $this->user_gestion->index(10,Input::all());
        $status = 2;
        $userStatusSelects = $this->userStatus_gestion->getAllSelect();
        $userStatusSelects[0] = '全部';
        return view('users.index',compact('users','status','userStatusSelects'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('users.create', array_merge(
            $this->role_gestion->getAllSelect(),
            $this->language_gestion->getAllSelect()));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'password' => 'alpha_dash|between:5,20',
            'tel' => 'alpha_dash|max:255',
        ]);
        $res = $this->user_gestion->create($request->all());
        if(!$res['status'])
        {
            return redirect()->back()->withErrors($res['error'])->withInput();
        }
        return redirect()->action('UsersController@index');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($name)
    {
        $user = $this->user_gestion->getUserByName($name);
        if (Gate::denies('modify',$user))
        {
            return Redirect::back();
        }
        $manages = "";
        foreach($user->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
        }
        $userStatusSelects = $this->userStatus_gestion->getAllSelect();
        return view('users.edit',
                    array_merge(compact('user','manages','userStatusSelects'),
                                $this->role_gestion->getAllSelect(),
                                $this->language_gestion->getAllSelect()));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $name)
    {
        $this->validate($request, [
            'password' => 'alpha_dash|between:5,20',
            'tel' => 'alpha_dash|max:255',
        ]);
        $user = $this->user_gestion->getUserByName($name);
        if (Gate::denies('modify',$user))
        {
            return Redirect::back();
        }
        $res = $this->user_gestion->update($user->id,$request->all());
        if(!$res['status'])
        {
            return redirect()->back()->withErrors($res['error'])->withInput();
        }
        if ( $user->name == \Request::user()->name )
        {
            return redirect('profile');
        } else {
            return redirect()->action('UsersController@index');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($name)
    {

        $user = $this->user_gestion->getUserByName($name);
        if (!Gate::denies('delete',$user))
        {
            $this->user_gestion->closeUser($user->id);
        }
        return Redirect::back();
    }

   

    public function searchByDepartment()
    {
        $department_id = Input::has('id') && Input::get('id') !== '#' ? Input::get('id') : 0;
        if($department_id == 0)
        {
            $users = $this->user_gestion->index(10);
        }
        else
        {
            $users = $this->user_gestion->searchByDepatment(10,$department_id);
            error_log("end search");
        }
        return response()->json($users);

    }

    public function usersExcel()
    {

    }

    public function exampleExcel()
    {
        error_log("excel");
        $progs = $this->user_gestion->exampleExcel();
        Excel::create('User Info Template', function($excel) use($progs)
                      {

                          $excel->sheet('User Info Template', function($sheet) use($progs)
                                        {
                                            $sheet->fromArray($progs, null, 'A1', true,false);
                                        });
                      })->export('xls');

    }



    public function profile()
    {
        $user = Auth::user();
        if (Gate::denies('modify',$user))
        {
            return Redirect::back();
        }
        $userStatusSelects = $this->userStatus_gestion->getAllSelect();
        return view('users.show',
                    array_merge(compact('user','userStatusSelects'),
                                $this->role_gestion->getAllSelect(),
                                $this->language_gestion->getAllSelect()));
    }

}
