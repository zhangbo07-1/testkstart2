<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\SurveyRepository;
use Gate,Auth,Redirect;
class SurveysController extends Controller
{
     /**
     * The PostRepository instance.
     *
     * @var App\Repositories\PostRepository
     */
    protected $survey_gestion;
    protected $surveyType = 1;

    /**
     * Create a new BlogController instance.
     *
     * @param  App\Repositories\PostRepository $blog_gestion
     * @return void
     */
    public function __construct(
        SurveyRepository $survey_gestion
    )
    {
        $this->survey_gestion = $survey_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (Gate::denies('manage_survey',Auth::user()))
        {
            return Redirect::back();
        }
        $surveys = $this->survey_gestion->index(10,$this->surveyType,$request->all());
        return view('survey.index',compact('surveys'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manage_survey',Auth::user()))
        {
            return Redirect::back();
        }
        return view('survey.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('manage_survey',Auth::user()))
        {
            return Redirect::back();
        }
        $this->validate($request, [
            'title' => 'required|max:255',
        ]);
        $res = $this->survey_gestion->store($request->all(),$this->surveyType,Auth::user()->id);
        if(!$res['status'])
            return redirect()->back()->withErrors($res['error'])->withInput();
        return redirect()->route('survey.edit', ['id' => $res['id']]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifySurvey',$survey))
        {
           return Redirect::back();
        }
        return view('survey.show',compact('survey'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifySurvey',$survey))
        {
           return Redirect::back();
        }
        $manages = "";
        foreach($survey->manages as $manage)
        {
            $manages = $manage->department->name.",".$manages;
        }
        return view('survey.edit',compact('survey','manages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifySurvey',$survey))
        {
           return Redirect::back();
        }
       $this->validate($request, [
            'title' => 'required|max:255',
        ]);
        $res = $this->survey_gestion->update($request->all(),$id);
        if(!$res['status'])
            return redirect()->back()->withErrors($res['error'])->withInput();
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (!Gate::denies('deleteSurvey',$survey))
        {
           $this->survey_gestion->destroy($id);
        }
        return Redirect::back();
    }

    public function push($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (!Gate::denies('modifySurvey',$survey))
        {
           $this->survey_gestion->push($id);
        }
        return Redirect::back();

    }

    public function answer($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('apply',$survey))
        {
           return Redirect::back();
        }
        return view('survey.answer',compact('survey'));
    }

    public function lists()
    {
        $surveys = $this->survey_gestion->lst(5,1);
        return view('survey.list',compact('surveys'));
    }


}
