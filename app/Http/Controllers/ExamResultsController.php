<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ExamRepository;
use App\Repositories\ExamResultRepository;
use Gate,Redirect;
class ExamResultsController extends Controller
{
    protected $exam_gestion;

    protected $result_gestion;


    public function __construct(
        ExamRepository $exam_gestion,
        ExamResultRepository $result_gestion )
    {
        $this->exam_gestion = $exam_gestion;
        $this->result_gestion = $result_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



    public function courseScore(Request $request,$courseId,$examId)
    {
        $this->result_gestion->score($examId,$request->all(),$courseId);
        return redirect('lessons/');
    }
    public function mark($exam_id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        if (Gate::denies('maintain',$exam))
        {
            return Redirect::back();
        }
        $results = $this->result_gestion->mark($exam->id);
        return view('exams/mark',compact('results','exam'));
    }
    public function markQuestion($exam_id,$id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        $result = $this->result_gestion->getQuestionResult($id);
        return view('exams/markQuestion',compact('result','exam'));
    }

    public function scoreQuestion(Request $request,$exam_id,$id)
    {
        $exam = $this->exam_gestion->getExamByExamId($exam_id);
        $this->result_gestion->scoreQuestion($exam->id,$id,$request->all());
        return redirect()->to('exams/'.$exam->exam_id.'/mark');
    }

}
