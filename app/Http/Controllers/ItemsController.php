<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\SurveyRepository;
use App\Repositories\ItemRepository;
use Gate,Auth,Redirect;
class ItemsController extends Controller
{
    /**
     * The PostRepository instance.
     *
     * @var App\Repositories\PostRepository
     */
    protected $survey_gestion;

    /**
     * The ItemRepository instance.
     *
     * @var App\Repositories\ItemRepository
     */
    protected $Item_gestion;
    /**
     * Create a new BlogController instance.
     *
     * @param  App\Repositories\PostRepository $blog_gestion
     * @return void
     */
    public function __construct(
        SurveyRepository $survey_gestion,
        ItemRepository $item_gestion
    )
    {
        $this->survey_gestion = $survey_gestion;
        $this->item_gestion = $item_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifySurvey',$survey))
        {
           return Redirect::back();
        }
        return view('items.index',compact('survey'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifySurvey',$survey))
        {
           return Redirect::back();
        }
        return view('items.create',compact('survey'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $this->validate($request, [
            'title' => 'required|unique:items',
        ]);
        $survey = $this->survey_gestion->getById($id);
        if (Gate::denies('modifySurvey',$survey))
        {
           return Redirect::back();
        }
        $this->item_gestion->store($request->all(), $id);
        if($survey->type == 1)
            return redirect()->route('survey.items.index', ['id' => $id]);
        else
            return redirect()->route('evaluation.items.index', ['id' => $id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($survey_id,$item_id)
    {
        $survey = $this->survey_gestion->getById($survey_id);
        if (Gate::denies('modifySurvey',$survey))
        {
           return Redirect::back();
        }
        $item = $this->item_gestion->getById($item_id);
        return view('items.edit',compact('item','survey'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$survey_id,$item_id)
    {
        $this->validate($request, [
            'title' => 'required|unique:items,title,'.$item_id.'',
        ]);
        $survey = $this->survey_gestion->getById($survey_id);
        if (Gate::denies('modifySurvey',$survey))
        {
           return Redirect::back();
        }
        $this->item_gestion->update($request->all(), $survey_id,$item_id);
        if($survey->type == 1)
            return redirect()->route('survey.items.index', ['id' => $survey_id]);
        else
            return redirect()->route('evaluation.items.index', ['id' => $survey_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($survey_id,$item_id)
    {
        $survey = $this->survey_gestion->getById($survey_id);
        if (!Gate::denies('modifySurvey',$survey))
        {
            $this->item_gestion->destroy($item_id);
        }
        return Redirect::back();
    }
}
