<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\ExamRepository;
use App\Repositories\ExamDistributionRepository;
use Input,Gate,Auth,Redirect;
class ExamDistributionsController extends Controller
{
    protected $exam_gestion;
    protected $distribution_gestion;


     public function __construct(
         ExamRepository $exam_gestion,
         ExamDistributionRepository $distribution_gestion )
    {
        $this->distribution_gestion = $distribution_gestion;
        $this->exam_gestion = $exam_gestion;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        $id = $request->input('exam_id');
        $exam = $this->exam_gestion->getById($id);
        if (Gate::denies('modify',$exam))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->distribution_gestion->addDistributions($request->all(),$id);
        return response()->json($res);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
         $id = $request->input('exam_id');
        $exam = $this->exam_gestion->getById($id);
        if (Gate::denies('modify',$exam))
        {
            return response()->json(['result'=>false]);
        }
        $res = $this->distribution_gestion->deleteDistributions($request->all(),$id);
        return response()->json($res);
    }

     public function assignCourse(Request $request, $id )
    {
        $res = $this->distribution_gestion->AssignCourse($request->input('course_id'),$id);
        return response()->json(['result'=>true,'exam_title'=>$res->title,'message'=>trans('exam.assign_success')]);
    }

    public function getAssignList(Request $request)
    {
        $exams = $this->distribution_gestion->getAssignList(5,$request->all());
        return response()->json($exams);

    }


     public function getDistributeUserList(Request $request,$id)
    {
        $distributeUsers = $this->distribution_gestion->getDistributeUser(5,$request->all(),$id);

        return response()->json($distributeUsers);
    }

    public function getUnDistributeUserList(Request $request,$id)
    {
        $unDistributeUsers = $this->distribution_gestion->getUnDistributeUser(5,$request->all(),$id);
         return response()->json($unDistributeUsers);
    }


}
