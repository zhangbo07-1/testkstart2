<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\MessageRepository;
use Gate,Auth,Redirect;
class MessagesController extends Controller
{
    /**
     * The MessageRepository instance.
     *
     * @var App\Repositories\MessageRepository
     */
    protected $message_gestion;

    /**
     * Create a new MessageController instance.
     *
     * @param  App\Repositories\MessageRepository $Message_gestion
     * @return void
     */
    public function __construct(
        MessageRepository $message_gestion
    )
    {
        $this->message_gestion = $message_gestion;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messages = $this->message_gestion->index(6);
        return view('messages/index',compact('messages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (!Gate::denies('create_message',Auth::user()))
        {
            return view('messages/create');
        }
        return Redirect::to('messages');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Gate::denies('create_message',Auth::user()))
        {
            return Redirect::back();
        }
        $this->validate($request, [
            'title' => 'required|unique:messages|max:255',
            'body' => 'required',
        ]);
        $message = $this->message_gestion->store($request->all(), $request->user()->id);
        return redirect()->route('news-manage.show', ['id' => $message->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = $this->message_gestion->getById($id);
        if (!Gate::denies('apply', $message))
        {
            return view('messages/show')->withMessage($message);

        }
        return Redirect::back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message = $this->message_gestion->getById($id);
        if (Gate::denies('modify',$message))
        {
            return Redirect::back();
        }
        return view('messages/edit')->withMessage($message);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message = $this->message_gestion->getById($id);
        if (Gate::denies('modify',$message))
        {
           return Redirect::back();
        }
        $message = $this->message_gestion->update($request->all(), $id);
        return redirect()->route('news-manage.show', ['id' => $message->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $message = $this->message_gestion->getById($id);
        if (!Gate::denies('delete',$message))
        {
            $this->message_gestion->destroy($id);
        }
        return Redirect::back();
    }

    public function lists()
    {
        $messages = $this->message_gestion->index(6);
        return view('messages/lists',compact('messages'));
    }
}
