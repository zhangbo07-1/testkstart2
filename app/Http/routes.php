<?php

/*
   |--------------------------------------------------------------------------
   | Application Routes
   |--------------------------------------------------------------------------
   |
   | Here is where you can register all of the routes for an application.
   | It's a breeze. Simply tell Laravel the URIs it should respond to
   | and give it the controller to call when that URI is requested.
   |
 */
// 认证路由...
Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', ['as'=>'login','uses'=>'Auth\AuthController@postLogin']);
Route::get('logout', ['as'=>'logout','uses'=>'Auth\AuthController@getLogout']);
// 注册路由...
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', ['as'=>'register','uses'=>'Auth\AuthController@postRegister']);
Route::get('manual/registration', function () {
    return view('manual.registration');

});



Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'lang'], function () {
        Route::get('/', 'MenuController@index');
        Route::get('manual/manager', function () {
            return view('manual.manager.manager');
        });
        Route::get('manual/user/pc', function () {
            return view('manual.user.pc');

        });

        Route::get('manual/user/wechat', function () {
            return view('manual.user.wechat');

        });
        Route::get('catalogs','CatalogsController@index');
        Route::post('catalogs/edit','CatalogsController@edit');
        Route::get('catalogs/select','CatalogsController@catalogSelect');
        Route::get('organization','OrganizationController@index');
        Route::post('organization/show','OrganizationController@show');
        Route::get('organization/example','OrganizationController@exampleExcel');
        Route::get('organization/manage/select','OrganizationController@manageSelect');
        Route::get('organization/department/select','OrganizationController@departmentSelect');
        Route::get('organization/import','ImportLogsController@organizationImportView');
        Route::post('organization/import','ImportLogsController@organizationImport');
        Route::get('users/import','ImportLogsController@userImportView');
        Route::post('users/import','ImportLogsController@userImport');
        Route::get('users/department','UsersController@searchByDepartment');
        Route::get('users/excel','UsersController@usersExcel');
        Route::get('users/example','UsersController@exampleExcel');
        //Route::get('users','UsersController@searchByDepartment');
        Route::resource('users', 'UsersController');
        Route::get('import-log/{id}/download','ImportLogsController@downLoad');
        Route::resource('import-log','ImportLogsController');

        Route::get('profile','UsersController@profile');
        Route::get('posts-manage','PostsController@manage');
        Route::resource('posts', 'PostsController');
        Route::resource('comments', 'CommentsController');
        Route::resource('resources-manage', 'ResourcesController');
        Route::get('resources', 'ResourcesController@lists');
        Route::get('resources/{id}/download', 'ResourcesController@downLoad');
        Route::any('/api/imageUp', 'UmeditorController@fileUpdateAPI');
        Route::resource('news-manage', 'MessagesController');
        Route::get('news/lists','MessagesController@lists');
        Route::get('news/{id}','MessagesController@show');
        Route::resource('email-manage','EmailTempletsController');
        Route::get('log-manage','LogsController@login');
        Route::get('log-manage/login','LogsController@login');
        Route::get('log-manage/email','LogsController@email');
        Route::get('system-manage', 'SystemInfoController@logo');
        Route::get('system-manage/logo', 'SystemInfoController@logo');
        Route::post('system-manage/logo/upload', 'SystemInfoController@saveLogo');
        Route::get('system-manage/info','SystemInfoController@info');
        Route::post('system-manage/info','SystemInfoController@saveInfo');
        Route::get('system-manage/censor','SystemInfoController@censor');
        Route::post('system-manage/censor','SystemInfoController@saveCensor');
        Route::get('system-manage/email','SystemInfoController@email');
        Route::post('system-manage/email','SystemInfoController@saveEmail');
        Route::get('survey/lists','SurveysController@lists');
        Route::get('survey/{id}/answer', 'SurveysController@answer');
        Route::post('survey/{id}/score', 'SurveyResultsController@score');
        Route::get('survey/{id}/result', 'SurveyResultsController@result');
        Route::get('survey/{id}/push', 'SurveysController@push');
        Route::resource('survey.items', 'ItemsController');
        Route::resource('survey', 'SurveysController');
        Route::post('evaluation/{id}/distribute','EvaluationsController@distribute');
        Route::resource('evaluation.items', 'ItemsController');
        Route::resource('evaluation', 'EvaluationsController');
        Route::get('api/evaluation/distribute','EvaluationsController@getDistributeList');
        Route::resource('question-bank', 'QuestionBanksController');
        Route::resource('question-bank.questions', 'QuestionsController');
        Route::resource('exams','ExamsController');
        Route::resource('courses','CoursesController');

        Route::resource('courses.distribute','CourseRecordsController');
        Route::delete('courses/{id}/distribute','CourseRecordsController@destroy');
         Route::post('courses/{id}/redistribute','CourseRecordsController@redistribute');
        Route::get('courses/{course_id}/teachers','CoursesController@assignTeacher');
        Route::resource('courses.upload','CourseWaresController');
        Route::get('courses/{course_id}/exam','CoursesController@assignExam');
        Route::get('courses/{course_id}/evaluation','CoursesController@assignEvaluation');
        Route::get('courses/{course_id}/launch','CourseWaresController@launch');

        Route::post('courses/{course_id}/launch', 'CourseRecordsController@saveRecordMessage');
        Route::get('course-ware/{id}/download','CourseWaresController@download');
        Route::get('api/courses/{id}/assigned-teacher','CourseMaintainsController@getAsssignedTeacherList');
        Route::get('api/courses/{id}/unassigned-teacher','CourseMaintainsController@getUnAsssignedTeacherList');
        Route::get('api/courses/{id}/distribute','CourseRecordsController@getDistributeUserList');
        Route::get('api/courses/{id}/undistribute','CourseRecordsController@getUnDistributeUserList');
        Route::get('api/courses/{id}/overdue','CourseRecordsController@getOverdueUserList');
        Route::delete('courses-maintain','CourseMaintainsController@destroy');
        Route::post('courses-maintain','CourseMaintainsController@store');
        Route::resource('exams.combinate', 'ExamCombinationsController');

        Route::get('exams/{exam_id}/teachers', 'ExamsController@assignTeacher');
        Route::get('exams/{exam_id}/distribute','ExamsController@distribute');
        Route::get('exams/{exam_id}/mark', 'ExamResultsController@mark');
        Route::get('exams/{exam_id}/preview','ExamsController@preview');
        Route::get('exams/{exam_id}/result/{id}/mark','ExamResultsController@markQuestion');
        Route::post('exams/{exam_id}/questionResult/{id}/marking','ExamResultsController@scoreQuestion');
        Route::get('api/exams/assign-course','ExamDistributionsController@getAssignList');
        Route::post('exams/{id}/assign-course','ExamDistributionsController@assignCourse');
        Route::resource('exams.distribute','ExamDistributionsController');
        Route::delete('exams/{id}/distribute','ExamDistributionsController@destroy');
        Route::get('api/exams/{id}/distribute','ExamDistributionsController@getDistributeUserList');
        Route::get('api/exams/{id}/undistribute','ExamDistributionsController@getUnDistributeUserList');

        Route::get('lessons', 'CourseRecordsController@lessons');
        Route::get('lessons-done', 'CourseRecordsController@lessonsDone');
        Route::get('lessons-overdue', 'CourseRecordsController@lessonsOverdue');
        Route::get('lessons/{course_id}/log', 'CourseRecordsController@getLog');
        Route::get('lessons/{course_id}/exam/{exam_id}/answer', 'ExamsController@answer');

        Route::post('lessons/{course_id}/exams/{exam_id}/score', 'ExamResultsController@CourseScore');

         Route::get('lessons/{course_id}/evaluation/{evaluation_id}/answer', 'EvaluationsController@answer');
          Route::post('lessons/{course_id}/evaluation/{evaluation_id}/score', 'EvaluationResultsController@CourseScore');

        Route::post('lessons/{course_id}/evaluation/{evaluation_id}/score', 'SurveyResultsController@CourseScore');


        Route::get('api/exams/{id}/assigned-teacher','ExamMaintainsController@getAsssignedTeacherList');
        Route::get('api/exams/{id}/unassigned-teacher','ExamMaintainsController@getUnAsssignedTeacherList');

        Route::delete('exams-maintain','ExamMaintainsController@destroy');
        Route::post('exams-maintain','ExamMaintainsController@store');
    });
});
