var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
var manage_name =  window.opener.document.getElementById("manage_name").innerHTML;
var node_id;

function getTree(node) 
{

    var tree = "";
    $.ajax({
        url: "/organization/show?operation=get_node",
        type: 'GET',
        async: false,
        
        data:
        {
            _token: CSRF_TOKEN,
            id:  node,
            manage:manage_name,
        },
        dataType: 'JSON',
        success: function (obj)
        {
            
            tree = JSON.stringify(obj);
        }
    });
    console.log(tree);
    return tree;


}
$('#tree').treeview({
    data: getTree(0),
    multiSelect: true,
    collapseIcon: "glyphicon glyphicon-chevron-down",
    expandIcon: "glyphicon glyphicon-chevron-right",


    showCheckbox: true
    
});


function getManageIds()
{
    //取得所有选中的节点，返回节点对象的集合
    var nodes=$("#jstree_div").jstree("get_top_checked");
    $("#manage_id").val(nodes);
}


function getSelect()
{
    var names = "";
    var nodes = $("#jstree_div").jstree("get_top_checked");
    $.each(nodes, function(i, n) {
        var node = $('#jstree_div').jstree("get_node", n);
        names += node.text+",";

    });
    window.opener.document.getElementById("manage_id").value = nodes;
    window.opener.document.getElementById("manage_name").innerHTML = names;
    window.opener = null;
    window.close();
}
