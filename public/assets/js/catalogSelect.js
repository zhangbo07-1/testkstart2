var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
var catalog_name =  window.opener.document.getElementById("catalog_name").innerHTML;
var node_id;
$('#jstree_catalog_div').jstree({
    'core' : {
	'data' : {
	    'url': "/catalogs/edit?operation=get_node",
            'type': 'POST',
            'dataType': 'JSON',
	    'data' : function (node) {
                return {  '_token': CSRF_TOKEN,'id' : node.id ,'catalog' : catalog_name};
            }
	},

	'check_callback' : function(o, n, p, i, m) {
	    if(m && m.dnd && m.pos !== 'i') { return false; }
	    return true;
	},
	'themes' : {
	    'responsive' : false,
	    'variant' : 'middle',
	    'stripes' : true
	}
    },
    'types' : {
        'default' : {
            "icon" : "glyphicon glyphicon-folder-open"
        }
    },
    "checkbox" : {
        "keep_selected_style" : false,
        "tree_state" : false
    },
    'plugins' : ['sort','types','search','checkbox','wholerow']
});
function getSelect()
{
    var names = "";
    var nodes = $("#jstree_catalog_div").jstree("get_top_checked");
    $.each(nodes, function(i, n) {
        var node = $('#jstree_catalog_div').jstree("get_node", n);
        names += node.text+",";

    });
    window.opener.document.getElementById("catalog_id").value = nodes;
    window.opener.document.getElementById("catalog_name").innerHTML = names;
    window.opener = null;
    window.close();
}
var to = false;
function search_node()
{
     var ref = $('#jstree_div').jstree(true);
    var v = $('#searchWord').val();
    $.ajax({
        url: "/catalogs/edit?operation=search_node",
        type: 'POST',
        data:
        {
            _token: CSRF_TOKEN,
            keyword:  v
        },
        dataType: 'JSON',
        success: function (res)
        {
                $.each(res, function(i, d) 
            {
                var node = $('#jstree_div').jstree("get_node",d.id);
                ref.open_node(node);
            });
        }
    });
    if(to)
    {
        clearTimeout(to);
    }
    to = setTimeout(function ()
        {
            var v = $('#searchWord').val();
            $('#jstree_div').jstree(true).search(v);
        }, 250);
}
