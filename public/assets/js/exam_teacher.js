function checkButton() {
    $("#checkUsersButton").change(function () {
        if ($(this).is(":checked")) {
            $("input.checkUsers:checkbox:not(:checked)").prop('checked', true);
        } else {
            $("input.checkUsers:checkbox:checked").prop('checked', false);
        }
    });

    $("#unCheckUsersButton").change(function () {
        if ($(this).is(":checked")) {
            $("input.unCheckUsers:checkbox:not(:checked)").prop('checked', true);
        } else {
            $("input.unCheckUsers:checkbox:checked").prop('checked', false);
        }
    });
}
var distribute = {
    exam_id: exam_id,
    usersCheckedUrl: '/api/exams/' + exam_id+'/assigned-teacher/',
    usersNotCheckedUrl: '/api/exams/' + exam_id+'/unassigned-teacher',
    usersCheckedTemplate: Handlebars.compile($("#userCheckedTemplate").html()),
    usersNotCheckedTemplate: Handlebars.compile($("#userNotCheckedTemplate").html())
}

function getUsers(userType, url) {
    url = typeof url !== 'undefined' ? url : distribute[userType + "Url"];
    $.get(url, {search_word: $("#searchWord").val(), exam_id: distribute.exam_id},
        function (responseData) {
            $("#" + userType + "Form").html(distribute[userType + 'Template'](responseData));
            if (responseData.last_page != 1) {
                $('#' + userType + 'Form-Pagination').bootpag({
                    total: responseData.last_page,
                    page: responseData.current_page,
                    maxVisible: 10,
                    leaps: true,
                    firstLastUse: true,
                    first: '首页',
                    last: '尾页',
                    wrapClass: 'pagination',
                    activeClass: 'active',
                    disabledClass: 'disabled',
                    nextClass: 'next',
                    prevClass: 'prev',
                    lastClass: 'last',
                    firstClass: 'first'
                }).on("page", function (event, num) {
                    var visit_url = url + "/?page= " + num;
                    $.get(visit_url, {search_word: $("#searchWord").val(), exam_id: distribute.exam_id},
                        function (responseData) {
                            $("#" + userType + "Form").html(distribute[userType + "Template"](responseData));
                        });
                });
            }
        });
}



function refreshUsers() {
    initialAllUsersTable();
    getUsers("usersNotChecked");
    getUsers("usersChecked");
}


$("#searchButton").click(function () {
    refreshUsers();
})

$(document).ready(function () {
    getUsers("usersNotChecked");
    getUsers("usersChecked");
    checkButton();
})

var opts = {
    lines: 15 // The number of lines to draw
    , length: 28 // The length of each line
    , width: 14 // The line thickness
    , radius: 42 // The radius of the inner circle
    , scale: 1 // Scales overall size of the spinner
    , corners: 1 // Corner roundness (0..1)
    , color: '#000' // #rgb or #rrggbb or array of colors
    , opacity: 0.25 // Opacity of the lines
    , rotate: 0 // The rotation offset
    , direction: 1 // 1: clockwise, -1: counterclockwise
    , speed: 1 // Rounds per second
    , trail: 60 // Afterglow percentage
    , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
    , zIndex: 2e9 // The z-index (defaults to 2000000000)
    , className: 'spinner' // The CSS class to assign to the spinner
    , top: '50%' // Top position relative to parent
    , left: '50%' // Left position relative to parent
    , shadow: false // Whether to render a shadow
    , hwaccel: false // Whether to use hardware acceleration
    , position: 'absolute' // Element positioning
}
var spinner = new Spinner(opts).spin();

function initCheckButtons(){
    $("#unCheckUsersButton").prop('checked',false);
    $("#checkUsersButton").prop('checked',false);
}

function initialAllUsersTable() {
    $("#usersCheckedForm").html(spinner.el);
    $("#usersNotCheckedForm").html(spinner.el);
    $("#usersNotCheckedForm-Pagination").html('');
    $("#usersCheckedForm-Pagination").html('');

}


var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
function postUser() {
    var selected = [];
    var is_all = false;

    if ($('input#unCheckUsersButton').is(':checked')) {
        is_all = true;
    } else {
        $('#usersNotCheckedForm input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
    }
    console.log({
        _token: CSRF_TOKEN,
        user_id: selected,
        exam_id: exam_id,
        is_all: is_all,
        search_word: $("#searchWord").val()
    });
    $.ajax({
        url: "/exams-maintain",
        type: 'POST',
        data: {
            _token: CSRF_TOKEN,
            user_id: selected,
            exam_id: exam_id,
            is_all: is_all,
            search_word: $("#searchWord").val()
        },
        dataType: 'JSON',
        success: function (data) {
            if (data.result == true) {
                initCheckButtons();
                showSuccessMessage(data.message);
                refreshUsers();
            }
        }
    })
}

function deleteUser() {
    var selected = [];
    var is_all = false;

    if ($('input#checkUsersButton').is(':checked')) {
        is_all = true;
    } else {
        $('#usersCheckedForm input:checked').each(function () {
            selected.push($(this).attr('value'));
        });
    }

    selected.push($(this).attr('value'));

    $.ajax({
        url: "/exams-maintain",
        type: 'DELETE',
        data: {
            _token: CSRF_TOKEN,
            user_id: selected,
            exam_id: exam_id,
            is_all: is_all,
            search_word: $("#searchWord").val()
        },
        dataType: 'JSON',
        success: function (data) {

            if(data.result == true)
            {
                initCheckButtons();
                showSuccessMessage(data.message);
                refreshUsers();
            }
        }
    })
}


$("#notCheckUsersSubmit").click(function () {
    postUser();
    refreshUsers();
})

$("#checkUsersSubmit").click(function () {
    deleteUser();
    refreshUsers();
})



$('#searchWord').keypress(function (e) {
    var key = e.which;
    if (key == 13)  // the enter key code
    {
        refreshUsers();
        return false;
    }
});
//# sourceMappingURL=distribute.js.map
