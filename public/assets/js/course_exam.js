var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
var examTemplate = Handlebars.compile($("#examTemplate").html());

function getExams(userType, url) {
    var url = '/api/exams/assign-course';
  $.get(url, {search_word: $("#searchWord").val(),course_id:course_id},
        function (responseData) {
            console.log(responseData);
            //$("#" + userType + "Form").append(distribute[userType + 'Template'](responseData));
            $("#ExamsForm").html(examTemplate(responseData));
            if (responseData.last_page != 1) {
                $('#ExamsForm-Pagination').bootpag({
                    total: responseData.last_page,
                    page: responseData.current_page,
                    maxVisible: 10,
                    leaps: true,
                    firstLastUse: true,
                    first: '首页',
                    last: '尾页',
                    wrapClass: 'pagination',
                    activeClass: 'active',
                    disabledClass: 'disabled',
                    nextClass: 'next',
                    prevClass: 'prev',
                    lastClass: 'last',
                    firstClass: 'first'
                }).on("page", function (event, num) {
                    var visit_url = url + "/?page= " + num;
                    $.get(visit_url, {search_word: $("#searchWord").val()},
                        function (responseData) {
                            console.log(examTemplate(responseData));
                            $("#ExamsForm").html(examTemplate(responseData));
                        });
                });
            }
        });
}




function setExam(id) {

    $.ajax({
        url: "/exams/"+id+'/assign-course',
        type: 'POST',
        data: {
            _token: CSRF_TOKEN,
            exam_id: id,
            course_id: course_id
        },
        dataType: 'JSON',
        success: function (data) {
            if (data.result == true) {
                showSuccessMessage(data.message);
                getExams();
                $("#selectedExamTitle").html(data.exam_title);
            }
        }
    });
}

function getChooseResult() {
    var selectedVal = "";
    var selected = $("#ExamsForm input[type='radio']:checked");
    if (selected.length > 0) {
        selectedVal = selected.val();
        console.log(selectedVal);
        setExam(selectedVal);
    }
}

$(document).ready(function () {
    getExams();
    $("#courseSearchButton").click(function () {
        getExams();
    })

    $("#chooseExamButton").click(function () {
        getChooseResult();
    })
})

$('#searchWord').keypress(function (e) {
    var key = e.which;
    if (key == 13)  // the enter key code
    {
        getExams();
        return false;
    }
});


//# sourceMappingURL=course_exam.min.js.map
