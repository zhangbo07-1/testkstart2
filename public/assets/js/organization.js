var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
var node_id;
$('#jstree_div').jstree({
    'core' : {
	'data' : {
	    'url': "/organization/show?operation=get_node",
            'type': 'POST',
            'dataType': 'JSON',
	    'data' : function (node) {
                return {  '_token': CSRF_TOKEN,'id' : node.id ,'NUM' : true};
            }
	},

	'check_callback' : function(o, n, p, i, m) {
	    if(m && m.dnd && m.pos !== 'i') { return false; }
	    return true;
	},
	'themes' : {
	    'responsive' : false,
	    'variant' : 'middle',
	    'stripes' : true
	}
    },
    'types' : {
        'default' : {
            "icon" : "glyphicon glyphicon-user"
        }
    },
    'unique' : {
	'duplicate' : function (name, counter) {
	    return name + ' ' + counter;
	}
    },

    'plugins' : ['state','sort','types','unique','search','wholerow']
});
$('#jstree_div').on('create_node.jstree', function (e, data)
    {
        $.ajax({
            url: "/organization/show?operation=create_node",
            type: 'POST',
            data:
            {
                _token: CSRF_TOKEN,
                type: data.node.type,
                id:  data.node.parent,
                text: data.node.text
            },
            dataType: 'JSON',
            success: function (d)
            {
                data.instance.set_id(data.node, d.id);
            }
        });
    })
		.on('rename_node.jstree', function (e, data)
                    {
		        $.ajax({
                            url: "/organization/show?operation=rename_node",
                            type: 'POST',
                            data:
                            {
                                _token: CSRF_TOKEN,
                                id:  data.node.id,
                                text: data.text
                            },
                            dataType: 'JSON',
                            success: function (d)
                            {
                                data.instance.set_id(data.node, d.id);
                            }
                        });

                        
		    })
    
                .on('delete_node.jstree', function (e, data)
                    {
                        $.ajax({
                            url: "/organization/show?operation=delete_node",
                            type: 'POST',
                            data:
                            {
                                _token: CSRF_TOKEN,
                                id:  data.node.id
                            },
                            dataType: 'JSON',
                            success: function (d)
                            {
                                if(d.status != 'OK')
                                {
                                    data.instance.refresh();
                                    showSuccessMessage(d.error);
                                }
                            }
                        });
                    });
;


var to = false;
$('#node_search').keyup(function ()
    {
        if(to)
        {
            clearTimeout(to);
        }
        to = setTimeout(function ()
            {
                var v = $('#node_search').val();
                $('#jstree_div').jstree(true).search(v);
            }, 250);
    });

function search_node()
{
     var ref = $('#jstree_div').jstree(true);
    var v = $('#searchWord').val();
    $.ajax({
        url: "/organization/show?operation=search_node",
        type: 'POST',
        data:
        {
            _token: CSRF_TOKEN,
            keyword:  v
        },
        dataType: 'JSON',
        success: function (res)
        {
            console.log(res);
                $.each(res, function(i, d) 
            {
                var node = $('#jstree_div').jstree("get_node",d.id);
                ref.open_node(node);
            });
        }
    });
    if(to)
    {
        clearTimeout(to);
    }
    to = setTimeout(function ()
        {
            var v = $('#searchWord').val();
            $('#jstree_div').jstree(true).search(v);
        }, 250);
}
function create_node()
{
    var ref = $('#jstree_div').jstree(true),
        sel = ref.get_selected();
    if(!sel.length)
    {
        return false;
    }
    sel = sel[0];
    sel = ref.create_node(sel, {"type":"file"});
    if(sel)
    {
        ref.edit(sel);
    }
};
function rename_node()
{
    var ref = $('#jstree_div').jstree(true),
        sel = ref.get_selected();
    if(!sel.length)
    {
        return false;
    }
    sel = sel[0];
    ref.edit(sel);
};
function delete_node()
{
    var ref = $('#jstree_div').jstree(true),
        sel = ref.get_selected(true);
    if(!sel.length)
    {
        return false;
    }
    if(sel[0].text.indexOf('(0)') < 0)
    {
        showSuccessMessage("该组织存在用户，无法删除");
        return false;
    }
    
    if(!confirm("确认要删除？")) {

        return false;
    }
    ref.delete_node(sel);
};
