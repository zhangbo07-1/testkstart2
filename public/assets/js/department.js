var CSRF_TOKEN = $('meta[name="_token"]').attr('content');
var department_name =  window.opener.document.getElementById("department_name").innerHTML;
var node_id;
$('#jstree_department_div').jstree({
    'core' : {
        "multiple":false,
        'data' : {
            'url': "/organization/show?operation=get_node",
            'type': 'POST',
            'dataType': 'JSON',
            'data' : function (node) {
                return {  '_token': CSRF_TOKEN,'id' : node.id ,'manage' :department_name};
            }
        },

        'check_callback' : function(o, n, p, i, m) {
            if(m && m.dnd && m.pos !== 'i') { return false; }
            return true;
        },
        'themes' : {
            'responsive' : false,
            'variant' : 'middle',
            'stripes' : true
        }
    },
    'types' : {
        'default' : {
            "icon" : "glyphicon glyphicon-user"
        }
    },
    "checkbox" : {
        "keep_selected_style" : false,
        "tree_state" : false
    },
    
  'plugins' : ['sort','types','search','checkbox','wholerow']
});


function getSelect()
{
    var names = "";
    var nodes = $("#jstree_department_div").jstree("get_top_checked");
    $.each(nodes, function(i, n) {
        var node = $('#jstree_department_div').jstree("get_node", n);
        names += node.text+",";

    });
    window.opener.document.getElementById("department_id").value = nodes;
    window.opener.document.getElementById("department_name").innerHTML = names;
    window.opener = null;
    window.close();
}
var to = false;
function search_node()
{
     var ref = $('#jstree_department_div').jstree(true);
    var v = $('#searchWord').val();
    $.ajax({
        url: "/organization/show?operation=search_node",
        type: 'POST',
        data:
        {
            _token: CSRF_TOKEN,
            keyword:  v
        },
        dataType: 'JSON',
        success: function (res)
        {
            console.log(res);
                $.each(res, function(i, d) 
            {
                var node = $('#jstree_department_div').jstree("get_node",d.id);
                ref.open_node(node);
            });
        }
    });
    if(to)
    {
        clearTimeout(to);
    }
    to = setTimeout(function ()
        {
            var v = $('#searchWord').val();
            $('#jstree_department_div').jstree(true).search(v);
        }, 250);
}
